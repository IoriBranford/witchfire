#include "ui.hpp"
#include "ui_internal.hpp"
#include "body.hpp"
#include "input.hpp"
#include "units.hpp"
#include "ship.hpp"
#include <cmath>

static UIWidget *ui_cursortarget;
static float ui_cursoranalog[ANALOG_COUNT];

static const float CURSOR_ANALOG_THRESHOLD = 0.5f;

static void UICursor_OnHit(void *owner, const Hit &hit)
{
	const int CURSOR_TARGET_GROUP_FLAGS =
		(1 << HITGROUP_UIBUTTON);

	if (hit.hittergroupflags & CURSOR_TARGET_GROUP_FLAGS)
	{
		if (!ui_cursortarget)
		{
			ui_cursortarget = (UIWidget*)hit.hitter;
			if (ui_cursortarget->highlightfunc)
			{
				ui_cursortarget->highlightfunc(ui_cursortarget,
						true);
			}
		}
	}
}

static void UICursor_Move(int dx_mi, int dy_mi)
{
	UIWidget *ui_cursor = UIWidget_Get("cursor");
	if (!ui_cursor)
		return;

	Body_AddPosAndKeepIn(ui_cursor->body, dx_mi, dy_mi, &SCREEN_BOUNDS_MI);

	if (ui_cursortarget && ui_cursortarget->body &&
		!Body_IntersectsBody(ui_cursor->body, ui_cursortarget->body))
	{
		if (ui_cursortarget->highlightfunc)
			ui_cursortarget->highlightfunc(ui_cursortarget, false);
		ui_cursortarget = NULL;
	}
}

static void UICursor_OnPointCommand(int commandflags, float x, float y)
{
	UICursor_Move(PixelToMicron(x), PixelToMicron(y));
}

static void UICursor_OnDigitalCommand(int command, bool on)
{
	switch (command)
	{
	case DIGITAL_FIRE_LEFT:
		if (on)
		{
			//ui_cursortarget->pressaction();
			// target pushed
		}
		else
		{
			if (ui_cursortarget && ui_cursortarget->releaseaction)
				ui_cursortarget->releaseaction(ui_cursortarget);
		}
		break;

	case DIGITAL_FIRE_RIGHT:
		// cancel, somehow
		break;

	case DIGITAL_LEFT:
	case DIGITAL_RIGHT:
	case DIGITAL_UP:
	case DIGITAL_DOWN:
		if (!on)
			break;

		UIWidget *source = ui_cursortarget ? ui_cursortarget
				: UIWidget_Get("cursor");

		UIWidget *neighbor = UIWidget_DirSearchFrom(source, command);

		if (source && neighbor)
		{
			const SDL_Point *neighborpos
				= Body_Pos_Micron(neighbor->body);
			const SDL_Point *sourcepos
				= Body_Pos_Micron(source->body);

			UICursor_Move(neighborpos->x - sourcepos->x,
					neighborpos->y - sourcepos->y);
		}
		break;
	}
}

static void UICursor_OnAnalogCommand(int command, float v)
{
	if (std::abs(v) < CURSOR_ANALOG_THRESHOLD)
	{
		ui_cursoranalog[command] = 0;
	}
	else if (!ui_cursoranalog[command])
	{
		ui_cursoranalog[command] = v;
		switch (command)
		{
		case ANALOG_X:
			if (v > 0)
				UICursor_OnDigitalCommand(DIGITAL_RIGHT, true);
			else
				UICursor_OnDigitalCommand(DIGITAL_LEFT, true);
			break;

		case ANALOG_Y:
			if (v > 0)
				UICursor_OnDigitalCommand(DIGITAL_DOWN, true);
			else
				UICursor_OnDigitalCommand(DIGITAL_UP, true);
			break;
		}
	}
}

void UICursor_Init()
{
	UIWidget *ui_cursor = UIWidget_Get("cursor");
	if (!ui_cursor)
		return;

	ui_cursortarget = NULL;
	ui_cursoranalog[ANALOG_X] = ui_cursoranalog[ANALOG_Y] = 0;

	Input_SetPointCallback(UICursor_OnPointCommand);
	Input_SetDigitalCallback(UICursor_OnDigitalCommand);
	Input_SetAnalogCallback(UICursor_OnAnalogCommand);

	Body_SetOwnerAndHitCallback(ui_cursor->body, ui_cursor, UICursor_OnHit);
}
