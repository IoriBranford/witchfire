#ifndef __TITLE_H__
#define __TITLE_H__

void Title_LoadAssets();
void Title_Init();
void Title_Tick();
void Title_PreRender(int dt);

#endif
