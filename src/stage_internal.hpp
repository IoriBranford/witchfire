#ifndef __STAGE_INTERNAL_H__
#define __STAGE_INTERNAL_H__

#include "SDL.h"

struct StageObject
{
	char cel[16];
	SDL_Point pos;
	char script[16];

	StageObject();
};

extern SDL_Point stage_vel_mi;

void StageScript_RegisterLib();

#endif
