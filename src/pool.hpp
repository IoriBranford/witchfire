#ifndef __POOL_H__
#define __POOL_H__

#include <vector>
#include <algorithm>
#include <cassert>
#include "alist.hpp"

const int POOL_NID = AList<short>::NKEY;

template <class T> class Pool
{
public:
	Pool()
	{
	}

	Pool(unsigned size)
	{
		Init(size);
	}

	unsigned Count()
	{
		return pool.size();
	}

	void Init(unsigned size)
	{
		Clear();
		size = std::min(size, 1u << ((sizeof(short) << 3) - 1));
		idindexes.Init(size);
		pool.reserve(size);
		indexids.reserve(size);
	}

	bool Full()
	{
		return idindexes.Full();
	}

	int New()
	{
		if (Full())
			return POOL_NID;

		int id = idindexes.Push(Count());
		pool.push_back(T());
		indexids.push_back(id);
		return id;
	}

	T& operator[](int id)
	{
		assert(0 <= id && id < (int)idindexes.Size());
		assert(0 <= idindexes[id] && idindexes[id] < (short)Count());
		return pool[idindexes[id]];
	}

	typedef typename std::vector<T>::iterator Iterator;

	Iterator Begin()
	{
		return pool.begin();
	}

	Iterator End()
	{
		return pool.end();
	}

	void ForEach(void (*func)(T&))
	{
		assert(func);
		unsigned count = Count();

		for (unsigned i = 0; i < count; ++i)
		{
			func(pool[i]);
		}
	}

	void ForEach(void (*func)(T&, void*), void *data)
	{
		assert(func);
		unsigned count = Count();

		for (unsigned i = 0; i < count; ++i)
		{
			func(pool[i], data);
		}
	}

	void Sort(bool (*compare)(const T&, const T&))
	{
		//TODO if necessary
	}

	void Sort(bool (*compare)(const T&, const T&),
	     Iterator it0, Iterator it1)
	{
		//TODO if necessary
	}

	void DelIf(bool (*shoulddel)(T&), void (*uninit)(T&) = NULL)
	{
		assert(shoulddel);
		unsigned count = Count();

		for (unsigned i = 0; i < count; )
		{
			if (shoulddel(pool[i]))
			{
				if (uninit)
					uninit(pool[i]);
				Del(indexids[i]);
				--count;
			}
			else
			{
				++i;
			}
		}
	}

	void Del(int id)
	{
		if (id == POOL_NID)
			return;

		assert(0 <= id && id < (int)idindexes.Size());

		int backid = indexids[Count() - 1];
		int delindex = idindexes[id];

		idindexes[backid] = delindex;
		pool[delindex] = pool[Count() - 1];
		indexids[delindex] = backid;

		idindexes.Pop(id);
		pool.pop_back();
		indexids.pop_back();
	}

	void Clear()
	{
		pool.clear();
		idindexes.Clear();
		indexids.clear();
	}

	void PrintFreeIds()
	{
		idindexes.PrintFreeKeys();
	}

private:
	AList<short> idindexes;
	std::vector<T> pool;
	std::vector<short> indexids;
};

#endif
