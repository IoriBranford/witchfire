#ifndef __UI_INTERNAL_H__
#define __UI_INTERNAL_H__

struct UIWidget;
struct Body;
struct Sprite;
struct Text;

typedef void (*ActionFunc)(UIWidget *self);
typedef void (*HighlightFunc)(UIWidget *self, bool highlight);

struct UIWidget
{
	Body *body;
	Sprite *sprite;
	Text *text;

	HighlightFunc highlightfunc;
	ActionFunc releaseaction;
};

#define UIACTION_REGISTER(func) UIAction_Register(#func, func)
void UIAction_Register(const char *name, ActionFunc func);
void UIAction_RegisterMain();

ActionFunc UIAction_Find(const char *name);
void UIAction_Call(const char *name, UIWidget *widget);

#endif
