#include "data.hpp"

#include "disk.hpp"

#include <cassert>

static const char *DIR = "dat/";
const char *Data::EXT = ".json";

bool Data::Open(const char *name)
{
	std::string path;
	path += DIR;
	path += name;
	path += EXT;
	return FOpen(path);
}

bool Data::FOpen(const char *path)
{
	std::string text;
	int readct = Disk_TextOpen(path, text);
	if (readct <= 0)
		return false;

	return Parse(text.c_str());
}

bool Data::Parse(const char *text)
{
	json = cJSON_Parse(text);
	return json != NULL;
}

#include <stdlib.h>

bool Data::Save(const char *name)
{
	std::string path;
	path += DIR;
	path += name;
	path += EXT;
	return FSave(path);
}

bool Data::FSave(const char *path)
{
	char *rawtext = cJSON_Print(json);
	std::string text (rawtext);
	free(rawtext); // allocated with malloc(), not new

	Disk_SetWritePathBase();
	int writect = Disk_TextSave(path, text);

	return writect > 0 && (unsigned)writect == text.size();
}

void Data::Close()
{
	cJSON_Delete(json);
}

void Data::Rename(const char *dname)
{
	size_t len = std::strlen(dname) + 1;
	free(json->string);
	json->string = (char*)malloc(len);
	std::memcpy(json->string, dname, len);
}

Data Data::InsAfter(Data it, const char *dname, Data d)
{
	if (!d)
		return Data();

	if (it)
	{
		if (it.json->next)
			it.json->next->prev = d.json;
		d.json->next = it.json->next;
		d.json->prev = it.json;
		it.json->next = d.json;
	}
	else
	{
		if (json->child)
			json->child->prev = d.json;
		d.json->next = json->child;
		json->child = d.json;
	}

	if (dname)
		d.Rename(dname);

	return d;
}

Data Data::Replace(Data it, const char *dname, Data d)
{
	assert(it);
	std::string dnamestr;
	if (dname)
		dnamestr = dname;

	d.json->next = it.json->next;
	d.json->prev = it.json->prev;
	if (d.json->next)
		d.json->next->prev = d.json;
	if (it.json == json->child)
		json->child = d.json;
	else
		d.json->prev->next = d.json;

	it.json->next = it.json->prev = 0;
	it.Close();

	if (dname)
		d.Rename(dnamestr.c_str());

	return d;
}

void Data::ParseStrIntoNum()
{
	assert(json && json->valuestring);

	double valdouble;
	char *afterval;

	valdouble = strtod(json->valuestring, &afterval);

	if(!*afterval)
	{
		json->valueint = json->valuedouble = valdouble;
	}
}
