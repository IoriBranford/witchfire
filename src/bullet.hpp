#ifndef __BULLET_H__
#define __BULLET_H__

#include "SDL.h"

struct Bullet;
// dumb projectile that hurts ships

void AllBullets_Init(int n);
void AllBullets_Tick();
void AllBullets_PreRender(int dt);

Bullet* Bullet_New(const char *type, const SDL_Point *pos_mi, double angle,
		int speed_mi, const SDL_Point *accel_mi = NULL, int frame = 0);

void AllBullets_KillEnemy();

#endif
