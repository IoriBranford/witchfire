#include "body.hpp"

#include "units.hpp"

#include "SDL.h"

#include <algorithm>
#include <cmath>

#include "body_internal.hpp"

//void Body_SetVolume(Body *body, const SDL_Rect *vol_mi)
//{
//	body->vol_mi = *vol_mi;
//}

void Body_SetPos(Body *body, const SDL_Point *pos_mi)
{
	body->pos_mi = *pos_mi;
}

void Body_SetVel(Body *body, const SDL_Point *vel_mi)
{
	body->vel_mi = *vel_mi;
}

const SDL_Point* Body_Pos_Micron(const Body *body)
{
	return &body->pos_mi;
}

static SDL_Point body_posintime_result;

const SDL_Point* Body_PosInTime_Micron(const Body *body, int t)
{
	body_posintime_result = body->pos_mi;
	body_posintime_result.x += body->vel_mi.x * t;
	body_posintime_result.y += body->vel_mi.y * t;
	int tsq = t * t;
	body_posintime_result.x += body->accel_mi.x * tsq / 2;
	body_posintime_result.y += body->accel_mi.y * tsq / 2;

	return &body_posintime_result;
}

const SDL_Point* Body_Vel_Micron(const Body *body)
{
	return &body->vel_mi;
}

void Body_SetAccel(Body *body, int ax_mi, int ay_mi)
{
	body->accel_mi.x = ax_mi;
	body->accel_mi.y = ay_mi;
}

void Body_AddAccelTowardVel(Body *body, int vx_mi, int vy_mi)
{
	body->accel_mi.x += vx_mi - body->vel_mi.x;
	body->accel_mi.y += vy_mi - body->vel_mi.y;
}

void Body_AddAccelTowardPos(Body *body, int x_mi, int y_mi)
{
	body->accel_mi.x += (x_mi - body->pos_mi.x);
	body->accel_mi.y += (y_mi - body->pos_mi.y);
}

void Body_AddAccelToKeepIn(Body *body, const SDL_Rect *bounds_mi,
		float bouncefactor)
{
	int nextvx = body->vel_mi.x + body->accel_mi.x;
	int nextvy = body->vel_mi.y + body->accel_mi.y;
	int nextx = body->pos_mi.x + nextvx;
	int nexty = body->pos_mi.y + nextvy;

	int nextl = nextx;
	int nextt = nexty;
	int nextr = nextx;
	int nextb = nexty;

	switch (body->vol_mi.type)
	{
	case VOLUME_BOX:
		nextl += body->vol_mi.box.rect.x;
		nextt += body->vol_mi.box.rect.y;
		nextr = nextl + body->vol_mi.box.rect.w;
		nextb = nextt + body->vol_mi.box.rect.h;
		break;

	case VOLUME_BALL:
	case VOLUME_RAY:
		// TODO when/if we need them
		break;

	default:
		;
	}

	int pushl = std::max(0, bounds_mi->x - nextl);
	int pusht = std::max(0, bounds_mi->y - nextt);
	int pushr = std::min(0, bounds_mi->x + bounds_mi->w - nextr);
	int pushb = std::min(0, bounds_mi->y + bounds_mi->h - nextb);

	if (bouncefactor)
	{
		bouncefactor = -(bouncefactor + 1);
		if (pushl) body->accel_mi.x += bouncefactor * nextvx;
		if (pusht) body->accel_mi.y += bouncefactor * nextvy;
		if (pushr) body->accel_mi.x += bouncefactor * nextvx;
		if (pushb) body->accel_mi.y += bouncefactor * nextvy;
	}
	else
	{
		body->accel_mi.x += pushl;
		body->accel_mi.y += pusht;
		body->accel_mi.x += pushr;
		body->accel_mi.y += pushb;
	}
}

void Body_AddAccelToFollow(Body *body, const Body *leadbody,
		const SDL_Point *offset_mi)
{
	SDL_Point nextpos_mi =
	{
		body->pos_mi.x + body->vel_mi.x + body->accel_mi.x,
		body->pos_mi.y + body->vel_mi.y + body->accel_mi.y
	};

	SDL_Point targetpos_mi =
	{
		leadbody->pos_mi.x + leadbody->vel_mi.x + leadbody->accel_mi.x,
		leadbody->pos_mi.y + leadbody->vel_mi.y + leadbody->accel_mi.y
	};

	if (offset_mi)
	{
		targetpos_mi.x += offset_mi->x;
		targetpos_mi.y += offset_mi->y;
	}

	body->accel_mi.x += targetpos_mi.x - nextpos_mi.x;
	body->accel_mi.y += targetpos_mi.y - nextpos_mi.y;
}

void Body_AddPosAndKeepIn(Body *body, int x_mi, int y_mi,
		const SDL_Rect *bounds_mi)
{
	int nextx = body->pos_mi.x + x_mi;
	int nexty = body->pos_mi.y + y_mi;

	int nextl = nextx;
	int nextt = nexty;
	int nextr = nextx;
	int nextb = nexty;

	switch (body->vol_mi.type)
	{
	case VOLUME_BOX:
		nextl += body->vol_mi.box.rect.x;
		nextt += body->vol_mi.box.rect.y;
		nextr = nextl + body->vol_mi.box.rect.w;
		nextb = nextt + body->vol_mi.box.rect.h;
		break;

	case VOLUME_BALL:
	case VOLUME_RAY:
		// TODO when/if we need them
		break;

	default:
		;
	}

	body->pos_mi.x = nextx;
	body->pos_mi.y = nexty;
	body->pos_mi.x += std::max(0, bounds_mi->x - nextl);
	body->pos_mi.y += std::max(0, bounds_mi->y - nextt);
	body->pos_mi.x += std::min(0, bounds_mi->x + bounds_mi->w - nextr);
	body->pos_mi.y += std::min(0, bounds_mi->y + bounds_mi->h - nextb);
}

void Body_SetSpriteDstRect(const Body *body, SDL_Rect *dstrect_px)
{
	dstrect_px->x = MicronToPixel(body->pos_mi.x);
	dstrect_px->y = MicronToPixel(body->pos_mi.y);

	const SDL_Rect &rect = body->vol_mi.box.rect;
	const SDL_Point &center = body->vol_mi.ball.center;
	int radius = body->vol_mi.ball.radius;

	switch (body->vol_mi.type)
	{
	case VOLUME_BOX:
		dstrect_px->x += dstrect_px->w * rect.x / rect.w;
		dstrect_px->y += dstrect_px->h * rect.y / rect.h;
		break;

	case VOLUME_BALL:
		dstrect_px->x += dstrect_px->w * center.x / radius;
		dstrect_px->y += dstrect_px->h * center.y / radius;
		break;

	default:
		;
	}
}

void Body_Tick(Body &body)
{
	body.vel_mi.x += body.accel_mi.x / body.mass;
	body.vel_mi.y += body.accel_mi.y / body.mass;
	body.pos_mi.x += body.vel_mi.x;
	body.pos_mi.y += body.vel_mi.y;
}

void Body_Draw(Body &body)
{
	SDL_Rect rect_px;
	SDL_Point p0_px, p1_px;
	const int BALL_POINTS_CT = 16;
	const double BALL_POINTS_ARC_RAD = 2 * M_PI / BALL_POINTS_CT;
	double angle_rad = BALL_POINTS_ARC_RAD * 0.5;
	int ballpointsdist_mi;

	switch (body.vol_mi.type)
	{
	case VOLUME_BOX:
		rect_px = body.vol_mi.box.rect;
		rect_px.x += body.pos_mi.x;
		rect_px.y += body.pos_mi.y;
		rect_px.x = MicronToPixel(rect_px.x);
		rect_px.y = MicronToPixel(rect_px.y);
		rect_px.w = MicronToPixel(rect_px.w);
		rect_px.h = MicronToPixel(rect_px.h);
		//SDL_RenderDrawRect(renderer, &rect_px);
		break;

	case VOLUME_RAY:
		p0_px = body.vol_mi.ray.origin;
		p0_px.x += body.pos_mi.x;
		p0_px.y += body.pos_mi.y;

		p1_px = p0_px;
		p1_px.x += body.vol_mi.ray.vec.x;
		p1_px.y += body.vol_mi.ray.vec.y;

		p0_px.x = MicronToPixel(p0_px.x);
		p0_px.y = MicronToPixel(p0_px.y);
		p1_px.x = MicronToPixel(p1_px.x);
		p1_px.y = MicronToPixel(p1_px.y);

		//SDL_RenderDrawLine(renderer,
		//		p0_px.x, p0_px.y, p1_px.x, p1_px.y);
		break;

	case VOLUME_BALL:
		ballpointsdist_mi =
			body.vol_mi.ball.radius * 2 * M_PI / BALL_POINTS_CT;
		p0_px = body.vol_mi.ball.center;
		p0_px.x += body.pos_mi.x + body.vol_mi.ball.radius;
		p0_px.y += body.pos_mi.y;
		p1_px = p0_px;

		for (int i = 0; i < BALL_POINTS_CT; ++i)
		{
			p1_px.x -= ballpointsdist_mi * std::sin(angle_rad);
			p1_px.y += ballpointsdist_mi * std::cos(angle_rad);

			//SDL_RenderDrawLine(renderer,
			//		MicronToPixel(p0_px.x),
			//		MicronToPixel(p0_px.y),
			//		MicronToPixel(p1_px.x),
			//		MicronToPixel(p1_px.y));

			p0_px = p1_px;
			angle_rad += BALL_POINTS_ARC_RAD;
		}
		break;

	default:
		;
	}
}

