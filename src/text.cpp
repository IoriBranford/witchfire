#include "font.hpp"
#include "alist.hpp"
#include "SDL.h"
#include <string>

struct Text
{
	Font *font;
	SDL_Rect rect_px;
	std::string u8string;
	Uint8 r,g,b,a;

	Text() : font(0), u8string(), r(0xFF), g(0xFF), b(0xFF), a(0xFF)
	{
		rect_px.x = rect_px.y = rect_px.w = rect_px.h = 0;
	}
};

static bool Text_ShouldDel(const Text &text)
{
	return text.u8string.empty();
}

static void Text_Draw(Text &text)
{
	//SDL_SetRenderDrawColor(text.r, text.g, text.b, text.a);
	if (text.font)
		Font_DrawString(text.font, &text.rect_px,
			text.u8string.c_str());
}

void Text_SetString(Text *text, const char *u8string)
{
	text->u8string = u8string;
}

typedef AList<Text> TextList;
static TextList alltexts;

void AllTexts_Init(int n)
{
	alltexts.Init(n);
}

//static bool AllTexts_Full()
//{
//	return alltexts.Full();
//}

void AllTexts_Prune()
{
	alltexts.DelIf(Text_ShouldDel);
}

void AllTexts_Draw()
{
	alltexts.ForEach(Text_Draw);
}

Text* Text_New(const char *fontname, const SDL_Rect *rect_px,
		const char *u8string)
{
	Text *text = alltexts.Push();

	if (text)
	{
		text->font = Font_Get(fontname);
		text->rect_px = *rect_px;
		text->u8string = u8string;
	}

	return text;
}

void Text_Del(Text *text)
{
	text->u8string.clear();
}
