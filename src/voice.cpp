#include "voice.hpp"

#include "SDL_mixer.h"

#include "units.hpp"
#include <cmath>

struct Voice
{
	int channel;
	short angle;
	short distance;

	Voice()
	{
		channel = -1;
		angle = 0;
		distance = 0;
	}
};

void Voice_SetPosition(Voice *voice, short angle, short distance)
{
	voice->angle = angle;
	voice->distance = distance;

	if (voice->channel > -1)
		Mix_SetPosition(voice->channel, voice->angle, voice->distance);
}

void Voice_Play(Voice *voice, Mix_Chunk *chunk, int loops)
{
	voice->channel = Mix_PlayChannel(voice->channel, chunk, loops);
	if (voice->channel > -1)
		Mix_SetPosition(voice->channel, voice->angle, voice->distance);
}

bool Voice_Playing(Voice *voice)
{
	return (voice->channel > -1) && (Mix_Playing(voice->channel));
}

void Voice_Stop(Voice *voice)
{
	if (voice->channel > -1)
		Mix_HaltChannel(voice->channel);
}

static void Voice_Tick(Voice & voice)
{
	if (voice.channel > -1)
	{
		if (!Mix_Playing(voice.channel))
		{
			voice.channel = -1;
		}
	}
}

#include "alist.hpp"

typedef AList<Voice> VoiceList;
static VoiceList allvoices;

#include <climits>
static const int DEL_MARK = INT_MIN;

void AllVoices_Init(int n)
{
	allvoices.Init(n);
}

static bool Voice_ShouldDel(const Voice &voice)
{
	return voice.channel == DEL_MARK;
}

void AllVoices_Tick()
{
	allvoices.DelIf(Voice_ShouldDel);
	allvoices.ForEach(Voice_Tick);
}

bool AllVoices_Full()
{
	return allvoices.Full();
}

Voice* Voice_New()
{
	return allvoices.Push();
}

void Voice_Del(Voice *voice)
{
	//Voice &voice = allvoices[id];
	//Voice_Stop(&voice);
	voice->channel = DEL_MARK;
}

void AllVoices_SetVolume(int volume)
{
	Mix_Volume(-1, volume);
}

#include "body.hpp"

void Voice_SetToBody(Voice *voice, Body *body)
{
	short angle = (MicronToPixel(Body_Pos_Micron(body)->x) -
			SCREEN_CENTERX_PX) >> 2;
	angle = (((angle % 360) + 360) % 360);
	short dist = std::abs(
			(MicronToPixel(Body_Pos_Micron(body)->y) -
			 SCREEN_CENTERY_PX) >> 2);
	Voice_SetPosition(voice, angle, dist);
}
