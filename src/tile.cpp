#include "tile.hpp"
#include "tile_internal.hpp"

#include "mapdata.hpp"
#include "data.hpp"

#include "app.hpp"
#include "disk.hpp"

#include "texture.hpp"
#include "render.hpp"

#include <cassert>

#include "units.hpp"

static const char *TILESET_DIR = "stg/";
static const char *TILESET_EXT = ".png";

std::map<std::string, Tileset> alltilesets;
std::vector<TileLayer> alltilelayers;
SDL_Rect tilemap_viewrect_mi;
SDL_Rect tilemap_looprect_mi;
LoopState tilemap_loopstate;

static Tileset* Tileset_Load(Data tileset)
{
	assert(tileset);

	std::string name = tileset.Find("cel").CStr();
	if (name.empty())
		return 0;

	std::map<std::string, Tileset>::iterator it;
	it = alltilesets.find(name);
	if (it != alltilesets.end())
		return &it->second;

	std::string path (TILESET_DIR);
	path += name;
	path += TILESET_EXT;

	int sheetw, sheeth;
	unsigned int texture = Texture_FOpen(path.c_str(), &sheetw, &sheeth);

	if (texture)
	{
		int tilewidth = tileset.Find("tilewidth").Int();
		int tileheight = tileset.Find("tileheight").Int();
		float tcwidth = (float)tilewidth / sheetw;
		float tcheight = (float)tileheight / sheeth;
		int cols = sheetw / tilewidth;
		int rows = sheeth / tileheight;

		alltilesets[name] = Tileset(
				tcwidth, tcheight,
				tilewidth, tileheight,
				cols, rows,
				texture);

		return &alltilesets[name];
	}

	SDL_LogError(SDL_LOG_CATEGORY_RENDER, SDL_GetError());
	return 0;
}

TileLayer* TileLayer_New(const Data &layer, const Data &tilesets, int depth)
{
	Data type = layer.Find("type");
	if (std::strcmp(type.CStr(), "tilelayer"))
		return 0;

	int cols = layer.Find("width").Int();
	int rows = layer.Find("height").Int();
	Data tiles = layer.Find("data");

	TileLayer tilelayer (cols, rows, depth, 0);

	int c = 0, r = 0;
	int firstgid = 1;
	//int tilect = 0;

	for (Data tile = tiles.Begin();
			tile != tiles.End() && r < rows;
			++tile)
	{
		if (tile.Int() && !tilelayer.tileset)
		{
			Data tileset = MapData_FindGIDTileset(tilesets,
					tile.Int());

			if (tileset)
			{
				firstgid = tileset.Find("firstgid").Int();
				tilelayer.tileset = Tileset_Load(tileset);
			}
		}

		tilelayer.Tile(c, r) = (tile.Int() - firstgid);

		//if (tilect)
		//	tilelayer.Tile(c, r) %= tilect;

		if (++c >= cols)
		{
			c = 0;
			++r;
		}
	}

	if (!tilelayer.tileset)
	{
		SDL_LogError(SDL_LOG_CATEGORY_ERROR,
			"%s: couldn't find tileset for tile layer %s",
			__func__, layer.Find("name").CStr());
		return 0;
	}

	alltilelayers.push_back(tilelayer);
	return &alltilelayers[alltilelayers.size() - 1];
}

void TileLayer_Draw(TileLayer &tilelayer)
{
	assert(tilelayer.tileset);

	const Tileset &tileset = *tilelayer.tileset;
	SDL_Rect viewrect_px =
	{
		MicronToPixel(tilemap_viewrect_mi.x),
		MicronToPixel(tilemap_viewrect_mi.y),
		MicronToPixel(tilemap_viewrect_mi.w),
		MicronToPixel(tilemap_viewrect_mi.h)
	};
	int layerc = (viewrect_px.x/tileset.tilew) - 1;
	int layerr = (viewrect_px.y/tileset.tileh) - 1;

	struct { float x, y, w, h; } srcrect;
	SDL_Rect dstrect;
	int depth = tilelayer.depth;

	srcrect.w = tileset.tcw;
	srcrect.h = tileset.tch;
	dstrect.w = tileset.tilew;
	dstrect.h = tileset.tileh;

	dstrect.x = (layerc * dstrect.w) - viewrect_px.x;
	dstrect.y = (layerr * dstrect.h) - viewrect_px.y;
	int dstrecty0 = dstrect.y;

	int tile = -1;
	int tilect = tileset.TileCt();

	int loopc0 = 0;
	int loopr0 = 0;
	int loopcols = tilelayer.cols;
	int looprows = tilelayer.rows;

	if (tilemap_loopstate != LOOP_INACTIVE)
	{
		loopc0 = tilemap_looprect_mi.x / tileset.tilew;
		loopr0 = tilemap_looprect_mi.y / tileset.tileh;
		loopcols = tilemap_looprect_mi.w / tileset.tilew;
		looprows = tilemap_looprect_mi.h / tileset.tilew;
	}

	int loopc1 = loopc0 + loopcols;
	int loopr1 = loopr0 + looprows;

	layerc = loopc0+((((layerc-loopc0) % loopcols) + loopcols) % loopcols);
	layerr = loopr0+((((layerr-loopr0) % looprows) + looprows) % looprows);

	int layerr0 = layerr;

	int i = tilelayer.Index(layerc, layerr);
	Render_SetDrawingTransform(0, 0, 0, 0);
	Render_SetDrawingTexture(tileset.texture);
	Render_BeginDrawing();
	while (dstrect.x < viewrect_px.w)
	{
		while (dstrect.y < viewrect_px.h)
		{
			if (tile != tilelayer[i])
			{
				tile = tilelayer[i];
				srcrect.x = tileset.TileTCX(tile);
				srcrect.y = tileset.TileTCY(tile);
			}

			if (0 <= tile && tile < tilect)
			{
				Render_DrawQuad(dstrect.x,
						dstrect.y,
						dstrect.x + dstrect.w,
						dstrect.y + dstrect.h,
						srcrect.x,
						srcrect.y,
						srcrect.x + srcrect.w,
						srcrect.y + srcrect.h,
						depth);
			}

			dstrect.y += dstrect.h;

			++i;
			if (++layerr >= loopr1)
			{
				layerr = loopr0;
				i -= looprows;
				//break;
			}
		}

		dstrect.x += dstrect.w;
		dstrect.y = dstrecty0;

		i += (tilelayer.rows - layerr) + layerr0;
		if (++layerc >= loopc1)
		{
			layerc = loopc0;
			i -= loopcols * tilelayer.rows;
			//i %= tilelayer.Size();
			//break;
		}

		layerr = layerr0;
	}
	Render_EndDrawing();
}

