#include "stage_internal.hpp"

#include "ship.hpp"

#include "data.hpp"
#include "mapdata.hpp"

#include "music.hpp"

#include "tile.hpp"

#include "cel.hpp"
#include "sound.hpp"
#include "script.hpp"
#include "database.hpp"

#include "units.hpp"

#include <cstdlib>
#include <cassert>

static const char *STAGE_DIR = "stg/";

static unsigned stageload_group = 0;
static int stageload_layerdepth = 0;

static Data StageLoad_ReadObjectProperty(Data properties,
		Data defaultproperty)
{
	Data property = properties.Find(defaultproperty.Name());

	if (property)
		return property;

	return defaultproperty;
}

static void StageLoad_ReadShip(Data object, Data tilesets)
{
	SDL_Point pos;

	pos.x = object.Find("x").Int();
	pos.y = object.Find("y").Int();

	Data gid = object.Find("gid");
	Data tileset;

	if (gid)
		tileset = MapData_FindGIDTileset(tilesets, gid.Int());

	if (tileset)
	{
		pos.x += tileset.Find("tilewidth").Int() / 2;
		pos.y -= tileset.Find("tileheight").Int() / 2;

		Data tileoffset = tileset.Find("tileoffset");
		if (tileoffset)
		{
			pos.x += tileoffset.Find("x").Int();
			pos.y += tileoffset.Find("y").Int();
		}
	}

	pos.x = PixelToMicron(pos.x);
	pos.y = PixelToMicron(pos.y);

	Data properties = object.Find("properties");
	Data defaultproperties;
	std::string type = object.Find("type").CStr();
	if (!type.empty())
		defaultproperties = Database_Get("objects", type.c_str());

	Data team;
	Data script;

	if (defaultproperties)
	{
		for (Data p = defaultproperties.Begin();
				p != defaultproperties.End();
				++p)
		{
			if (p.Named("team") && !team)
			{
				team = StageLoad_ReadObjectProperty(
						properties, p);
			}
			else
			if (p.Named("script") && !script)
			{
				script = StageLoad_ReadObjectProperty(
						properties, p);
			}
			else
			if (p.Named("cel") || p.NamePrefixed("cel."))
			{
				Cel_Load(p.CStr());
			}
			else
			if (p.Named("sound") || p.NamePrefixed("sound."))
			{
				Sound_Load(p.CStr());
			}
		}
	}

	if (script)
		Script_Load(script.CStr());

	if (team && !std::strcmp(team.CStr(), "player"))
	{
		//TODO set initial player spawn
	}
	else
	{
		ShipSeq_AddSpawn(stageload_group, type.c_str(), &pos,
				script.CStr());
	}

	if (!(++object))
		++stageload_group;
}

static void StageLoad_ReadLayer(Data layer, Data tilesets)
{
	const char *type = layer.Find("type").CStr();

	if (!std::strcmp(type, "tilelayer"))
	{
		TileLayer_New(layer, tilesets, stageload_layerdepth);
	}
	else
	if (!std::strcmp(type, "objectgroup"))
	{
		Data os = layer.Find("objects");
		if (!os)
			return;

		for (Data o = os.Begin(); o != os.End(); ++o)
		{
			const char *otype = o.Find("type").CStr();

			if (!std::strcmp(otype, "looparea"))
			{
			}
			else
			if (!std::strcmp(otype, "scrollspeed"))
			{
			}
			else
			{
				StageLoad_ReadShip(o, tilesets);
			}
		}
	}

	++stageload_layerdepth;
}

static void StageLoad_ReadProperty(Data property)
{
	if (property.Named("music"))
	{
		if (*property.CStr())
			Music_Load(property.CStr());
		else
			Music_Uninit();
	}
	else
	if (property.Named("vel.x"))
	{
		stage_vel_mi.x = PixelToMicron(property.Int());
	}
	else
	if (property.Named("vel.y"))
	{
		stage_vel_mi.y = PixelToMicron(property.Int());
	}
}

bool Stage_Load(const char *name)
{
	stage_vel_mi.x = 0;
	stage_vel_mi.y = 0;

	stageload_group = 0;
	stageload_layerdepth = 0;
	std::string path (STAGE_DIR);
	path += name;
	path += Data::EXT;
	return MapData_FOpen(path.c_str(), MAPDATAOPEN_REVERSELAYERS,
			StageLoad_ReadLayer, StageLoad_ReadProperty);
}
