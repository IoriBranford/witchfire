#ifndef __SCHEDULE_H__
#define __SCHEDULE_H__

typedef void (*TimelineFunc)(void *data);
// Function to call after a number of ticks have passed

struct TimelineEvent;

void Timeline_Init(int maxcalls);

int Timeline_AddEvent(TimelineFunc func,
		void *data,
		unsigned tickslater);
void Timeline_CancelEvent(int id);

void Timeline_Tick();

void Timeline_Clear();

#endif
