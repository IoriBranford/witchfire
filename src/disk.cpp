#include "disk.hpp"

#include "SDL.h"

#include <physfs.h>

#include <cstdlib>
#include <cstring>

bool Disk_Open(const char *arg0)
{
	bool success = PHYSFS_init(arg0);

	if (success)
	{
		const char *ORG = "witchfire";
		const char *APP = "witchfire";
		const char *PAK = "pk7";

		success = PHYSFS_setSaneConfig(ORG, APP, PAK, false, false);
	}

	if (success)
	{
		Disk_SetWritePathUser();

		SDL_Log("Search Paths:");
		char **paths = PHYSFS_getSearchPath();

		for (int i = 0; paths[i] != NULL; ++i)
		{
			SDL_Log(paths[i]);
		}

		PHYSFS_freeList(paths);
	}
	else
	{
		SDL_LogError(SDL_LOG_CATEGORY_ERROR, PHYSFS_getLastError());
	}

	return success;
}

//bool Disk_AutoSetup()
//{
//	const char *ORG = "witchfire";
//	const char *APP = "witchfire";
//	const char *PAK = "pk7";
//
//	if (!PHYSFS_setSaneConfig(ORG, APP, PAK, false, false))
//	{
//		SDL_LogError(SDL_LOG_CATEGORY_ERROR, PHYSFS_getLastError());
//		return false;
//	}
//	return true;
//}

void Disk_Close()
{
	PHYSFS_deinit();
}

bool Disk_FileExists(const char *name)
{
	return PHYSFS_exists(name);
}

int Disk_CStrOpen(const char *name, char **textp)
{
	PHYSFS_File *file = PHYSFS_openRead(name);
	bool success = (file != NULL);
	int readct = 0;
	char *text = NULL;

	if (!success)
	{
		SDL_LogError(SDL_LOG_CATEGORY_ERROR, "%s: %s",
				name,
				PHYSFS_getLastError());
		return 0;
	}

	const int BUFSIZE = 256;
	char buf[BUFSIZE];
	int bufreadct = 0;

	do
	{
		bufreadct = PHYSFS_read(file, buf, 1, BUFSIZE);

		if (bufreadct > 0)
		{
			text = (char*)std::realloc(text, readct + bufreadct);
			if (text)
			{
				std::memcpy(&text[readct], buf, bufreadct);
				readct += bufreadct;
			}
		}
	}
	while (!PHYSFS_eof(file) && bufreadct > 0);

	success = PHYSFS_eof(file);
	PHYSFS_close(file);


	if (success)
	{
		*textp = text;
	}
	else
	{
		SDL_LogError(SDL_LOG_CATEGORY_ERROR, "%s: %s",
				name,
				PHYSFS_getLastError());
	}

	return readct;
}

void Disk_CStrClose(char *text)
{
	free(text);
}

int Disk_TextOpen(const char *name, std::string & text)
{
	PHYSFS_File *file = PHYSFS_openRead(name);
	bool success = (file != NULL);
	int readct = 0;

	if (success)
	{
		const int BUFSIZE = 256;
		char buf[BUFSIZE];
		int bufreadct = 0;

		do
		{
			bufreadct = PHYSFS_read(file, buf, 1, BUFSIZE);
			if (bufreadct > 0)
			{
				text.append(buf, bufreadct);
				readct += bufreadct;
			}
		}
		while (!PHYSFS_eof(file) && bufreadct > 0);

		success = PHYSFS_eof(file);
		PHYSFS_close(file);
	}

	if (!success)
	{
		SDL_LogError(SDL_LOG_CATEGORY_ERROR, "%s: %s",
				name,
				PHYSFS_getLastError());
	}

	return readct;
}

int Disk_TextSave(const char *name, const std::string & text)
{
	PHYSFS_File *file = PHYSFS_openWrite(name);
	int writect = 0;
	bool success = (file != NULL);

	if (success)
	{
		writect = PHYSFS_write(file, text.c_str(), 1, text.size());
		PHYSFS_close(file);
		success = writect > 0 && (unsigned)writect == text.size();
	}

	if (!success)
	{
		SDL_LogError(SDL_LOG_CATEGORY_ERROR, "%s: %s",
				name,
				PHYSFS_getLastError());
	}

	return writect;
}

void Disk_SetWritePathBase()
{
	PHYSFS_setWriteDir(PHYSFS_getBaseDir());
}

void Disk_SetWritePathUser()
{
	PHYSFS_setWriteDir(PHYSFS_getUserDir());
}

const char* Disk_DirSlash()
{
	return PHYSFS_getDirSeparator();
}
