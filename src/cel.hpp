#ifndef __CEL_H__
#define __CEL_H__

#include <string>
#include "data.hpp"

struct Cel;
struct SDL_Point;

bool Cel_TrimPathIntoName(std::string & path);
bool Cel_Load(const char *name, Data indata = Data());
int Cel_LoadBatch(size_t ct, const char **names, const char *prefix);
const Cel* Cel_Get(const char *name);
void AllCels_Unload();

//SDL_Texture* Cel_Texture(const Cel *cel);
size_t Cel_FrameCt(const Cel *cel);
int Cel_FrameTime(const Cel *cel, int frame);
void Cel_FrameSize(const Cel *cel, int frame, int *w, int *h);
//const SDL_Rect* Cel_FrameRect(const Cel *cel, int frame);
//const SDL_Rect* Cel_FrameRectByID(const Cel *cel, unsigned frameid);
void Cel_DrawFrame(const Cel *cel, int frame, const SDL_Point *center_px);

#endif
