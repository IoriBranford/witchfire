#ifndef __TEXTURE_H__
#define __TEXTURE_H__

struct SDL_Surface;
unsigned int Texture_CreateFromSurface(SDL_Surface *surface);
unsigned int Texture_FOpen(const char *path,
		int *outwidth = 0, int *outheight = 0);
void Texture_Delete(unsigned int texture);

#endif
