#include "ui_internal.hpp"
#include <map>
#include <string>

static std::map<std::string, ActionFunc> ui_allactions;

#include "app.hpp"
#include "play.hpp"
#include "stats.hpp"

static void StartCampaign(UIWidget *widget)
{
	Stats_Reset();
	App_RequestPhaseChange(Play_LoadAssets,
			Play_Init, Play_Tick, Play_PreRender);
}

static void ExitApp(UIWidget *widget)
{
	App_RequestQuit();
}

void UIAction_Register(const char *name, ActionFunc func)
{
	ui_allactions[name] = func;
}

void UIAction_Call(const char *name, UIWidget *widget)
{
	std::map<std::string, ActionFunc>::iterator it
		= ui_allactions.find(name);

	if (it != ui_allactions.end())
		it->second(widget);
}

void UIAction_RegisterMain()
{
	UIACTION_REGISTER(StartCampaign);
	UIACTION_REGISTER(ExitApp);
}

ActionFunc UIAction_Find(const char *name)
{
	std::map<std::string, ActionFunc>::iterator it
		= ui_allactions.find(name);

	if (it != ui_allactions.end())
		return it->second;

	return NULL;
}
