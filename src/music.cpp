#include "music.hpp"

#include "SDL.h"
#include "SDL_mixer.h"
#include <gme/gme.h>

#include "disk.hpp"

#include "units.hpp"

static const int NO_FADE = 1;

struct Music
{
	Music_Emu *emu;
	int volume;
	std::string nextmusic;
	int fadetimeleft_tk;
	int fadetime_tk;

	Music() :
		emu(NULL),
		volume(MIX_MAX_VOLUME),
		nextmusic(),
		fadetimeleft_tk(NO_FADE),
		fadetime_tk(NO_FADE)
	{
	}
};

static Music music_player;

static Mix_Music *music_wav;
static std::string music_filetext; // OGG must be kept in memory while playing

static const char *MUSIC_DIR = "mus/";
//static const char *MUSIC_EXT = ".vgm";

static const int MUSIC_RATE = 44100;

static void Music_PlayCallback(void *data, Uint8 *bytes, int bytect)
{
	Sint16 *samples = (Sint16*)bytes;
	int samplect = bytect >> 1;

	Music *musicplayer = (Music*)data;
	int volume = musicplayer->volume;
	int fadetimeleft = musicplayer->fadetimeleft_tk;
	int fadetime = musicplayer->fadetime_tk;

	gme_play(musicplayer->emu, samplect, samples);

	for (int i = 0; i < samplect; ++i)
	{
		int sample = samples[i];
		sample = sample * volume / MIX_MAX_VOLUME;
		sample = sample * fadetimeleft / fadetime;
		samples[i] = sample;
	}
}

void Music_Init()
{
	music_player = Music();
	music_wav = NULL;
	music_filetext.clear();
}

bool Music_Load(const char *name)
{
	std::string path (MUSIC_DIR);
	path += name;
	//path += MUSIC_EXT;

	return Music_FOpen(path.c_str());
}

bool Music_FOpen(const char *path)
{
	Music_Uninit();

	Music_Emu *emu = NULL;
	Mix_Music *wav = NULL;
	gme_err_t emu_error = NULL;
	const char *wav_error = NULL;
	gme_type_t emu_type = gme_identify_extension(path);

	bool success = false;

	// TODO for VGZ inflate with zlib

	if (emu_type)
	{
		emu = gme_new_emu(emu_type, MUSIC_RATE);

		if (emu != NULL && Disk_TextOpen(path, music_filetext) > 0)
		{
			emu_error = gme_load_data(emu,
					music_filetext.c_str(),
					music_filetext.length());
			music_filetext.clear();
			success = (emu_error == NULL);
		}
	}
	else
	{
		SDL_RWops *rwops = NULL;

		if (Disk_TextOpen(path, music_filetext) > 0)
		{
			rwops = SDL_RWFromConstMem(music_filetext.c_str(),
					music_filetext.length());
		}

		if (rwops)
			wav = Mix_LoadMUS_RW(rwops, 1);

		success = (rwops && wav);
		if (!success)
			wav_error = Mix_GetError();
	}

	if (!success)
	{
		if (emu)
			gme_delete(emu);

		if (emu_error)
			SDL_LogCritical(SDL_LOG_CATEGORY_AUDIO, emu_error);
		if (wav_error)
			SDL_LogCritical(SDL_LOG_CATEGORY_AUDIO, wav_error);
	}
	else
	{
		music_player.emu = emu;
		music_wav = wav;
	}

	return success;
}

void Music_Start(int track)
{
	if (music_player.emu)
	{
		gme_err_t error = gme_start_track(music_player.emu, track);
		if (error)
		{
			SDL_LogError(SDL_LOG_CATEGORY_AUDIO, error);
		}
		else
		{
			Music_SetPlaying(true);
			Music_Fade(NO_FADE, NULL);
		}
	}
	else if (music_wav)
	{
		Mix_HookMusic(NULL, NULL);
		Mix_PlayMusic(music_wav, 1);
	}
}

void Music_SetPlaying(bool playing)
{
	if (music_player.emu)
	{
		if (playing)
			Mix_HookMusic(Music_PlayCallback, &music_player);
		else
			Mix_HookMusic(NULL, NULL);
	}
	else if (music_wav)
	{
		if (playing)
			Mix_ResumeMusic();
		else
			Mix_PauseMusic();
	}
}

void Music_SetVolume(int volume)
{
	music_player.volume = volume;
	Mix_VolumeMusic(volume);
}

void Music_Fade(int time_tk, const char *nextmusic)
{
	music_player.fadetimeleft_tk = time_tk;
	music_player.fadetime_tk = time_tk;

	if (nextmusic)
		music_player.nextmusic = nextmusic;

	if (music_wav)
		Mix_FadeOutMusic(time_tk * MS_PER_TICK);
}

void Music_Tick()
{
	if (music_player.fadetimeleft_tk > NO_FADE)
		--music_player.fadetimeleft_tk;

	if (!music_player.nextmusic.empty())
	{
		if (Music_Finished())
		{
			if (Music_Load(music_player.nextmusic.c_str()))
				Music_Start();

			music_player.nextmusic.clear();
		}
	}
}

bool Music_Finished()
{
	if (music_player.emu)
	{
		return gme_track_ended(music_player.emu)
		|| (music_player.fadetime_tk > NO_FADE
				&& music_player.fadetimeleft_tk <= NO_FADE);
	}

	if (music_wav)
	{
		return !Mix_PlayingMusic();
	}

	return true;
}

void Music_Uninit()
{
	Music_SetPlaying(false);
	Mix_HaltMusic();

	if (music_player.emu)
	{
		gme_delete(music_player.emu);
		music_player.emu = NULL;
	}

	if (music_wav)
	{
		Mix_FreeMusic(music_wav);
		music_wav = NULL;
	}

	music_filetext.clear();
}
