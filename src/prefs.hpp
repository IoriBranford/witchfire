#ifndef __PREFS_H__
#define __PREFS_H__

#include "SDL.h"

struct Prefs
{
	int windowwidth;
	int windowheight;
	Uint32 windowflags;
	Uint32 rendererflags;
};

void Prefs_Init();

int Prefs_ParseArgs(int argct, char **args);

const Prefs* Prefs_Get();

#endif
