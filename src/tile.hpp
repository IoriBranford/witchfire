#ifndef __TILE_H__
#define __TILE_H__

struct TileLayer;

struct SDL_Rect;
struct SDL_Point;

class Data;

TileLayer* TileLayer_New(const Data &layer, const Data &tilesets, int depth);

void TileMap_Init();

void TileMap_TranslateView(int dx_mi, int dy_mi);
bool TileMap_RectInView(const SDL_Rect *rect_mi);
void TileMap_ToScreenPos(SDL_Point *pos_mi);

void TileMap_SetLoopRect(const SDL_Rect *rect_mi);
void TileMap_DeactivateLoop();

void TileMap_Draw();
void TileMap_Clear();

#endif
