#include "play.hpp"
#include "play_internal.hpp"

#include "app.hpp"
#include "input.hpp"
//#include "timeline.hpp"
#include "cel.hpp"
#include "sound.hpp"
#include "music.hpp"
#include "script.hpp"
#include "database.hpp"

#include "ship.hpp"
#include "bullet.hpp"
#include "stage.hpp"
#include "mapdata.hpp"
#include "stats.hpp"

#include "ui.hpp"

#include "alist.hpp"

#include "units.hpp"

#include "SDL.h"

#include "title.hpp"

static PlayPhase play_phase;
static int play_digitalflags;
static float play_analogvalues[ANALOG_COUNT];
static bool play_analogvaluesonce;

int PlayerInput(lua_State *machine)
{
	if (AllShips_PlayerDying())
	{
		ScriptFunc_Return(machine, 0.0);
		ScriptFunc_Return(machine, 0.0);
		ScriptFunc_Return(machine, false);
		ScriptFunc_Return(machine, false);
	}
	else
	{
		ScriptFunc_Return(machine, (double)play_analogvalues[ANALOG_X]);
		ScriptFunc_Return(machine, (double)play_analogvalues[ANALOG_Y]);
		if (play_analogvaluesonce)
		{
			play_analogvaluesonce = false;
			play_analogvalues[ANALOG_X] = 0;
			play_analogvalues[ANALOG_Y] = 0;
		}

		ScriptFunc_Return(machine,
			(play_digitalflags & (1<<DIGITAL_FIRE_LEFT)) != 0);
		ScriptFunc_Return(machine,
			(play_digitalflags & (1<<DIGITAL_FIRE_RIGHT)) != 0);
	}
	return 4;
}

void Play_ApplyDigitalToAnalog(int digitalcommand)
{
	int analogcommand = digitalcommand >> 1;
	int negdigitalflag = 1 << (2 * analogcommand);
	int posdigitalflag = negdigitalflag << 1;

	play_analogvalues[analogcommand] =
		((play_digitalflags & posdigitalflag) != 0)
		- ((play_digitalflags & negdigitalflag) != 0);
	play_analogvalues[analogcommand] *= PLAYER_DIGITAL_SPEED;
}

void Play_OnDigitalCommand(int command, bool on)
{
	if (on)
		play_digitalflags |= (1<<command);
	else
		play_digitalflags &= ~(1<<command);

	switch (command)
	{
	case DIGITAL_LEFT:
	case DIGITAL_RIGHT:
	case DIGITAL_UP:
	case DIGITAL_DOWN:
		Play_ApplyDigitalToAnalog(command);
		break;
	}
}

void Play_OnAnalogCommand(int command, float v)
{
	play_analogvalues[command % ANALOG_COUNT] = v * PLAYER_DIGITAL_SPEED;
}

void Play_OnPointCommand(int commandflags, float x, float y)
{
	play_analogvaluesonce = true;
	play_analogvalues[ANALOG_X] += PixelToMicron(x);
	play_analogvalues[ANALOG_Y] += PixelToMicron(y);
}

void Play_ReceivePlayerCommands(int &digitalflags, float analogvalues[])
{
	digitalflags = play_digitalflags;
	analogvalues[ANALOG_X] = play_analogvalues[ANALOG_X];
	analogvalues[ANALOG_Y] = play_analogvalues[ANALOG_Y];

	if (play_analogvaluesonce)
	{
		play_analogvaluesonce = false;
		play_analogvalues[ANALOG_X] = 0;
		play_analogvalues[ANALOG_Y] = 0;
	}
}

void Play_IncScore(unsigned int points)
{
	if (Stats_WillExtend(points))
	{
		Stats_Extend();
		PlayUI_UpdateLives();
		Sound_Play(Sound_Get("extend"));
	}

	Stats_IncScore(points);

	PlayUI_UpdateScore();
}

void Play_LoadAssets()
{
	Database_LoadInto("commonobjects", "objects",
			MAPDATAOPEN_LOADTILESETIMAGES);

	Database_LoadInto("stageobjects", "objects", 0);

	UI_Load("play_hud");

	ShipSeq_Init(256);
	Stage_Load("caravan");

	const luaL_Reg LIB[] =
	{
		SCRIPTFUNC_REG(PlayerInput),
		{NULL, NULL}
	};

	ScriptFunc_RegisterLib("Game", LIB);

	Sound_Load("extend");
}

void Play_Init()
{
	play_phase = PHASE_PLAY;
	play_digitalflags = 0;
	play_analogvalues[ANALOG_X] = play_analogvalues[ANALOG_Y] = 0;
	play_analogvaluesonce = false;

	Input_SetDigitalCallback(Play_OnDigitalCommand);
	Input_SetAnalogCallback(Play_OnAnalogCommand);
	Input_SetPointCallback(Play_OnPointCommand);

	//ShipSeqTest_Init();
	Stage_Init();

	AllShips_Init(32);
	if (AllShips_NoPlayer())
		AllShips_SpawnPlayer();

	AllBullets_Init(128);

	PlayUI_Init();

	Music_Start();
}

void Play_Tick()
{
	Stage_Tick();
	ShipSeq_Tick();

	if (AllShips_NoPlayer())
	{
		if (Stats_Lives() > 0)
		{
			Stats_DecLives();
			PlayUI_UpdateLives();
		}

		if (Stats_Lives() <= 0)
		{
			if (play_phase == PHASE_PLAY)
			{
				Music_Load("gameover.vgm");
				Music_Start();
				play_phase = PHASE_GAMEOVER;
			}
		}
		else
		{
			AllShips_SpawnPlayer();
		}
	}

	if (AllShips_NoEnemy())
	{
		if (ShipSeq_Finished())
		{
			if (play_phase == PHASE_PLAY)
			{
				Music_Load("clear.vgm");
				Music_Start();
				play_phase = PHASE_CLEAR;
			}
		}
		else if (!ShipSeq_Waiting())
		{
			ShipSeq_IncGroup();
		}
	}

	AllShips_Tick();
	AllBullets_Tick();

	if (Music_Finished())
	{
		if (play_phase == PHASE_CLEAR)
		{
			if (Stats_IncStage() > 6)
			{
				App_RequestPhaseChange(Title_LoadAssets,
						Title_Init,
						Title_Tick,
						Title_PreRender);
			}
			else
			{
				App_RequestPhaseChange(Play_LoadAssets,
						Play_Init,
						Play_Tick,
						Play_PreRender);
			}
		}
		else
		if (play_phase == PHASE_GAMEOVER)
		{
			App_RequestPhaseChange(Title_LoadAssets,
					Title_Init,
					Title_Tick,
					Title_PreRender);
		}
	}
}

void Play_PreRender(int dt)
{
	AllShips_PreRender(dt);
	AllBullets_PreRender(dt);
}
