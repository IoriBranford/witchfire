#include "ui.hpp"
#include "music.hpp"

void Title_LoadAssets()
{
	UI_Load("title_menu");
}

void Title_Init()
{
	UICursor_Init();
	Music_Start();
}

void Title_Tick()
{
}

void Title_PreRender(int dt)
{
}
