#include "data.hpp"
#include "database.hpp"
#include "mapdata.hpp"

#include <string>
#include <map>
#include <cstdlib>

static const char *DATABASE_DIR = "db/";

typedef std::map<std::string, Data> Database;
typedef std::map<std::string, Database> AllDatabases;

static AllDatabases alldatabases;
static Database *database_nowloading = NULL;

static void Database_ReadObject(Data object, Data tilesets)
{
	std::string name = object.Find("name").CStr();
	if (name.empty())
		return;

	Database &db = *database_nowloading;
	Database::iterator dit = db.find(name);
	if (dit != db.end())
		dit->second.Close();

	Data properties = object.Find("properties").Clone();

	for (Data p = properties.Begin(); p != properties.End(); ++p)
	{
		Data newp;

		if (!std::strcmp(p.CStr(), "true"))
			newp = Data(true);
		else if (!std::strcmp(p.CStr(), "false"))
			newp = Data(false);
		else
		{
			char *afternumber;
			double number = std::strtod(p.CStr(), &afternumber);

			if (!*afternumber)
				newp = Data(number);
		}

		if (newp)
		{
			properties.Replace(p, p.Name(), newp);
			p = newp;
		}
	}

	Data gid = object.Find("gid");
	Data tileset;

	if (gid)
		tileset = MapData_FindGIDTileset(tilesets, gid.Int());

	if (tileset)
	{
		Data cel = tileset.Find("cel");
		properties.InsAfter(properties.End(),
				"cel", cel.Clone());
	}

	db[name] = properties;
}

static void Database_ReadLayer(Data layer, Data tilesets)
{
	const char *type = layer.Find("type").CStr();

	if (!std::strcmp(type, "objectgroup"))
	{
		Data os = layer.Find("objects");
		if (os)
			for (Data o = os.Begin(); o != os.End(); ++o)
				Database_ReadObject(o, tilesets);
	}
}

static void Database_ReadProperty(Data property)
{
}

//static void Database_Unload(AllDatabases::iterator dbit)
//{
//	if (dbit == alldatabases.end())
//		return;
//
//	Database &db = dbit->second;
//
//	for (Database::iterator dit = db.begin(); dit != db.end(); ++dit)
//		dit->second.Close();
//
//	alldatabases.erase(dbit);
//}

bool Database_LoadInto(const char *filename, const char *dbname,
		int mapdataopenflags)
{
	std::string path (DATABASE_DIR);
	path += filename;
	path += Data::EXT;

	AllDatabases::iterator dbit = alldatabases.find(dbname);
	if (dbit == alldatabases.end())
		//Database_Unload(dbit);
		alldatabases[dbname] = Database();

	database_nowloading = &alldatabases[dbname];

	bool success = MapData_FOpen(path.c_str(), mapdataopenflags,
			Database_ReadLayer, Database_ReadProperty);

	database_nowloading = NULL;
	return success;
}

Data Database_Get(const char *dbname, const char *dataname)
{
	AllDatabases::iterator dbit;
	dbit = alldatabases.find(dbname);
	if (dbit == alldatabases.end())
		return Data();

	Database &db = dbit->second;

	Database::iterator dit = db.find(dataname);
	if (dit == db.end())
		return Data();

	return dit->second;
}

/*
static const char *DATABASE_NAMES[] =
{
	"ships",
	NULL
};

bool AllDatabases_Load()
{
	bool success = true;
	for (int i = 0; success && DATABASE_NAMES[i]; ++i)
	{
		success = Database_Load(DATABASE_NAMES[i]);
	}
	return success;
}
*/

void AllDatabases_Unload()
{
	for (AllDatabases::iterator dbit = alldatabases.begin();
			dbit != alldatabases.end();
			++dbit)
	{
		Database &db = dbit->second;

		for (Database::iterator dit = db.begin();
				dit != db.end();
				++dit)
		{
			dit->second.Close();
		}
	}

	alldatabases.clear();
}
