#include "data.hpp"
#include "disk.hpp"

#include <cstdlib>

bool Data::FOpenXML(const char *path)
{
	std::string text;
	int readct = Disk_TextOpen(path, text);
	if (readct <= 0)
		return false;

	return ParseXML(text.c_str());
}

int Data::ParseXMLElement(yxml_t &xml, const char **text)
{
	Data prevchild = Begin();
	if (prevchild)
		while (prevchild.json->next)
			++prevchild;

	std::string newchildname;
	std::string newchildval;

	Data newchild;

	yxml_ret_t status = YXML_OK;

	for (; **text && status >= YXML_OK && status != YXML_ELEMEND; ++*text)
	{
		status = yxml_parse(&xml, **text);

		switch (status)
		{
		case YXML_ATTRSTART:
			newchildname = xml.attr;
			break;

		case YXML_ATTRVAL:
			newchildval += xml.data;
			break;

		case YXML_ATTREND:
			double valdouble;
			char *afterval;

			valdouble = std::strtod(newchildval.c_str(), &afterval);

			if (!*afterval)
				newchild = Data(valdouble);
			else if (newchildval == "true")
				newchild = Data(true);
			else if (newchildval == "false")
				newchild = Data(false);
			else
				newchild = Data(newchildval.c_str());

			newchildval.clear();
			break;

		case YXML_ELEMSTART:
			newchildname = xml.elem;
			newchild.InitObj();
			if (newchild.ParseXMLElement(xml, text) != YXML_ELEMEND)
			{
				newchild.Close();
				newchild = Data();
			}
			break;

		default:
			;
		}

		if (newchild)
		{
			InsAfter(prevchild, newchildname.c_str(),
					newchild);

			prevchild = newchild;
			newchild = End();
			newchildname.clear();
		}
	}

	return status;
}

bool Data::ParseXML(const char *text)
{
	const int XML_BUFFER_SIZE = 4096;
	yxml_t xml;
	char xmlbuffer[XML_BUFFER_SIZE];
	yxml_init(&xml, &xmlbuffer, XML_BUFFER_SIZE);

	if (!*this)
		InitObj();

	int status = ParseXMLElement(xml, &text);

	return status >= YXML_OK;
}
