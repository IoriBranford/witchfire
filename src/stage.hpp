#ifndef __STAGE_H__
#define __STAGE_H__

bool Stage_Load(const char *name);
void Stage_Init();
void Stage_Tick();
void Stage_PreRender(int dt);

#endif
