#include "cel.hpp"

#include <map>
#include <vector>
#include <cassert>

#include "SDL.h"
#include "SDL_image.h"

#include "render.hpp"
#include "texture.hpp"
#include "disk.hpp"
#include "data.hpp"
#include "app.hpp"

#include "units.hpp"

struct Frame
{
	int time;
	int w, h;
	struct { float x, y, w, h; } texcoords;

	Frame()
	{
		time = 1;
		w = h = 0;
		texcoords.x = 
		texcoords.y = 
		texcoords.w = 
		texcoords.h = 
			0;
	}
};

struct Cel
{
	unsigned int texture;
	std::vector<Frame> frames;
	std::map<unsigned, unsigned> frameidstoidxs;

	Cel() :
		texture(0), frames(), frameidstoidxs()
	{
	}
};

static std::map<std::string, Cel> allcels;

static const char *CEL_DIR = "cel/";
static const char *CEL_EXT = ".png";

bool Cel_TrimPathIntoName(std::string & path)
{
	// TilEd JSON escapes '/'
	for (size_t escslashpos = path.find("\\/");
			escslashpos != std::string::npos;
			escslashpos = path.find("\\/"))
	{
		path.replace(escslashpos, 2, "/");
	}

	size_t extpos = path.rfind(CEL_EXT);
	if (extpos != std::string::npos)
		path.erase(extpos);

	size_t dirpos = path.find(CEL_DIR);
	if (dirpos != std::string::npos)
		path.erase(0, dirpos + std::strlen(CEL_DIR));

	return (extpos != std::string::npos || dirpos != std::string::npos);
}

static size_t Cel_ReadFrames(Cel &cel, Data frames,
		float celw, float celh)
{
	assert(frames);

	for (Data d = frames.Begin(); d; ++d)
	{
		Frame frame;

		Data duration = d.Find("duration");
		if (duration)
			frame.time = duration.Int() / MS_PER_TICK;

		Data id = d.Find("id");

		Data fd = d.Find("frame");
		frame.w = fd.Find("w").Int();
		frame.h = fd.Find("h").Int();
		frame.texcoords.x = fd.Find("x").Float();
		frame.texcoords.y = fd.Find("y").Float();
		frame.texcoords.w = frame.w;
		frame.texcoords.h = frame.h;

		frame.texcoords.x /= celw;
		frame.texcoords.y /= celh;
		frame.texcoords.w /= celw;
		frame.texcoords.h /= celh;

		if (id)
		{
			cel.frameidstoidxs[(unsigned)id.Int()] =
				cel.frames.size();
		}

		cel.frames.push_back(frame);
	}

	return cel.frames.size();
}

bool Cel_Load(const char *name, Data indata)
{
	std::string key (name);
	std::map<std::string, Cel>::iterator it;

	it = allcels.find(key);
	if (it != allcels.end())
		return (it->second.texture != 0);

	std::string path (CEL_DIR);
	path += name;
	path += Data::EXT;

	Data framesdata;
	Data data;
	if (indata)
		data = indata;

	if (data || (Disk_FileExists(path.c_str()) && data.FOpen(path)))
	{
		framesdata = data.Find("frames");

		Data metadata = data.Find("meta");
		Data celdata = metadata.Find("image");

		path = (CEL_DIR);
		path += name;
		size_t n = path.rfind('/');
		if (n != std::string::npos)
		{
			path.erase(n);
		}
		path += '/';
		path += celdata.CStr();
	}
	else
	{
		path = (CEL_DIR);
		path += name;
		path += CEL_EXT;
	}

	int width, height;
	unsigned int texture = Texture_FOpen(path.c_str(), &width, &height);

	if (texture)
	{
		allcels[name] = Cel();
		Cel &cel = allcels[name];
		cel.texture = texture;

		if (framesdata)
		{
			Cel_ReadFrames(cel, framesdata, width, height);
		}
		else
		{
			Frame frame;
			frame.w = width;
			frame.h = height;
			frame.texcoords.w = 1;
			frame.texcoords.h = 1;
			cel.frames.push_back(frame);
		}
	}

	if (!indata)
		data.Close();

	return texture != 0;
}

int Cel_LoadBatch(size_t ct, const char **names, const char *prefix)
{
	int got = 0;
	std::string name;
	size_t prefixlen = 0;
	if (prefix)
	{
		name += prefix;
		prefixlen = name.length();
	}

	for (size_t i = 0; i < ct; ++i)
	{
		if (names[i])
		{
			name += names[i];

			if (Cel_Load(name.c_str()))
				++got;

			if (prefixlen)
				name.erase(prefixlen, std::string::npos);
		}
	}

	return got;
}

const Cel* Cel_Get(const char *name)
{
	std::map<std::string, Cel>::iterator it;
	std::string key (name);
	it = allcels.find(key);
	if (it != allcels.end())
		return &it->second;
	return NULL;
}

void AllCels_Unload()
{
	std::map<std::string, Cel>::iterator it;
	for (it = allcels.begin(); it != allcels.end(); ++it)
	{
		//SDL_DestroyTexture(it->second.texture);
		Texture_Delete(it->second.texture);
	}
	allcels.clear();
}

//SDL_Texture* Cel_Texture(const Cel *cel)
//{
//	return cel->texture;
//}

size_t Cel_FrameCt(const Cel *cel)
{
	return cel->frames.size();
}

int Cel_FrameTime(const Cel *cel, int frame)
{
	return cel->frames[frame % Cel_FrameCt(cel)].time;
}
/*
const SDL_Rect* Cel_FrameRect(const Cel *cel, int frame)
{
	size_t framect = cel->frames.size();
	frame %= framect;
	return &cel->frames[frame].texcoords;
}

const SDL_Rect* Cel_FrameRectByID(const Cel *cel, unsigned frameid)
{
	std::map<unsigned, unsigned>::const_iterator it;
	it = cel->frameidstoidxs.find(frameid);
	if (it == cel->frameidstoidxs.end())
		return NULL;
	return &cel->frames[it->second].texcoords;
}
*/

void Cel_FrameSize(const Cel *cel, int frame, int *w, int *h)
{
	frame %= cel->frames.size();
	*w = cel->frames[frame].w;
	*h = cel->frames[frame].h;
}

void Cel_DrawFrame(const Cel *cel, int f, const SDL_Point *center_px)
{
	f %= cel->frames.size();
	const Frame &frame = cel->frames[f];
	Render_SetDrawingTexture(cel->texture);
	Render_BeginDrawing();

	int l = 0;
	int t = 0;
	if (center_px)
	{
		l -= center_px->x;
		t -= center_px->y;
	}

	Render_DrawQuad(l, t, l + frame.w, t + frame.h,
			frame.texcoords.x, frame.texcoords.y,
			frame.texcoords.x + frame.texcoords.w,
			frame.texcoords.y + frame.texcoords.h);
	Render_EndDrawing();
}
