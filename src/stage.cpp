#include "stage_internal.hpp"

#include "tile.hpp"
#include "units.hpp"

SDL_Point stage_vel_mi;

void Stage_Init()
{
	//TEST
	SDL_Rect looprect =
	{
		70*8,
		0*8,
		48*8,
		30*8
	};
	TileMap_SetLoopRect(&looprect);

	StageScript_RegisterLib();
}

void Stage_Tick()
{
	TileMap_TranslateView(stage_vel_mi.x, stage_vel_mi.y);
}

void Stage_PreRender(int dt)
{
	//TileMap_TranslateView(6 * dt, 0);
}
