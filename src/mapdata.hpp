#ifndef __MAPDATA_H__
#define __MAPDATA_H__

#include "data.hpp"

typedef void (*MapLayerCallback)(Data layer, Data tilesets);
typedef void (*MapPropertyCallback)(Data property);

enum MapDataOpenFlag
{
	MAPDATAOPEN_LOADTILESETIMAGES	= 1<<0,
	MAPDATAOPEN_REVERSELAYERS	= 1<<1
};

bool MapData_FOpen(const char *path, int flags,
		MapLayerCallback maplayercallback = NULL,
		MapPropertyCallback mappropcallback = NULL);

Data MapData_FindGIDTileset(Data tilesets, int gid);

#endif
