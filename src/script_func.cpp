#include "script.hpp"
#include "script_internal.hpp"

void ScriptFunc_RegisterFunc(const char *funcname, lua_CFunction func)
{
	lua_pushcfunction(script_machine, func);
	lua_setglobal(script_machine, funcname);
}

void ScriptFunc_RegisterLib(const char *libname, const luaL_Reg *lib)
{
	//Lua5.1:
	luaL_register(script_machine, libname, lib);
	lua_pop(script_machine, 1);
	//Lua5.2:
	//luaL_newlib(script_machine, lib);
	//lua_setglobal(script_machine, libname);
}

template <> const char* ScriptFunc_Arg(lua_State *machine, const char *argname,
		const char *defaultvalue)
{
	lua_getfield(machine, 1, argname);
	const char *value = luaL_optstring(machine, -1, defaultvalue);
	lua_pop(machine, 1);
	return value;
}

template <> double ScriptFunc_Arg(lua_State *machine, const char *argname,
		double defaultvalue)
{
	lua_getfield(machine, 1, argname);
	double value = luaL_optnumber(machine, -1, defaultvalue);
	lua_pop(machine, 1);
	return value;
}

template <> void* ScriptFunc_Arg(lua_State *machine, const char *argname,
		void* defaultvalue)
{
	lua_getfield(machine, 1, argname);
	void *value = defaultvalue;
	if (lua_islightuserdata(machine, -1))
		value = lua_touserdata(machine, -1);
	lua_pop(machine, 1);
	return value;
}

template <> void ScriptFunc_Return(lua_State *machine, const char *value)
{
	if (value)
		lua_pushstring(machine, value);
	else
		lua_pushnil(machine);
}

template <> void ScriptFunc_Return(lua_State *machine, double value)
{
	lua_pushnumber(machine, value);
}

template <> void ScriptFunc_Return(lua_State *machine, bool value)
{
	lua_pushboolean(machine, value);
}

template <> void ScriptFunc_Return(lua_State *machine, void *value)
{
	lua_pushlightuserdata(machine, value);
}

void ScriptFunc_ReturnNil(lua_State *machine)
{
	lua_pushnil(machine);
}

