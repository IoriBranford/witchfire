#include "sprite.hpp"
#include "cel.hpp"
#include "render.hpp"
#include "SDL.h"

struct Sprite
{
	SDL_Rect dstrect_px;

	const Cel *cel;
	SDL_RendererFlip flip;

	int frame;
	int frametime;
	int loopct;

	float angle_deg;
	SDL_Point center_px;

	SDL_BlendMode blendmode;

	int drawflags;

	Sprite(const SDL_Rect *dr		= NULL,
		const Cel *cel		= NULL,
		int frm				= 0,
		const SDL_RendererFlip f	= SDL_FLIP_NONE,
		const float a			= 0,
		const SDL_Point *c		= NULL);
};

const int DRAW_DEFAULT_FLAGS = DRAW_TEX;//|DRAW_RECT;

Sprite::Sprite( const SDL_Rect *dr,
		const Cel *cel,
		int frm,
		const SDL_RendererFlip f,
		const float a,
		const SDL_Point *c)
{
	if (dr)
	{
		this->dstrect_px = *dr;
	}
	else
	{
		this->dstrect_px.x = this->dstrect_px.y = 0;
		this->dstrect_px.w = this->dstrect_px.h = 0;
	}

	this->cel = cel;

	frame = frm;
	frametime = 0;
	loopct = 0;

	this->flip = f;

	if (this->cel)
	{
		if (!this->dstrect_px.w && !this->dstrect_px.h)
		{
			//dstrect_px.w = Cel_FrameRect(cel, frame)->w;
			//dstrect_px.h = Cel_FrameRect(cel, frame)->h;
			Cel_FrameSize(cel, frame,
					&dstrect_px.w, &dstrect_px.h);
		}
	}

	this->angle_deg = a;
	if (c)
	{
		this->center_px = *c;
	}
	else
	{
		this->center_px.x = this->dstrect_px.w >> 1;
		this->center_px.y = this->dstrect_px.h >> 1;
	}

	this->blendmode = SDL_BLENDMODE_BLEND;
	this->drawflags = DRAW_DEFAULT_FLAGS;
}

void Sprite_SetCel(Sprite *sprite, const Cel *cel)
{
	sprite->cel = cel;
}

void Sprite_SetAnimation(Sprite *sprite, int loopct, int startframe)
{
	if (!sprite->cel)
		return;

	int framect = (int)Cel_FrameCt(sprite->cel);
	sprite->frame = ((startframe % framect) + framect) % framect;
	sprite->frametime = 0;
	sprite->loopct = loopct;
}

bool Sprite_AnimStopped(Sprite *sprite)
{
	return sprite->loopct == ANIMLOOP_STOP;
}

void Sprite_SetAngle(Sprite *sprite, float angle_deg)
{
	sprite->angle_deg = angle_deg;
}

void Sprite_SetCenter(Sprite *sprite, const SDL_Point *center_px)
{
	sprite->center_px = *center_px;
}

void Sprite_SetBlendMode(Sprite *sprite, SDL_BlendMode blendmode)
{
	sprite->blendmode = blendmode;
}

void Sprite_EnableDrawFlags(Sprite *sprite, int drawflags)
{
	sprite->drawflags |= drawflags;
}

void Sprite_DisableDrawFlags(Sprite *sprite, int drawflags)
{
	sprite->drawflags &= ~drawflags;
}

static void Sprite_AdvanceFrame(Sprite &sprite, int dt)
{
	if (!sprite.cel)
		return;
	
	if (Cel_FrameCt(sprite.cel) < 2)
		return;

	if (sprite.loopct == ANIMLOOP_STOP)
		return;

	sprite.frametime += dt;

	int framect = Cel_FrameCt(sprite.cel);
	for (int frametime = Cel_FrameTime(sprite.cel,sprite.frame);
		sprite.frametime >= frametime;
		frametime = Cel_FrameTime(sprite.cel, sprite.frame))
	{
		sprite.frametime -= frametime;
		++sprite.frame;
		if (sprite.frame >= framect)
		{
			if (sprite.loopct > 0)
				--sprite.loopct;

			if (sprite.loopct == ANIMLOOP_STOP)
				sprite.frame = framect - 1;
			else
				sprite.frame %= framect;
		}
	}

	Cel_FrameSize(sprite.cel, sprite.frame,
			&sprite.dstrect_px.w, &sprite.dstrect_px.h);
	//const SDL_Rect *framerect = Cel_FrameRect(sprite.cel,
	//		sprite.frame);
	//sprite.dstrect_px.w = framerect->w;
	//sprite.dstrect_px.h = framerect->h;
}

static void Sprite_Draw(Sprite &sprite)
{
	if (sprite.cel && (sprite.drawflags & DRAW_TEX))
	{
		//SDL_SetTextureBlendMode(texture, sprite.blendmode);
		Render_SetDrawingTransform(
				sprite.dstrect_px.x + sprite.center_px.x,
				sprite.dstrect_px.y + sprite.center_px.y,
				0, sprite.angle_deg);

		Cel_DrawFrame(sprite.cel, sprite.frame, &sprite.center_px);
	}

	if (sprite.drawflags & DRAW_RECT || !sprite.cel)
	{
		//SDL_RenderDrawRect(renderer, &sprite.dstrect_px);
	}
}

#include "alist.hpp"

typedef AList<Sprite> SpriteList;
static SpriteList allsprites;

#include <climits>
static const int DEL_MARK = INT_MIN;

void AllSprites_Init(int n)
{
	allsprites.Init(n);
}

static bool Sprite_ShouldDel(const Sprite &sprite)
{
	return sprite.dstrect_px.x == DEL_MARK;
}

void AllSprites_Prune()
{
	allsprites.DelIf(Sprite_ShouldDel);
}

void AllSprites_PreRender(int dt)
{
	allsprites.ForEach(Sprite_AdvanceFrame, dt);
}

void AllSprites_Draw()
{
	allsprites.ForEach(Sprite_Draw);
}

bool AllSprites_Full()
{
	return allsprites.Full();
}

Sprite* Sprite_New( const SDL_Rect *dr,
		const Cel *cel,
		int frm,
		const SDL_RendererFlip f,
		const float a,
		const SDL_Point *c)
{
	return allsprites.Push(Sprite(dr, cel, frm, f, a, c));
}

void Sprite_Del(Sprite *sprite)
{
	sprite->dstrect_px.x = DEL_MARK;
}

#include "body.hpp"
#include "units.hpp"

void Sprite_SetToBody(Sprite *sprite, const Body *body)
{
	Body_SetSpriteDstRect(body, &sprite->dstrect_px);
	const SDL_Point *bodypos = Body_Pos_Micron(body);
	sprite->center_px.x = MicronToPixel(bodypos->x) - sprite->dstrect_px.x;
	sprite->center_px.y = MicronToPixel(bodypos->y) - sprite->dstrect_px.y;
}
