#ifndef __UTF8_H__
#define __UTF8_H__

#ifdef __cplusplus
extern "C" {
#endif

void UCharToUTF8seq(char *utf8seq, unsigned uchar);
unsigned UCharToUTF8int(unsigned uchar);
unsigned UTF8seqToUTF8int(const unsigned char *utf8seq, unsigned *utf8int);

#ifdef __cplusplus
}//extern "C" {
#endif

#endif
