#ifndef __SCRIPT_INTERNAL_H__
#define __SCRIPT_INTERNAL_H__

#include "lua.hpp"
#ifndef LUA_OK
#define LUA_OK 0
#endif

#include "SDL.h"

extern lua_State *script_machine;

#define SCRIPT_ADDFUNC(func) do\
{\
	lua_pushcfunction(script_machine, func);\
	lua_setglobal(script_machine, #func);\
}\
while (0);

#define SCRIPT_FUNCREG(func) {#func, func}

void Script_InitLib(const char *libname, const luaL_Reg *funcs);

void Script_GetNamedArgs(lua_State *machine, int argtable, int argct,
		const char **argnames);

void ScriptAI_Init();

#endif
