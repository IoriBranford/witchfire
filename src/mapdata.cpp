#include "mapdata.hpp"

#include "cel.hpp"

#include <cassert>

Data MapData_FindGIDTileset(Data tilesets, int gid)
{
	Data tileset = tilesets.Begin();
	while (tileset)
	{
		Data nexttileset = tileset;
		++nexttileset;
		if (!nexttileset || nexttileset.Find("firstgid").Int() > gid)
			return tileset;
		tileset = nexttileset;
	}
	return Data();
}

static void MapData_ReadTilesets(Data tilesets, bool loadcel)
{
	assert(tilesets);

	std::string celname;
	for (Data tileset = tilesets.Begin(); tileset; ++tileset)
	{
		Data celfile = tileset.Find("image");
		celname = celfile.CStr();

		if (Cel_TrimPathIntoName(celname))
		{
			tileset.Replace(celfile, "cel", 
					Data(celname.c_str()));
		}
		
		if (loadcel)
			Cel_Load(celname.c_str());
	}
}

static void MapData_ReadLayers(Data layers, Data tilesets,
		MapLayerCallback maplayercallback, bool reverse)
{
	Data startlayer = layers.Begin();
	if (reverse)
	{
		while (++Data(startlayer))
			++startlayer;
	}

	for (Data layer = startlayer; layer; reverse ? --layer : ++layer)
	{
		Data layertype = layer.Find("type");

		if (!std::strcmp(layertype.CStr(), "imagelayer"))
		{
			Data celfile = layer.Find("image");
			std::string celname = celfile.CStr();
                
			if (Cel_TrimPathIntoName(celname))
			{
				layer.Replace(celfile, "cel", 
						Data(celname.c_str()));
			}
                
			Cel_Load(celname.c_str());
		}

		if (maplayercallback)
			maplayercallback(layer, tilesets);
	}
}

static void MapData_ReadProperties(Data properties,
		MapPropertyCallback mappropcallback)
{
	for (Data property = properties.Begin(); property; ++property)
	{
		property.ParseStrIntoNum();
		mappropcallback(property);
	}
}

bool MapData_FOpen(const char *path, int flags,
		MapLayerCallback maplayercallback,
		MapPropertyCallback mappropcallback)
{
	Data mapdata;
	Data tilesets;
	Data layers;

	bool success = mapdata.FOpen(path) && mapdata;

	if (success)
	{
		success = (layers = mapdata.Find("layers"));
	}

	if (success)
	{
		success = (tilesets = mapdata.Find("tilesets"));
	}

	if (success)
	{
		if (mappropcallback)
		{
			MapData_ReadProperties(mapdata.Find("properties"),
					mappropcallback);
		}

		MapData_ReadTilesets(tilesets,
				flags & MAPDATAOPEN_LOADTILESETIMAGES);

		MapData_ReadLayers(layers, tilesets, maplayercallback,
				flags & MAPDATAOPEN_REVERSELAYERS);
	}
	
	if (mapdata)
	{
		if (success)
		{
			// TODO save mapdata file optimized for this game
		}
		mapdata.Close();
	}

	return success;
}
