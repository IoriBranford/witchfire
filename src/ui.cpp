#include "ui.hpp"
#include "ui_internal.hpp"

#include "body.hpp"
#include "sprite.hpp"
#include "text.hpp"

#include "cel.hpp"
#include "SDL.h"

#include <map>

std::map<std::string, UIWidget> ui_allwidgets;

void UI_Init()
{
	ui_allwidgets.clear();
	UIAction_RegisterMain();
}

void UI_Tick()
{
	//ui_allwidgets.DelIf(UIWidget_Deleted);
}

static void UIWidget_PreRender(UIWidget &uiwidget, int dt)
{
	if (uiwidget.sprite && uiwidget.body)
		Sprite_SetToBody(uiwidget.sprite, uiwidget.body);
}

void UI_PreRender(int dt)
{
	std::map<std::string, UIWidget>::iterator it;
	for (it = ui_allwidgets.begin();
		it != ui_allwidgets.end();
		++it)
	{
		UIWidget_PreRender(it->second, dt);
	}
}

UIWidget* UIWidget_New(const char *name,
		Body *body,
		Sprite *sprite,
		Text *text)
{
	UIWidget *uiwidget = UIWidget_Get(name);

	if (!uiwidget)
	{
		ui_allwidgets[std::string(name)] = UIWidget();
		uiwidget = &ui_allwidgets[std::string(name)];

		uiwidget->body = body;
		uiwidget->sprite = sprite;
		uiwidget->text = text;
		uiwidget->releaseaction = NULL;
	}

	return uiwidget;
}

UIWidget* UIWidget_Get(const char *name)
{
	std::map<std::string, UIWidget>::iterator it
		= ui_allwidgets.find(std::string(name));

	if (it == ui_allwidgets.end())
		return NULL;

	return &(it->second);
}

Text* UIWidget_Text(const UIWidget *uiwidget)
{
	return uiwidget->text;
}

void UIWidget_SetVisible(UIWidget *uiwidget, bool visible)
{
	if (visible)
	{
		Sprite_EnableDrawFlags(uiwidget->sprite, DRAW_TEX);
	}
	else
	{
		Sprite_DisableDrawFlags(uiwidget->sprite, DRAW_TEX);
	}
}

UIWidget* UIWidget_DirSearchFrom(UIWidget *uiwidget, int dir)
{
	if (!uiwidget->body)
		return NULL;

	const SDL_Point *pos = Body_Pos_Micron(uiwidget->body);

	UIWidget *nearest = NULL;
	std::map<std::string, UIWidget>::iterator it;
	for (it = ui_allwidgets.begin(); it != ui_allwidgets.end(); ++it)
	{
		UIWidget *otherwidget = &it->second;

		if (otherwidget == uiwidget)
			continue;

		if (!otherwidget->body)
			continue;

		if (!otherwidget->releaseaction)
			continue;

		const SDL_Point *nearestpos = !nearest ? NULL
			: Body_Pos_Micron(nearest->body);

		const SDL_Point *otherpos = Body_Pos_Micron(otherwidget->body);

		switch (dir)
		{
		case FD_LEFT:
			if (otherpos->x < pos->x)
			{
				if (!nearest || otherpos->x > nearestpos->x)
					nearest = otherwidget;
			}
			break;

		case FD_RIGHT:
			if (otherpos->x > pos->x)
			{
				if (!nearest || otherpos->x < nearestpos->x)
					nearest = otherwidget;
			}
			break;

		case FD_UP:
			if (otherpos->y < pos->y)
			{
				if (!nearest || otherpos->y > nearestpos->y)
					nearest = otherwidget;
			}
			break;

		case FD_DOWN:
			if (otherpos->y > pos->y)
			{
				if (!nearest || otherpos->y < nearestpos->y)
					nearest = otherwidget;
			}
			break;
		}
	}

	return nearest;
}

