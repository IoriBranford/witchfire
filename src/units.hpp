#ifndef __UNITS_H__
#define __UNITS_H__

const int TICKS_PER_SEC = 60;
const int MS_PER_TICK = 1000 / TICKS_PER_SEC;

#include "SDL.h"

const int MIC_PER_PX_SHIFT = 8;

inline int MicronToPixel(int mic)
{
	return mic >> MIC_PER_PX_SHIFT;
}

inline int PixelToMicron(int px)
{
	return px << MIC_PER_PX_SHIFT;
}

const int SCREEN_LEFT_PX = 0;
const int SCREEN_TOP_PX = 0;
const int SCREEN_RIGHT_PX = 320;
const int SCREEN_BOTTOM_PX = 240;
const int SCREEN_WIDTH_PX = (SCREEN_RIGHT_PX - SCREEN_LEFT_PX);
const int SCREEN_HEIGHT_PX = (SCREEN_BOTTOM_PX - SCREEN_TOP_PX);
const int SCREEN_CENTERX_PX = (SCREEN_WIDTH_PX) >> 1;
const int SCREEN_CENTERY_PX = (SCREEN_HEIGHT_PX) >> 1;

static const SDL_Rect SCREEN_BOUNDS_MI =
{
	PixelToMicron(SCREEN_LEFT_PX),
	PixelToMicron(SCREEN_TOP_PX),
	PixelToMicron(SCREEN_RIGHT_PX),
	PixelToMicron(SCREEN_BOTTOM_PX)
};

inline double DegreeToRadian(double deg)
{
	return deg * M_PI / 180.0;
}

inline double RadianToDegree(double rad)
{
	return rad * 180.0 / M_PI;
}

inline double CircleToRadian(double circ)
{
	return circ * M_PI * 2;
}

inline double RadianToCircle(double rad)
{
	return rad * 0.5 / M_PI;
}

const int HALF_CIRCLE = PixelToMicron(1);

inline int HalfCircleToDegree(int hc)
{
	return hc * 180 / HALF_CIRCLE;
}

inline int DegreeToHalfCircle(double deg)
{
	return deg * HALF_CIRCLE / 180;
}

#endif
