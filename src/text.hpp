#ifndef __TEXT_H__
#define __TEXT_H__

struct Text;

struct SDL_Rect;

void Text_SetString(Text *text, const char *u8string);

void AllTexts_Init(int n);
void AllTexts_Draw();
Text* Text_New(const char *fontname, const SDL_Rect *rect_px,
		const char *u8string);
void Text_Del(Text *text);

#endif
