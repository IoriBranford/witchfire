#include "ship.hpp"
#include "ship_internal.hpp"

#include "alist.hpp"

#include "body.hpp"
#include "sprite.hpp"
#include "voice.hpp"

#include "units.hpp"
#include <climits>

typedef AList<Ship> ShipList;
static ShipList allships;

static Ship *allships_player;

static int allships_enemyct;

void AllShips_Init(int n)
{
	allships.Init(n);
	allships_player = 0;
	allships_enemyct = 0;

	ShipScript_RegisterLib();
}

void AllShips_Tick()
{
	if (allships_player && Ship_IsDead(*allships_player))
	{
		allships_player = NULL;
	}

	allships.DelIf(Ship_IsDead, Ship_Uninit);
	allships.ForEach(Ship_Tick);
}

void AllShips_PreRender(int dt)
{
	allships.ForEach(Ship_PreRender, dt);
}

Ship* Ship_New(const char *type, const SDL_Point *pos, const char *script)
{
	if (AllBodies_Full() || AllSprites_Full() || AllVoices_Full())
		return NULL;

	assert(type && pos);

	Ship *ship = allships.Push();

	if (ship)
	{
		Ship_Init(*ship, type, pos, script);
		if (Ship_IsEnemyType(type))
			++allships_enemyct;
	}

	return ship;
}

bool AllShips_NoPlayer()
{
	return (allships_player == NULL || Ship_IsDead(*allships_player));
}

bool AllShips_PlayerDying()
{
	return (allships_player == NULL || Ship_IsDying(*allships_player));
}

bool AllShips_NoEnemy()
{
	return allships_enemyct == 0;
}

void AllShips_SpawnPlayer()
{
	static const SDL_Point PLAYER_START_POS =
	{
		PixelToMicron(SCREEN_CENTERX_PX >> 1),
		PixelToMicron(SCREEN_CENTERY_PX)
	};

	allships_player = Ship_New("kagari", &PLAYER_START_POS, "player");
}

Ship* AllShips_Player()
{
	if (AllShips_NoPlayer())
		return NULL;

	return allships_player;
}

void AllShips_DecEnemyCt()
{
	--allships_enemyct;
	assert(allships_enemyct >= 0);
}

int Ship_FindNearestOtherShip(Ship *ship, Ship **foundship)
{
	int value = INT_MAX;

	for (ShipList::Iterator it = allships.Begin();
			it != allships.End();
			++it)
	{
		Ship *othership = &(*it);
		if (othership == ship)
			continue;

		const SDL_Point *vec = Ship_VecToOtherShip(ship, othership);

		int v = (vec->x * vec->x) + (vec->y * vec->y);

		if (v < value)
		{
			if (foundship)
				*foundship = othership;

			value = v;
		}
	}

	return value;
}
