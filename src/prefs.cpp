#include "prefs.hpp"

#include <map>
#include "data.hpp"
#include <getopt.h>
#include <cstdio>

#include "units.hpp"

static Prefs prefs_struct;

enum Pref
{
	WIDTH,
	HEIGHT,
	FULLSCREEN,
	VSYNC,
	PREFCT
};

static const char *STR_WIDTH =			"width";
static const char *STR_HEIGHT =			"height";
static const char *STR_FULLSCREEN =		"fullscreen";
static const char *STR_VSYNC =			"vsync";

void Prefs_Init()
{
	prefs_struct.windowwidth = SCREEN_RIGHT_PX << 1;
	prefs_struct.windowheight = SCREEN_BOTTOM_PX << 1;
	prefs_struct.windowflags = SDL_WINDOW_OPENGL;
	prefs_struct.rendererflags = 0;//SDL_RENDERER_TARGETTEXTURE;
}

int Prefs_ParseArgs(int argct, char **args)
{
	int opt;

	enum PrefChar
	{
		CHAR_WIDTH = 'w',
		CHAR_HEIGHT = 'h',
		CHAR_FULLSCREEN = 'f',
		CHAR_VSYNC = 'v'
	};

	enum FullScreenMode
	{
		FULLSCREEN_ON = 'f',
		FULLSCREEN_DESKTOP = 'd'
	};

	const char SHORT_OPTS[] =
	{
		CHAR_WIDTH, ':',
		CHAR_HEIGHT, ':',
		CHAR_FULLSCREEN, ':',
		CHAR_VSYNC,
		0
	};

	const option LONG_OPTS[] =
	{
		{STR_WIDTH, required_argument, 0, CHAR_WIDTH},
		{STR_HEIGHT, required_argument, 0, CHAR_HEIGHT},
		{STR_FULLSCREEN, required_argument, 0, CHAR_FULLSCREEN},
		{STR_VSYNC, no_argument, 0, CHAR_VSYNC},
		{0}
	};

	while ((opt = getopt_long(argct, args, SHORT_OPTS, LONG_OPTS, 0)) != -1)
	{
		switch (opt)
		{
		case CHAR_WIDTH:
			if (!optarg)
				break;

			if (sscanf(optarg, "%d", &prefs_struct.windowwidth) < 1)
			{
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
						"Not valid width: %s", optarg);
			}
			break;

		case CHAR_HEIGHT:
			if (!optarg)
				break;

			if (sscanf(optarg, "%d", &prefs_struct.windowheight)< 1)
			{
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
						"Not valid height: %s", optarg);
			}
			break;

		case CHAR_FULLSCREEN:
			if (!optarg)
				break;

			switch (optarg[0])
			{
			case FULLSCREEN_ON:
				prefs_struct.windowflags |=
					SDL_WINDOW_FULLSCREEN;
				break;

			case FULLSCREEN_DESKTOP:
				prefs_struct.windowflags |=
					SDL_WINDOW_FULLSCREEN_DESKTOP;
				break;

			default:
				; //TODO Print:
				// f = true full screen with res change
				// d = window that fills the desktop
			}
			break;

		case CHAR_VSYNC:
			prefs_struct.rendererflags |=
				SDL_RENDERER_PRESENTVSYNC;
			break;

		//case '?':
		//	switch (optopt)
		//	{
		//	case CHAR_WIDTH:
		//	case CHAR_HEIGHT:
		//		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
		//				"Missing arg for -%c", optopt);
		//		return false;
		//	}
		//	break;

		//default:
		//	return false;
		}
	}

	return optind;
}

const Prefs* Prefs_Get()
{
	return &prefs_struct;
}
