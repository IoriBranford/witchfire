#ifndef __FONT_H__
#define __FONT_H__

struct Font;

struct SDL_Rect;

bool Font_Load(const char *fontname);
Font* Font_Get(const char *name);
void Font_DrawString(Font *font, SDL_Rect *textrect, const char *u8text);
void AllFonts_Unload();

#endif
