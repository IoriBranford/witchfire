#include "ship.hpp"
#include "ship_internal.hpp"
#include "body.hpp"
#include "sprite.hpp"

static void Ship_Shield_OnHit(void *owner, const Hit &hit)
{

}

void Ship_RaiseShield(Ship &ship, int radius_mi)
{
	if (ship.shieldbody)
		return;

	ship.shieldbody = Body_New(VOLUME_BALL, Body_Pos_Micron(ship.body), 1,
			1<<HITGROUP_PSHIELD,
			1<<HITGROUP_ESHIP | 1<<HITGROUP_EBULLET,
			&ship, Ship_Shield_OnHit);

	Body_SetVolumeBall(ship.shieldbody, radius_mi);
}

void Ship_LowerShield(Ship &ship)
{
	if (ship.shieldbody)
	{
		Body_Del(ship.shieldbody);
		ship.shieldbody = NULL;
	}

	if (ship.shieldsprite)
	{
		Sprite_Del(ship.shieldsprite);
		ship.shieldsprite = NULL;
	}
}

void Ship_Shield_Tick(Ship &ship)
{
	if (ship.shieldbody)
	{
		Body_SetAccel(ship.shieldbody, 0, 0);
		Body_AddAccelToFollow(ship.shieldbody, ship.body);
	}
}
