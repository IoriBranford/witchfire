#ifndef __STARFIELD_H__
#define __STARFIELD_H__

void Play_ReceivePlayerCommands(int &digitalflags, float analogvalues[]);

void Play_IncScore(unsigned int points);

void Play_LoadAssets();
void Play_Init();

void Play_Tick();
void Play_PreRender(int dt);

#endif
