#include "body_internal.hpp"
#include "units.hpp"

void Body_SetInGroupFlags(Body *body, int ingroupflags, bool on)
{
	if (on)
		body->ingroupflags |= ingroupflags;
	else
		body->ingroupflags &= ~ingroupflags;
}

void Body_SetHitsGroupFlags(Body *body, int hitsgroupflags, bool on)
{
	if (on)
		body->hitsgroupflags |= hitsgroupflags;
	else
		body->hitsgroupflags &= ~hitsgroupflags;
}

void Body_SetOwnerAndHitCallback(Body *body, void *owner,
		BodyOwnerHitCallback ownerhitcallback)
{
	body->owner = owner;
	body->ownerhitcallback = ownerhitcallback;
}

bool Body_IsInRect(const Body *body, const SDL_Rect *rect_mi,
		BodyOverlap *overlap_mi)
{
	SDL_Rect bodyrect;
	SDL_Point p0, p1;
	bool isin = false;

	switch (body->vol_mi.type)
	{
	case VOLUME_BOX:
		bodyrect = body->vol_mi.box.rect;
		bodyrect.x += body->pos_mi.x;
		bodyrect.y += body->pos_mi.y;

		if (!overlap_mi)
			return SDL_HasIntersection(&bodyrect, rect_mi);

		overlap_mi->type = OVERLAP_RECT;
		return SDL_IntersectRect(&bodyrect, rect_mi,
				&overlap_mi->rect.rect);

	case VOLUME_RAY:
		p0 = body->vol_mi.ray.origin;
		p0.x += body->pos_mi.x;
		p0.y += body->pos_mi.y;

		p1 = p0;
		p1.x += body->vol_mi.ray.vec.x;
		p1.y += body->vol_mi.ray.vec.y;

		isin = SDL_IntersectRectAndLine(rect_mi,
				&p0.x, &p0.y, &p1.x, &p1.y);

		if (overlap_mi)
		{
			overlap_mi->type = OVERLAP_POINTS;
			overlap_mi->points.p0 = p0;
			overlap_mi->points.p1 = p1;
		}

		return isin;

	default:
		;
	}

	return //SDL_PointInRect(body->pos_mi, rect_mi);
		!(body->pos_mi.x < rect_mi->x
		|| body->pos_mi.x >= rect_mi->x + rect_mi->w
		|| body->pos_mi.y < rect_mi->y
		|| body->pos_mi.y >= rect_mi->y + rect_mi->h
		 );
}

static bool IntersectLines(
		const SDL_Point *origin0,
		const SDL_Point *vec0,
		const SDL_Point *origin1,
		const SDL_Point *vec1,
		SDL_Point *intersectionp0 = NULL,
		SDL_Point *intersectionp1 = NULL
		)
{
	SDL_Point dorigin;
	dorigin.x = origin1->x - origin0->x;
	dorigin.y = origin1->y - origin0->y;

	int dorigincrossvec0 = ((dorigin.x * vec0->y) - (vec0->x * dorigin.y));
	int dorigincrossvec1 = ((dorigin.x * vec1->y) - (vec1->x * dorigin.y));
	int vec0crossvec1 = ((vec0->x * vec1->y) - (vec1->x * vec0->y));

	if (!vec0crossvec1)
	{
		if (dorigincrossvec0)
			return false;

		// TODO handle collinear cases if/when we need
		//SDL_Point dend;
		//dend.x = (dorigin->x + vec1->x) - (origin0->x + vec0->x);
		//dend.y = (dorigin->y + vec1->y) - (origin0->y + vec0->y);

		int dorigindotvec0 =
			(dorigin.x * vec0->x) + (dorigin.y * vec0->y);
		int ndorigindotvec1 =
			(-dorigin.x * vec1->x) + (-dorigin.y * vec1->y);

		int vec0lensq = (vec0->x * vec0->x) + (vec0->y * vec0->y);
		int vec1lensq = (vec1->x * vec1->x) + (vec1->y * vec1->y);

		if ((0 <= dorigindotvec0 && dorigindotvec0 <= vec0lensq)
		|| (0 <= ndorigindotvec1 && ndorigindotvec1 <= vec1lensq))
		{
			if (intersectionp0 && intersectionp1)
			{
				// TODO calc endpoints of overlap line
			}
			return true;
		}
	}
	else
	{
		if (0 <= dorigincrossvec0 && dorigincrossvec0 <= vec0crossvec1
		&& 0 <= dorigincrossvec1 && dorigincrossvec1 <= vec0crossvec1)
		{
			if (intersectionp0)
			{
				*intersectionp0 = *origin0;
				intersectionp0->x += vec0->x
					* dorigincrossvec1 / vec0crossvec1;
				intersectionp0->y += vec0->y
					* dorigincrossvec1 / vec0crossvec1;

				if (intersectionp1)
					*intersectionp1 = *intersectionp0;
			}
			return true;
		}
	}

	return false;
}

static bool Body_IsOnLine(const Body *body, const SDL_Point *origin_mi,
		const SDL_Point *vec_mi, BodyOverlap *overlap_mi = NULL)
{
	const Volume &vol = body->vol_mi;

	SDL_Rect bodyrect;
	SDL_Point bodyorigin;

	SDL_Point p0, p1;

	bool isin = false;

	switch (vol.type)
	{
	case VOLUME_BOX:
		bodyrect = vol.box.rect;
		bodyrect.x += body->pos_mi.x;
		bodyrect.y += body->pos_mi.y;

		isin = SDL_IntersectRectAndLine(&bodyrect,
				&p0.x, &p0.y, &p1.x, &p1.y);

		if (overlap_mi)
		{
			overlap_mi->type = OVERLAP_POINTS;
			overlap_mi->points.p0 = p0;
			overlap_mi->points.p1 = p1;
		}

		break;

	case VOLUME_RAY:
		bodyorigin = vol.ray.origin;
		bodyorigin.x += body->pos_mi.x;
		bodyorigin.y += body->pos_mi.y;

		isin = IntersectLines(&bodyorigin, &vol.ray.vec,
				origin_mi, vec_mi, &p0, &p1);
		
		if (overlap_mi)
		{
			overlap_mi->type = OVERLAP_POINTS;
			overlap_mi->points.p0 = p0;
			overlap_mi->points.p1 = p1;
		}

		break;

	default:
		;
	}

	return isin;
}

static bool Body_IsInCircle(const Body *body, const SDL_Point *center_mi,
		int rad_mi)
{
	// TODO volume-specific if needed

	// convert to pixels to avoid overflow

	SDL_Point bodytocenter_px =
	{
		MicronToPixel(center_mi->x - body->pos_mi.x),
		MicronToPixel(center_mi->y - body->pos_mi.y)
	};

	int rad_px = MicronToPixel(rad_mi);

	return (bodytocenter_px.x * bodytocenter_px.x)
		+ (bodytocenter_px.y * bodytocenter_px.y)
		< rad_px * rad_px;
}

bool Body_IntersectsBody(Body *body0, Body *body1,
		BodyOverlap *overlap_mi)
{
	//const Volume &vol0 = body0->vol_mi;
	const Volume &vol1 = body1->vol_mi;
	//VolumeType type0 = vol0.type;
	VolumeType type1 = vol1.type;
	
	SDL_Rect box;
	SDL_Point origin;

	switch (type1)
	{
	case VOLUME_BOX:
		box = vol1.box.rect;
		box.x += body1->pos_mi.x;
		box.y += body1->pos_mi.y;

		return Body_IsInRect(body0, &box, overlap_mi);

	case VOLUME_RAY:
		origin = vol1.ray.origin;
		origin.x += body1->pos_mi.x;
		origin.y += body1->pos_mi.y;

		return Body_IsOnLine(body0, &origin, &vol1.ray.vec,
				overlap_mi);

	case VOLUME_BALL:
		origin = vol1.ball.center;
		origin.x += body1->pos_mi.x;
		origin.y += body1->pos_mi.y;

		return Body_IsInCircle(body0, &origin, vol1.ball.radius);

	default:
		;
	}
	return false;
}

