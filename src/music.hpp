#ifndef __MUSIC_H__
#define __MUSIC_H__

void Music_Init();
bool Music_Load(const char *name);
bool Music_FOpen(const char *path);
void Music_Start(int track = 0);
void Music_SetPlaying(bool playing);
void Music_SetVolume(int volume);
void Music_Fade(int time_tk, const char *nextmusic);
void Music_Tick();
bool Music_Finished();
void Music_Uninit();

#endif
