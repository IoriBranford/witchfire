#include "app.hpp"
#include "app_internal.hpp"

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_mixer.h"
#include "prefs.hpp"
#include "disk.hpp"
#include "time.hpp"

#include "input.hpp"
#include "ui.hpp"
//#include "timeline.hpp"
#include "body.hpp"
#include "sprite.hpp"
#include "tile.hpp"
#include "text.hpp"
#include "render.hpp"
//#include "render3d.hpp"
#include "cel.hpp"
#include "voice.hpp"
#include "sound.hpp"
#include "music.hpp"
#include "script.hpp"
#include "font.hpp"
#include "database.hpp"
#include "stats.hpp"

#include "units.hpp"

#include <cstring>

static Uint32 APP_EVENT;

const int MAX_TICKS_PER_LOOP = 5;

enum AppEventCode
{
	APP_EVENT_PHASECHANGED,
	APP_EVENT_CODE_CT
};

static SDL_Window *app_window = NULL;

static AppPhase app_nextphase;

SDL_Window* App_Window()
{
	return app_window;
}

void App_RequestQuit()
{
	SDL_Event event;
	event.type = SDL_QUIT;
	SDL_PushEvent(&event);
}

void App_RequestPhaseChange(AppPhaseLoadAssetsFunc loadassetsfunc,
		AppPhaseInitFunc initfunc,
		AppPhaseTickFunc tickfunc,
		AppPhaseRenderFunc renderfunc)
{
	app_nextphase.loadassetsfunc = loadassetsfunc;
	app_nextphase.initfunc = initfunc;
	app_nextphase.tickfunc = tickfunc;
	app_nextphase.renderfunc = renderfunc;

	SDL_Event event;
	event.user.type = APP_EVENT;
	event.user.code = APP_EVENT_PHASECHANGED;
	event.user.data1 = &app_nextphase;
	SDL_PushEvent(&event);
}

static void App_Loop()
{
	Input_ClearCallbacks();

	TileMap_Init();
	AllSprites_Init(256);
	AllVoices_Init(256);
	AllBodies_Init(256);
	AllTexts_Init(256);
	//Timeline_Init(1024);
	UI_Init();

	AppPhase_LoadAssets();
	//Render3DTest_LoadAssets();
	AppPhase_Init();

	// Run game in accumulated fixed timesteps (ticks)
	// Source: http://gafferongames.com/game-physics/fix-your-timestep/

	Uint64 lastlooprealtime = SDL_GetPerformanceCounter();
	Sint64 accumulatedrealtime = 0;

	const Uint64 REAL_TIME_PER_SEC = SDL_GetPerformanceFrequency();
	const Sint64 REAL_TIME_PER_TICK = REAL_TIME_PER_SEC / TICKS_PER_SEC;
	const Uint64 MIN_REAL_TIME_PER_LOOP = REAL_TIME_PER_SEC / 500;

	const Sint64 MAX_ACCUMULATED_REAL_TIME =
		REAL_TIME_PER_TICK * MAX_TICKS_PER_LOOP;

	Time_ResetTicks();

	bool phaserunning = true;
	bool focused = true;

	int fps = 0;
	Uint64 fpsrealtime = 0;
	Text *fpstext = NULL;

	while (phaserunning)
	{
		Uint64 looprealtime = SDL_GetPerformanceCounter();
		Uint64 deltarealtime = looprealtime - lastlooprealtime;

		fpsrealtime += deltarealtime;
		if (fpsrealtime >= REAL_TIME_PER_SEC)
		{
			char fpsstring[8];
			std::snprintf(fpsstring, 8, "%dfps", fps);

			if (!fpstext)
			{
				const SDL_Rect fpsrect =
				{
					SCREEN_RIGHT_PX - 64,
					SCREEN_BOTTOM_PX - 16,
					0,
					0
				};

				fpstext = Text_New("Unifont16", &fpsrect,
						fpsstring);
			}
			else
			{
				Text_SetString(fpstext, fpsstring);
			}

			fpsrealtime %= SDL_GetPerformanceFrequency();
			fps = 0;
		}

		accumulatedrealtime += deltarealtime;
		// unsigned int time diff doesn't suffer from rollover
		// source: http://www.thetaeng.com/TimerWrap.htm

		// prevent death spiral (game falls behind real time)
		if (accumulatedrealtime > MAX_ACCUMULATED_REAL_TIME)
			accumulatedrealtime = MAX_ACCUMULATED_REAL_TIME;

		int dt = 0;
		while (accumulatedrealtime >= REAL_TIME_PER_TICK)
		{
			SDL_Event event;
			while (SDL_PollEvent(&event))
			{
				if (event.type == APP_EVENT)
				{
					switch (event.user.code)
					{
					case APP_EVENT_PHASECHANGED:
						AppPhase_Change(*((AppPhase*)
							event.user.data1));
						phaserunning = false;
						break;
					}

					continue;
				}

				switch (event.type)
				{
				case SDL_QUIT:
					AppPhase_Change(AppPhase());
					phaserunning = false;
					break;

//				case SDL_WINDOWEVENT:
//					switch (event.window.event)
//					{
//					case SDL_WINDOWEVENT_FOCUS_GAINED:
//						focused = true;
//						Mix_Resume(-1);
//						Music_SetPlaying(true);
//						break;
//
//					case SDL_WINDOWEVENT_FOCUS_LOST:
//						focused = false;
//						Mix_Pause(-1);
//						Music_SetPlaying(false);
//						break;
//					}
//					break;

				default:
					Input_OnEvent(event);
				}
			}

			if (!phaserunning)
				break;

			if (focused)
			{
				AppPhase_Tick();

				//Timeline_Tick();
				UI_Tick();

				AllBodies_Tick();
				AllVoices_Tick();

				AllSprites_Prune();
				//AllTexts_Prune();

				Music_Tick();

				Time_Tick(1);
				++dt;
			}

			accumulatedrealtime -= REAL_TIME_PER_TICK;
		}

		Script_GarbageCollect(2);

		if (phaserunning && focused)
		{
			AllSprites_PreRender(dt);
			AppPhase_PreRender(dt);
			UI_PreRender(dt);

			Render_Clear();

			//Render3DTest_Draw();

			AllTexts_Draw();
			TileMap_Draw();
			AllSprites_Draw();
			//AllBodies_Draw();

			++fps;
		}

		Render_Present(app_window);

		Sint64 looprealtimeleft = MIN_REAL_TIME_PER_LOOP -
			(SDL_GetPerformanceCounter() - looprealtime);

		if (looprealtimeleft > 0 &&
				looprealtimeleft < MIN_REAL_TIME_PER_LOOP)
		{
			SDL_Delay(looprealtimeleft * 1000 / REAL_TIME_PER_SEC);
		}

		lastlooprealtime = looprealtime;
	}

	AllCels_Unload();
	TileMap_Clear();
	AllSounds_Unload();
	AllDatabases_Unload();
	Script_Init();
}

#include "play.hpp"
#include "title.hpp"

int main(int argct, char **args)
{
	int argi = 0;
	int error = 0;
	const Prefs *prefs = NULL;

	error = SDL_Init(SDL_INIT_EVERYTHING);
	if (error)
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_SYSTEM, SDL_GetError());
		goto quit;
	}

	//SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_DEBUG);
	SDL_LogSetPriority(SDL_LOG_CATEGORY_ERROR, SDL_LOG_PRIORITY_ERROR);
	//SDL_LogSetPriority(SDL_LOG_CATEGORY_ASSERT, SDL_LOG_PRIORITY_DEBUG);
	//SDL_LogSetPriority(SDL_LOG_CATEGORY_SYSTEM, SDL_LOG_PRIORITY_DEBUG);
	//SDL_LogSetPriority(SDL_LOG_CATEGORY_AUDIO, SDL_LOG_PRIORITY_DEBUG);
	//SDL_LogSetPriority(SDL_LOG_CATEGORY_VIDEO, SDL_LOG_PRIORITY_DEBUG);
	//SDL_LogSetPriority(SDL_LOG_CATEGORY_RENDER, SDL_LOG_PRIORITY_DEBUG);
	//SDL_LogSetPriority(SDL_LOG_CATEGORY_INPUT, SDL_LOG_PRIORITY_DEBUG);

	if (!Disk_Open(args[0]))
		goto quit;

	Prefs_Init();
	argi = Prefs_ParseArgs(argct, args);
	prefs = Prefs_Get();
	argct -= argi;

	if (!IMG_Init(IMG_INIT_PNG))
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_SYSTEM, IMG_GetError());
		goto quit;
	}

	if (!Mix_Init(0))
		SDL_LogError(SDL_LOG_CATEGORY_AUDIO, Mix_GetError());

	Music_Init();

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	//SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	//SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	//SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

	app_window = SDL_CreateWindow("sdl2game",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
			     prefs->windowwidth,
			     prefs->windowheight,
			     prefs->windowflags);

	if (!app_window)
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_VIDEO, SDL_GetError());
		goto quit;
	}

	//SDL_SetHintWithPriority(SDL_HINT_RENDER_OPENGL_SHADERS, "0",
	//		SDL_HINT_OVERRIDE);

	if (!Render_Init(app_window, prefs->rendererflags))
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_RENDER, SDL_GetError());
		goto quit;
	}

	error = Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT,
				MIX_DEFAULT_CHANNELS, 2048);

	if (error)
		SDL_LogError(SDL_LOG_CATEGORY_AUDIO, Mix_GetError());

	Input_Init();
	Stats_Init();

	if (!Script_Init())
		goto quit;

	if (!Font_Load("Unifont16"))
		goto quit;

	APP_EVENT = SDL_RegisterEvents(1);

	app_nextphase.loadassetsfunc = Title_LoadAssets;
	app_nextphase.initfunc = Title_Init;
	app_nextphase.tickfunc = Title_Tick;
	app_nextphase.renderfunc = Title_PreRender;
	AppPhase_Change(app_nextphase);

	while (AppPhase_Valid())
		App_Loop();

quit:
	AllFonts_Unload();

	Script_Uninit();
	Input_Uninit();
	Music_Uninit();
	Mix_CloseAudio();

	SDL_DestroyWindow(app_window);

	Mix_Quit();
	SDL_Quit();

	Disk_Close();

	return 0;
}
