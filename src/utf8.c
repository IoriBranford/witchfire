
void UCharToUTF8seq(unsigned char *utf8seq, unsigned uchar)
{
	if (uchar < 0x80)
	{
		(*utf8seq++) = uchar;
		*utf8seq = '\0';
	}
	else if (uchar < 0x800)
	{
		(*utf8seq++) = ((uchar>>6) & 0x1F) | 0xC0;
		(*utf8seq++) = ((uchar>>0) & 0x3F) | 0x80;
		*utf8seq = '\0';
	}
	else if (uchar < 0x10000)
	{
		(*utf8seq++) = ((uchar>>0xC) & 0x0F) | 0xE0;
		(*utf8seq++) = ((uchar>>0x6) & 0x3F) | 0x80;
		(*utf8seq++) = ((uchar>>0x0) & 0x3F) | 0x80;
		*utf8seq = '\0';
	}
	else if (uchar < 0x110000)
	{
		(*utf8seq++) = ((uchar>>0x12) & 0x07) | 0xF0;
		(*utf8seq++) = ((uchar>>0x0C) & 0x3F) | 0x80;
		(*utf8seq++) = ((uchar>>0x06) & 0x3F) | 0x80;
		(*utf8seq++) = ((uchar>>0x00) & 0x3F) | 0x80;
		*utf8seq = '\0';
	}
}

unsigned UCharToUTF8int(unsigned uchar)
{
	unsigned utf8int = 0;

	if (uchar < 0x80)
	{
		utf8int = uchar;
	}
	else if (uchar < 0x800)
	{
		utf8int |= (((uchar>>6) & 0x1F) | 0xC0);
		utf8int |= (((uchar>>0) & 0x3F) | 0x80) << 8;
	}
	else if (uchar < 0x10000)
	{
		utf8int |= (((uchar>>0xC) & 0x0F) | 0xE0);
		utf8int |= (((uchar>>0x6) & 0x3F) | 0x80) << 0x08;
		utf8int |= (((uchar>>0x0) & 0x3F) | 0x80) << 0x10;
	}
	else if (uchar < 0x110000)
	{
		utf8int |= (((uchar>>0x12) & 0x07) | 0xF0);
		utf8int |= (((uchar>>0x0C) & 0x3F) | 0x80) << 0x08;
		utf8int |= (((uchar>>0x06) & 0x3F) | 0x80) << 0x10;
		utf8int |= (((uchar>>0x00) & 0x3F) | 0x80) << 0x18;
	}

	return utf8int;
}

unsigned UTF8seqToUTF8int(const unsigned char *utf8seq, unsigned *utf8int)
{
	if ((*utf8seq & 0x80) == 0)
	{
		*utf8int = *utf8seq;
		return 1;
	}
	else if ((*utf8seq & 0xE0) == 0xC0)
	{
		*utf8int = utf8seq[0];
		*utf8int |= utf8seq[1] << 8;
		return 2;
	}
	else if ((*utf8seq & 0xF0) == 0xE0)
	{
		*utf8int = utf8seq[0];
		*utf8int |= utf8seq[1] << 0x08;
		*utf8int |= utf8seq[2] << 0x10;
		return 3;
	}
	else if ((*utf8seq & 0xF8) == 0xF0)
	{
		*utf8int = utf8seq[0];
		*utf8int |= utf8seq[1] << 0x08;
		*utf8int |= utf8seq[2] << 0x10;
		*utf8int |= utf8seq[3] << 0x18;
		return 4;
	}

	return 0;
}
