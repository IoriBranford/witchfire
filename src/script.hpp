#ifndef __SCRIPT_H__
#define __SCRIPT_H__

struct lua_State;

extern "C"
{
#include "lauxlib.h" // for luaL_Reg, luaL_error
}

typedef int (*lua_CFunction) (lua_State *L);

bool Script_Init();
void Script_Uninit();

bool Script_Load(const char *name);

lua_State* Script_Resume(lua_State *inthread, const char *name, void *actor);

void Script_GarbageCollect(int stepsize);

void Script_KillThreadOf(void *actor);

#define SCRIPTFUNC_REGISTERFUNC(func) ScriptFunc_RegisterFunc(#func, func)
void ScriptFunc_RegisterFunc(const char *funcname, lua_CFunction func);
#define SCRIPTFUNC_REG(func) {#func, func}
void ScriptFunc_RegisterLib(const char *libname, const luaL_Reg *funcs);

#define SCRIPTFUNC_REQUIREARG(arg) do\
{\
	if (!arg)\
		return luaL_error(machine, "%s requires arg '%s'",\
				__func__, #arg);\
} while (0);

template <class T> T ScriptFunc_Arg(lua_State *machine, const char *argname,
		T defaultvalue);

template <class T> void ScriptFunc_Return(lua_State *machine, T value);
void ScriptFunc_ReturnNil(lua_State *machine);

#endif
