#ifndef __UI_H__
#define __UI_H__

struct UIWidget;

struct Body;
struct Sprite;
struct Text;

void UI_Init();
void UI_Tick();
void UI_PreRender(int dt);

UIWidget* UIWidget_New(const char *name,
		Body *body,
		Sprite *sprite,
		Text *text);

UIWidget* UIWidget_Get(const char *name);
Text* UIWidget_Text(const UIWidget *uiwidget);

void UIWidget_SetVisible(UIWidget *uiwidget, bool visible);

enum FindDir
{
	FD_LEFT,
	FD_RIGHT,
	FD_UP,
	FD_DOWN
};

UIWidget* UIWidget_DirSearchFrom(UIWidget *uiwidget, int dir);

bool UI_Load(const char *name);

void UICursor_Init();

#endif
