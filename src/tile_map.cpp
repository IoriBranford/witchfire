#include "tile.hpp"
#include "tile_internal.hpp"

#include "texture.hpp"

#include "units.hpp"

#include "SDL.h"

#include <cassert>

void TileMap_Init()
{
	TileMap_Clear();

	tilemap_viewrect_mi.x =
	tilemap_viewrect_mi.y = 0;
	tilemap_viewrect_mi.w = PixelToMicron(SCREEN_WIDTH_PX);
	tilemap_viewrect_mi.h = PixelToMicron(SCREEN_HEIGHT_PX);
	tilemap_looprect_mi.x = 0;
	tilemap_looprect_mi.y = 0;
	tilemap_looprect_mi.w = 0;
	tilemap_looprect_mi.h = 0;
	tilemap_loopstate = LOOP_INACTIVE;
}

static void TileMap_UpdateLoop()
{
	SDL_Rect &view = tilemap_viewrect_mi;
	SDL_Rect &loop = tilemap_looprect_mi;

	if (!loop.w || !loop.h)
		return;

	if (tilemap_loopstate != LOOP_INACTIVE)
	{
		view.x = loop.x +
			((((view.x - loop.x) % loop.w) + loop.w) % loop.w);
		view.y = loop.y +
			((((view.y - loop.y) % loop.h) + loop.h) % loop.h);
	}

	if (tilemap_loopstate != LOOP_ACTIVE)
	{
		if (loop.x <= view.x && view.x + view.w <= loop.x + loop.w
		&& loop.y <= view.y && view.y + view.h <= loop.y + loop.h)
		{
			switch (tilemap_loopstate)
			{
			case LOOP_INACTIVE:
				tilemap_loopstate = LOOP_ACTIVE;
				break;

			case LOOP_DEACTIVATING:
				tilemap_loopstate = LOOP_INACTIVE;
				loop.x = loop.y = 0;
				loop.w = loop.h = 0;
				break;

			default:
				;
			}
		}
	}
}

void TileMap_TranslateView(int dx_mi, int dy_mi)
{
	tilemap_viewrect_mi.x += dx_mi;
	tilemap_viewrect_mi.y += dy_mi;
	TileMap_UpdateLoop();
}

bool TileMap_RectInView(const SDL_Rect *rect_mi)
{
	return SDL_HasIntersection(&tilemap_viewrect_mi, rect_mi);
}

void TileMap_ToScreenPos(SDL_Point *pos_mi)
{
	assert(pos_mi);
	pos_mi->x -= tilemap_viewrect_mi.x;
	pos_mi->y -= tilemap_viewrect_mi.y;
}

void TileMap_SetLoopRect(const SDL_Rect *rect_mi)
{
	assert(rect_mi);
	tilemap_loopstate = LOOP_INACTIVE;
	tilemap_looprect_mi.x = rect_mi->x;
	tilemap_looprect_mi.y = rect_mi->y;
	tilemap_looprect_mi.w = rect_mi->w;
	tilemap_looprect_mi.h = rect_mi->h;
}

void TileMap_DeactivateLoop()
{
	tilemap_loopstate = LOOP_DEACTIVATING;
}

void TileMap_Draw()
{
	for (size_t i = 0; i < alltilelayers.size(); ++i)
		TileLayer_Draw(alltilelayers[i]);
}

void TileMap_Clear()
{
	std::map<std::string, Tileset>::iterator it;
	for (it = alltilesets.begin(); it != alltilesets.end(); ++it)
		Texture_Delete(it->second.texture);

	alltilesets.clear();
	alltilelayers.clear();
}

