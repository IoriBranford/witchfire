#include "script.hpp"
#include "script_internal.hpp"
#include "disk.hpp"

lua_State *script_machine;

static const char *SCRIPT_DIR = "scr/";
static const char *SCRIPT_EXT = ".lua";

static int ACTOR_THREAD_TABLE;

bool Script_Init()
{
	Script_Uninit();
	script_machine = luaL_newstate();
	if (!script_machine)
	{
		return false;
	}

	static const luaL_Reg loadedlibs[] = {
		{"", luaopen_base},
		//{LUA_LOADLIBNAME, luaopen_package},
		//Lua 5.2:{LUA_COLIBNAME, luaopen_coroutine},
		{LUA_TABLIBNAME, luaopen_table},
		//{LUA_IOLIBNAME, luaopen_io},
		//{LUA_OSLIBNAME, luaopen_os},
		{LUA_STRLIBNAME, luaopen_string},
		{LUA_BITLIBNAME, luaopen_bit},
		{LUA_MATHLIBNAME, luaopen_math},
		//{LUA_DBLIBNAME, luaopen_debug},
		{NULL, NULL}
	};

	for (const luaL_Reg *lib = loadedlibs; lib->func; lib++)
	{
		//Lua 5.1:
		lua_pushcfunction(script_machine, lib->func);
		lua_pushstring(script_machine, lib->name);
		lua_pcall(script_machine, 1, 0, 0);
		//Lua 5.2:
		//luaL_requiref(script_machine, lib->name, lib->func, 1);
		//lua_pop(script_machine, 1);
	}

	lua_newtable(script_machine);
	ACTOR_THREAD_TABLE = lua_gettop(script_machine);

	return true;
}

void Script_Uninit()
{
	if (script_machine)
		lua_close(script_machine);
	script_machine = NULL;
}

bool Script_Load(const char *name)
{
	lua_getglobal(script_machine, name);
	if (lua_isfunction(script_machine, -1))
	{
		lua_pop(script_machine, 1);
		return true;
	}

	std::string path (SCRIPT_DIR);
	path += name;
	path += SCRIPT_EXT;

	char *text;
	int length = Disk_CStrOpen(path.c_str(), &text);
	if (length <= 0)
		return false;

	int error = luaL_loadbuffer(script_machine, text, length, name);
	Disk_CStrClose(text);

	if (error == LUA_OK)
	{
		lua_setglobal(script_machine, name);
		return true;
	}

	SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
		lua_tostring(script_machine, -1));

	return false;
}

lua_State* Script_Resume(lua_State *inthread, const char *name, void *actor)
{
	int prevtop = lua_gettop(script_machine);

	lua_State *thread = inthread;
	if (!thread)
	{
		thread = lua_newthread(script_machine);
		lua_getglobal(thread, name);
	}

	//Lua 5.1:
	int result = lua_resume(thread, 0);
	//Lua 5.2:
	//int result = lua_resume(thread, script_machine, 0);

	if (result == LUA_YIELD)
	{
		if (!inthread)
		{
			lua_pushlightuserdata(script_machine, actor);
			lua_pushthread(thread);
			lua_settable(script_machine, ACTOR_THREAD_TABLE);
		}
	}
	else
	{
		if (result != LUA_OK)
		{
			SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
					"Script '%s': %s",
					name, lua_tostring(thread, -1));
		}

		if (inthread)
			Script_KillThreadOf(actor);

		thread = NULL;
	}

	lua_pop(script_machine, lua_gettop(script_machine) - prevtop);
	return thread;
}

void Script_GarbageCollect(int stepsize)
{
	lua_gc(script_machine, LUA_GCSTEP, stepsize);
}

void Script_KillThreadOf(void *actor)
{
	lua_pushlightuserdata(script_machine, actor);
	lua_pushnil(script_machine);
	lua_settable(script_machine, ACTOR_THREAD_TABLE);
}

