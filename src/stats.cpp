#include "stats.hpp"

#include <algorithm>

static const unsigned int STATS_SCORE_MAX = 99999999;

static const unsigned int STATS_SCORETOEXTEND_START = 10000;
static const unsigned int STATS_SCORETOEXTEND_INC = 600000;
static const unsigned int STATS_LIVES_START = 3;
static const unsigned int STATS_STAGE_START = 1;

// TODO get from prefs
static unsigned int stats_scoretoextend_start;
static unsigned int stats_scoretoextend_inc;
static unsigned int stats_lives_start;
static unsigned int stats_stage_start;

static unsigned int stats_score;
static unsigned int stats_scoretoextend;
static unsigned int stats_lives;
static unsigned int stats_stage;

void Stats_Init()
{
	stats_scoretoextend_start = STATS_SCORETOEXTEND_START;
	stats_scoretoextend_inc = STATS_SCORETOEXTEND_INC;
	stats_lives_start = STATS_LIVES_START;
	stats_stage_start = STATS_STAGE_START;
	Stats_Reset();
}

void Stats_Reset()
{
	stats_score = 0;
	stats_scoretoextend = stats_scoretoextend_start;
	stats_lives = stats_lives_start;
	stats_stage = stats_stage_start;
}

unsigned int Stats_Score()
{
	return stats_score;
}

unsigned int Stats_ScoreToExtend()
{
	return stats_scoretoextend;
}

unsigned int Stats_Lives()
{
	return stats_lives;
}

unsigned int Stats_Stage()
{
	return stats_stage;
}

unsigned int Stats_IncScore(unsigned int points)
{
	stats_score = std::min(stats_score + points, STATS_SCORE_MAX);
	return stats_score;
}

bool Stats_WillExtend(unsigned int points)
{
	return (points && stats_scoretoextend &&
			stats_lives < STATS_LIVES_MAX &&
			stats_score + points >= stats_scoretoextend);
}

void Stats_Extend()
{
	++stats_lives;
	stats_scoretoextend += stats_scoretoextend_inc;
}

unsigned int Stats_DecLives()
{
	return --stats_lives;
}

unsigned int Stats_IncStage()
{
	return ++stats_stage;
}

