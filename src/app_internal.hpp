#ifndef __APP_PHASE_H__
#define __APP_PHASE_H__

#include "app.hpp"

struct AppPhase
{
	AppPhaseLoadAssetsFunc loadassetsfunc;
	AppPhaseInitFunc initfunc;
	AppPhaseTickFunc tickfunc;
	AppPhaseRenderFunc renderfunc;

	AppPhase()
	{
		loadassetsfunc = 0;
		initfunc = 0;
		tickfunc = 0;
		renderfunc = 0;
	}
};

bool AppPhase_Valid();
void AppPhase_Change(const AppPhase &nextphase);
void AppPhase_LoadAssets();
void AppPhase_Init();
void AppPhase_Tick();
void AppPhase_PreRender(int dt);

#endif
