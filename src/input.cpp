#include "input.hpp"

#include "SDL.h"

#include <vector>
#include <map>
#include <cassert>
#include <cstring>

static const float JOY_AXIS_MAX = 32768;

static DigitalCallback input_digitalcallback;
static AnalogCallback input_analogcallback;
static PointCallback input_pointcallback;

union Device
{
	SDL_Joystick *as_joystick;
	SDL_GameController *as_controller;

	operator bool()
	{
		return as_joystick != 0;
	}
};

static std::vector<Device> input_devices;

static bool operator<(const SDL_Event &event0, const SDL_Event &event1)
{
	return memcmp(&event0, &event1, sizeof(SDL_Event)) < 0;
}

typedef std::map<SDL_Event, int> EventCommandMap;
static EventCommandMap input_eventcommands;

void Input_Init()
{
	Input_ClearCallbacks();
	Input_ResetDefaultEventCommands();

	input_devices.resize(SDL_NumJoysticks());
	for (size_t i = 0; i < input_devices.size(); ++i)
	{
		Device device;

		if (SDL_IsGameController(i))
		{
			device.as_controller = SDL_GameControllerOpen(i);
			SDL_Log("Controller found: %s",
					SDL_JoystickNameForIndex(i));
		}
		else
		{
			device.as_joystick = SDL_JoystickOpen(i);
			SDL_Log("Joystick found: %s",
					SDL_JoystickNameForIndex(i));
		}

		if (!device)
		{
			SDL_LogCritical(SDL_LOG_CATEGORY_INPUT,
					SDL_GetError());
		}

		input_devices[i] = device;
	}

	SDL_SetRelativeMouseMode(SDL_TRUE);
	//SDL_LogSetPriority(SDL_LOG_CATEGORY_INPUT, SDL_LOG_PRIORITY_DEBUG);
}

void Input_Uninit()
{
	for (size_t i = 0; i < input_devices.size(); ++i)
	{
		if (input_devices[i])
		{
			if (SDL_IsGameController(i))
				SDL_GameControllerClose(
						input_devices[i].as_controller);
			else
				SDL_JoystickClose(input_devices[i].as_joystick);
		}
	}
	input_devices.clear();
}

void Input_SetDigitalCallback(DigitalCallback digitalcallback)
{
	input_digitalcallback = digitalcallback;
}

void Input_SetAnalogCallback(AnalogCallback analogcallback)
{
	input_analogcallback = analogcallback;
}

void Input_SetPointCallback(PointCallback pointcallback)
{
	input_pointcallback = pointcallback;
}

void Input_ClearCallbacks()
{
	Input_SetDigitalCallback(NULL);
	Input_SetAnalogCallback(NULL);
	Input_SetPointCallback(NULL);
}

static void Input_OnKeyEvent(const SDL_Event &event, int command)
{
	if (!input_digitalcallback)
		return;

	if (event.key.repeat)
		return;

	input_digitalcallback(command, event.key.state == SDL_PRESSED);
}

static void Input_OnJoyAxisEvent(const SDL_Event &event, int command)
{
	if (!input_analogcallback)
		return;

	if (SDL_IsGameController(event.jaxis.which))
		return;

	float value = event.jaxis.value;
	if (value > 0)
		value += 1;

	SDL_LogDebug(SDL_LOG_CATEGORY_INPUT, "Joy %d: axis %d = %d",
			event.jaxis.which, event.jaxis.axis, event.jaxis.value);

	input_analogcallback(command, value / JOY_AXIS_MAX);
}

static void Input_OnJoyHatEvent(const SDL_Event &event, int command,
		int hatbit)
{
	if (!input_digitalcallback)
		return;

	if (SDL_IsGameController(event.jhat.which))
		return;

	SDL_LogDebug(SDL_LOG_CATEGORY_INPUT, "Joy %d: hat %d dir %x = %d",
			event.jhat.which,
			event.jhat.hat,
			hatbit,
			(event.jhat.value & hatbit) ? 1 : 0);

	input_digitalcallback(command, event.jhat.value & hatbit);
}

static void Input_OnJoyButtonEvent(const SDL_Event &event, int command)
{
	if (!input_digitalcallback)
		return;

	if (SDL_IsGameController(event.jbutton.which))
		return;

	SDL_LogDebug(SDL_LOG_CATEGORY_INPUT, "Joy %d: button %d = %d",
			event.jbutton.which,
			event.jbutton.button,
			event.jbutton.state);

	input_digitalcallback(command, event.jbutton.state == SDL_PRESSED);
}

static void Input_OnControllerAxisEvent(const SDL_Event &event,
		int command)
{
	if (!input_analogcallback)
		return;

	if (!SDL_IsGameController(event.caxis.which))
		return;

	float value = event.caxis.value;
	if (value > 0)
		value += 1;

	SDL_LogDebug(SDL_LOG_CATEGORY_INPUT, "Controller %d: axis %d = %d",
			event.caxis.which, event.caxis.axis, event.caxis.value);

	input_analogcallback(command, value / JOY_AXIS_MAX);
}

static void Input_OnControllerButtonEvent(const SDL_Event &event,
		int command)
{
	if (!input_digitalcallback)
		return;

	if (!SDL_IsGameController(event.cbutton.which))
		return;

	SDL_LogDebug(SDL_LOG_CATEGORY_INPUT, "Controller %d: button %d = %d",
			event.cbutton.which,
			event.cbutton.button,
			event.cbutton.state);

	input_digitalcallback(command, event.cbutton.state == SDL_PRESSED);
}

static void Input_OnMouseMotionEvent(const SDL_Event &event, int command)
{
	if (!input_pointcallback)
		return;

	SDL_LogDebug(SDL_LOG_CATEGORY_INPUT, "Mouse %d: move %d, %d",
			event.motion.which,
			event.motion.xrel,
			event.motion.yrel);

	input_pointcallback(event.motion.state,
			event.motion.xrel, event.motion.yrel);
}

static void Input_OnMouseButtonEvent(const SDL_Event &event, int command)
{
	if (!input_digitalcallback)
		return;

	input_digitalcallback(command, event.button.state == SDL_PRESSED);
}

static void Input_FillEventForMap(SDL_Event &mapevent,
		int device, int value)
{
	int type = mapevent.type;
	memset(&mapevent, 0, sizeof(SDL_Event));
	mapevent.type = type;

	switch (type)
	{
	case SDL_KEYDOWN:
	case SDL_KEYUP:
		mapevent.type = SDL_KEYDOWN;
		mapevent.key.keysym.sym = value;
		break;

	case SDL_JOYAXISMOTION:
		mapevent.jaxis.which = device;
		mapevent.jaxis.axis = value;
		break;

	case SDL_JOYHATMOTION:
		mapevent.jhat.which = device;
		mapevent.jhat.value = value;
		break;

	case SDL_JOYBUTTONDOWN:
	case SDL_JOYBUTTONUP:
		mapevent.type = SDL_JOYBUTTONDOWN;
		mapevent.jbutton.which = device;
		mapevent.jbutton.button = value;
		break;

	case SDL_CONTROLLERAXISMOTION:
		mapevent.caxis.which = device;
		mapevent.caxis.axis = value;
		break;

	case SDL_CONTROLLERBUTTONDOWN:
	case SDL_CONTROLLERBUTTONUP:
		mapevent.type = SDL_CONTROLLERBUTTONDOWN;
		mapevent.cbutton.which = device;
		mapevent.cbutton.button = value;
		break;

	case SDL_MOUSEMOTION:
		mapevent.motion.which = device;
		mapevent.motion.state = value;
		break;

	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
		mapevent.type = SDL_MOUSEBUTTONDOWN;
		mapevent.button.which = device;
		mapevent.button.button = value;
		break;
	}
}

static void Input_StripEventForMap(SDL_Event &mapevent)
{
	int device = 0;
	int value = 0;

	switch (mapevent.type)
	{
	case SDL_KEYDOWN:
	case SDL_KEYUP:
		// only 1 keyboard device supported at present
		value = mapevent.key.keysym.sym;
		break;

	case SDL_JOYAXISMOTION:
		device = mapevent.jaxis.which;
		value = mapevent.jaxis.axis;
		break;

	case SDL_JOYHATMOTION:
		device = mapevent.jhat.which;
		value = mapevent.jhat.value;
		break;

	case SDL_JOYBUTTONDOWN:
	case SDL_JOYBUTTONUP:
		device = mapevent.jbutton.which;
		value = mapevent.jbutton.button;
		break;

	case SDL_CONTROLLERAXISMOTION:
		device = mapevent.caxis.which;
		value = mapevent.caxis.axis;
		break;

	case SDL_CONTROLLERBUTTONDOWN:
	case SDL_CONTROLLERBUTTONUP:
		device = mapevent.cbutton.which;
		value = mapevent.cbutton.button;
		break;

	case SDL_MOUSEMOTION:
		device = mapevent.motion.which;
		// value = button flags
		// don't need to call different callbacks per button state
		break;

	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
		device = mapevent.button.which;
		value = mapevent.button.button;
		break;
	}

	Input_FillEventForMap(mapevent, device, value);
}

void Input_ClearEventCommands()
{
	input_eventcommands.clear();
}

void Input_SetEventCommand(int eventtype, int device,
		int eventvalue, int command)
{
	SDL_Event event;
	memset(&event, 0, sizeof(SDL_Event));
	event.type = eventtype;

	Input_FillEventForMap(event, device, eventvalue);
	input_eventcommands[event] = command;
}

void Input_SetEventCommands(int eventtype, int device,
		int n, int eventvalues[], int commands[])
{
	if (n)
		SDL_assert(eventvalues && commands);

	for (int i = 0; i < n; ++i)
	{
		Input_SetEventCommand(eventtype, device,
				eventvalues[i], commands[i]);
	}
}

void Input_OnEvent(const SDL_Event &event)
{
	SDL_Event mapevent = event;
	Input_StripEventForMap(mapevent);

	EventCommandMap::iterator it;
	int command;

	switch (event.type)
	{
	case SDL_JOYHATMOTION:
		break;

	default:
		it = input_eventcommands.find(mapevent);
		if (it == input_eventcommands.end())
			return;
		command = it->second;
		break;
	}

	switch (event.type)
	{
	case SDL_KEYDOWN:
	case SDL_KEYUP:
		Input_OnKeyEvent(event, command);
		break;

	case SDL_JOYAXISMOTION:
		Input_OnJoyAxisEvent(event, command);
		break;

	case SDL_JOYHATMOTION:
		for (int i = 0; i < 4; ++i)
		{
			mapevent.jhat.value = 1 << i;
			it = input_eventcommands.find(mapevent);
			if (it == input_eventcommands.end())
				continue;

			command = it->second;
			Input_OnJoyHatEvent(event, command, 1 << i);
		}
		break;

	case SDL_JOYBUTTONDOWN:
	case SDL_JOYBUTTONUP:
		Input_OnJoyButtonEvent(event, command);
		break;

	case SDL_CONTROLLERAXISMOTION:
		Input_OnControllerAxisEvent(event, command);
		break;

	case SDL_CONTROLLERBUTTONDOWN:
	case SDL_CONTROLLERBUTTONUP:
		Input_OnControllerButtonEvent(event, command);
		break;

	case SDL_MOUSEMOTION:
		Input_OnMouseMotionEvent(event, command);
		break;

	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
		Input_OnMouseButtonEvent(event, command);
		break;
	}
}
