#include "bullet.hpp"

#include "ship.hpp"

#include "body.hpp"
#include "sprite.hpp"
#include "cel.hpp"
#include "database.hpp"

#include "alist.hpp"

#include "units.hpp"

#include "SDL.h"

#include <cmath>
#include <cstring>

struct Bullet
{
	int health;

	Body *body;
	Sprite *sprite;

	Data properties;

	Bullet()
		: properties()
	{
		health = 1;
		body = NULL;
		sprite = NULL;
	}
};

static void Bullet_Kill(Bullet &bullet)
{
	Sprite_SetAnimation(bullet.sprite, ANIMLOOP_STOP);

	Data p;
	if ((p = bullet.properties.Find("hitspark")))
	{
		Bullet_New(p.CStr(), Body_Pos_Micron(bullet.body), 0, 0);
	}
}

static void Bullet_OnHit(void *owner, const Hit &hit)
{
	Bullet *bullet = (Bullet*)owner;

	int damage = 0;

	if (hit.hittergroupflags & HITGROUP_SHIP_FLAGS)
	{
		//Ship *ship = (Ship*)(hit.hitter);
		damage += bullet->health; // TODO get this from ship?
	}

	if (hit.hittergroupflags & HITGROUP_SHIELD_FLAGS)
	{
		damage += 1;//SHIELD_DAMAGE?
	}

	if (bullet->health > 0 && bullet->health - damage <= 0)
	{
		Bullet_Kill(*bullet);
	}

	bullet->health -= damage;
}

static void Bullet_Init(Bullet &bullet, const char *type,
		const SDL_Point *pos_mi, double angle_rad, int speed_mi,
		const SDL_Point *accel_mi, int frame)
{
	bullet.health = std::max(1.0, std::abs(MicronToPixel(speed_mi)));

	SDL_Rect volume =
	{
		PixelToMicron(-8),
		PixelToMicron(-8),
		PixelToMicron(16),
		PixelToMicron(16)
	};
	SDL_Rect spriterect =
	{
		MicronToPixel(pos_mi->x),
		MicronToPixel(pos_mi->y),
		0,
		0
	};
	//unsigned timeout = 6;
	int ingroupflags = 0;
	int hitsgroupflags = 0;
	char *cel = NULL;

	int animloopct = ANIMLOOP_INFINITE;
	Data ps = Database_Get("objects", type);
	if (ps)
	{
		Data p;
		if ((p=ps.Find("volume.x"))) volume.x = PixelToMicron(p.Int());
		if ((p=ps.Find("volume.y"))) volume.y = PixelToMicron(p.Int());
		if ((p=ps.Find("volume.w"))) volume.w = PixelToMicron(p.Int());
		if ((p=ps.Find("volume.h"))) volume.h = PixelToMicron(p.Int());
		if ((p=ps.Find("health"))) bullet.health = (p.Int());

		if ((p = ps.Find("team")))
		{
			if (!std::strcmp(p.CStr(), "enemy"))
			{
				ingroupflags = 1 << (HITGROUP_EBULLET);
				hitsgroupflags = 1 << HITGROUP_PSHIP
					| 1 << HITGROUP_PSHIELD;
			}
			else
			if (!std::strcmp(p.CStr(), "player"))
			{
				ingroupflags = 1 << (HITGROUP_PBULLET);
				hitsgroupflags = 1 << (HITGROUP_ESHIP);
			}
			else
			if (!std::strcmp(p.CStr(), "spark"))
			{
				animloopct = 1;
			}
		}

		if ((p=ps.Find("cel")))
			cel = p.CStr();

		bullet.properties = ps;
	}

	bullet.body = Body_New(VOLUME_BOX, pos_mi, 1,
			ingroupflags, hitsgroupflags,
			&bullet, Bullet_OnHit);
	bullet.sprite = Sprite_New(&spriterect, Cel_Get(cel), frame);
	Sprite_SetAnimation(bullet.sprite, animloopct, frame);
	Body *body = (bullet.body);
	Sprite *sprite = (bullet.sprite);

	Body_SetVolumeBox(body, &volume);
	SDL_Point vel;
	vel.x = std::cos(angle_rad) * speed_mi;
	vel.y = std::sin(angle_rad) * speed_mi;
	Body_SetVel(body, &vel);
	if (accel_mi)
		Body_SetAccel(body, accel_mi->x, accel_mi->y);

	Sprite_SetToBody(sprite, body);
	Sprite_SetAngle(sprite, RadianToDegree(angle_rad));
	Sprite_SetBlendMode(sprite, SDL_BLENDMODE_ADD);
}

static void Bullet_Uninit(Bullet &bullet)
{
	Body_Del(bullet.body);
	Sprite_Del(bullet.sprite);
}

static bool Bullet_OutOfBounds(const Bullet &bullet)
{
	Body *body = (bullet.body);

	return !Body_IsInRect(body, &SCREEN_BOUNDS_MI);
}

static bool Bullet_IsDead(const Bullet &bullet)
{
	return Bullet_OutOfBounds(bullet) || Sprite_AnimStopped(bullet.sprite);
}

static void Bullet_Tick(Bullet &bullet)
{
}

static void Bullet_PreRender(Bullet &bullet, int dt)
{
	Body *body = (bullet.body);
	Sprite *sprite = (bullet.sprite);
	Sprite_SetToBody(sprite, body);
}

typedef AList<Bullet> BulletList;
static BulletList allbullets;

void AllBullets_Init(int n)
{
	allbullets.Init(n);
}

void AllBullets_Tick()
{
	allbullets.DelIf(Bullet_IsDead, Bullet_Uninit);
	allbullets.ForEach(Bullet_Tick);
}

void AllBullets_PreRender(int dt)
{
	allbullets.ForEach(Bullet_PreRender, dt);
}

Bullet* Bullet_New(const char *type, const SDL_Point *pos_mi, double angle_rad,
		int speed_mi, const SDL_Point *accel_mi, int frame)
{
	if (AllBodies_Full() || AllSprites_Full())
		return NULL;

	Bullet *bullet = allbullets.Push();

	if (bullet)
	{
		Bullet_Init(*bullet, type, pos_mi, angle_rad, speed_mi,
				accel_mi, frame);
	}

	return bullet;
}

static void Bullet_KillEnemy(Bullet &bullet)
{
	Data ps = bullet.properties;
	if (ps)
	{
		Data p;
		if ((p = ps.Find("team")))
		{
			if (std::strcmp(p.CStr(), "enemy") == 0)
				Bullet_Kill(bullet);
		}
	}
}

void AllBullets_KillEnemy()
{
	allbullets.ForEach(Bullet_KillEnemy);
}

