#ifndef __BODY_STRUCT_H__
#define __BODY_STRUCT_H__

#include "body.hpp"
#include "SDL.h"

struct BoxVolume
{
	VolumeType type;
	SDL_Rect rect;
};

struct BallVolume
{
	VolumeType type;
	int radius;
	SDL_Point center;
};

struct RayVolume
{
	VolumeType type;
	SDL_Point vec;
	SDL_Point origin;
	int radius;
};

union Volume
{
	VolumeType type;
	BoxVolume box;
	BallVolume ball;
	RayVolume ray;

	Volume(VolumeType t = VOLUME_NONE);
};

struct Body
{
	//SDL_Rect vol_mi;
	Volume vol_mi;
	SDL_Point pos_mi;
	SDL_Point vel_mi;
	SDL_Point accel_mi;
	int mass;

	int ingroupflags;    // take hits from bodies that can hit these groups
	int hitsgroupflags;  // hit bodies that are in these groups
	//int hitflags;

	void *owner;
	BodyOwnerHitCallback ownerhitcallback;

	Body(//const SDL_Rect *vol = NULL,
		VolumeType voltype = VOLUME_BOX,
		const SDL_Point *p = NULL,
		//const SDL_Point *v = NULL,
		//const SDL_Point *a = NULL,
		int m = 1,
		int ingroup = 0,
		int hitsgroup = 0,
		void *o = NULL,
		BodyOwnerHitCallback ohcb = NULL)
	{
		vol_mi = Volume(voltype);
		//vol_mi.x = vol_mi.y = 0;
		//vol_mi.w = vol_mi.h = 0;
		pos_mi.x = pos_mi.y = 0;
		vel_mi.x = vel_mi.y = 0;
		accel_mi.x = accel_mi.y = 0;

		//if (vol)
		//	vol_mi = *vol;
		if (p)
			pos_mi = *p;
		//if (v)
		//	vel_mi = *v;
		//if (a)
		//	accel_mi = *a;
		mass = m;

		ingroupflags = ingroup;
		hitsgroupflags = hitsgroup;
		//hitflags = 0;
		owner = o;
		ownerhitcallback = ohcb;
	}
};

void Body_Tick(Body &body);

void Body_Draw(Body &body);

#endif
