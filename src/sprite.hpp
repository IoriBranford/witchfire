//	Sprite -- rectangular object to display on screen

#ifndef __SPRITE_H__
#define __SPRITE_H__

#include "SDL.h"

struct Sprite;
struct Cel;

void Sprite_SetCel(Sprite *sprite, const Cel *cel);

enum
{
	ANIMLOOP_INFINITE = -1,
	ANIMLOOP_STOP
};
void Sprite_SetAnimation(Sprite *sprite, int loopct, int startframe = 0);
bool Sprite_AnimStopped(Sprite *sprite);

void Sprite_SetAngle(Sprite *sprite, float angle_deg);
void Sprite_SetCenter(Sprite *sprite, const SDL_Point *center_px);
void Sprite_SetBlendMode(Sprite *sprite, SDL_BlendMode blendmode);

enum DrawFlag
{
	DRAW_TEX	= 1<<0,
	DRAW_RECT	= 1<<1
};

void Sprite_EnableDrawFlags(Sprite *sprite, int drawflags);
void Sprite_DisableDrawFlags(Sprite *sprite, int drawflags);

void AllSprites_Init(int n);
void AllSprites_Prune();
void AllSprites_PreRender(int dt);
void AllSprites_Draw();
bool AllSprites_Full();
Sprite* Sprite_New(const SDL_Rect *dr		= NULL,
		const Cel *cel		= NULL,
		int frm				= 0,
		const SDL_RendererFlip f	= SDL_FLIP_NONE,
		const float a			= 0,
		const SDL_Point *c		= NULL);
void Sprite_Del(Sprite *sprite);

struct Body;
void Sprite_SetToBody(Sprite *sprite, const Body *body);

#endif
