#ifndef __ALIST_H__
#define __ALIST_H__

#include <vector>
#include <cassert>
#include <cstdio>

template <class Value>
class AList
{
public:
	AList()
	{
		Init(0);
	}

	AList(size_t n)
	{
		Init(n);
	}

	size_t Size()
	{
		return alist.size();
	}

	void Init(size_t n)
	{
		alist.clear();

		if (!n)
		{
			free = NULL;
			return;
		}

		n = std::min(n, (size_t)1 << ((sizeof(size_t) << 3) - 1));
		alist.reserve(n);
		for (size_t i = 0; i < n; ++i)
			alist.push_back(Entry(Value(), NULL));
		for (size_t i = 1; i < n; ++i)
			alist[i - 1].next = &alist[i];
		alist[n - 1].next = NULL;

		head = NULL;
		free = &alist[0];
	}

	void Clear()
	{
		Init(0);
	}

	bool Empty()
	{
		return head == NULL;
	}

	bool Full()
	{
		return free == NULL;
	}

	Value* Push(const Value &value = Value())
	{
		if (Full())
			return NULL;

		Entry *pushed = free;
		free = pushed->next;

		pushed->next = head;
		head = pushed;

		pushed->value = value;

		return &pushed->value;
	}

	void Pop()
	{
		if (Empty())
			return;

		Entry *popped = head;
		head = popped->next;

		popped->next = free;
		free = popped;
	}

	// TODO if needed int InsAfter(int , Value value)

	//void Pop(int )
	//{
	//	assert( == NULL || (0 <=  &&  < alist.size()));
        //
	//	if ( != NULL)
	//	{
	//		alist[].next = free;
	//		free = ;
	//	}
	//}

	//Value& operator[](int )
	//{
	//	assert(0 <=  &&  < alist.size());
	//	return alist[].value;
	//}

private:
	struct Entry
	{
		Value value;
		Entry *next;

		Entry(Value v, Entry *n)
		{
			value = v;
			next = n;
		}
	};
	Entry *head;
	Entry *free;
	std::vector<Entry> alist;

public:
	struct Iterator
	{
		Iterator(Entry *e)
		{
			entry = e;
		}

		bool operator==(const Iterator &it)
		{
			return entry == it.entry;
		}

		bool operator!=(const Iterator &it)
		{
			return entry != it.entry;
		}

		Value& operator*()
		{
			return entry->value;
		}

		Value* operator->()
		{
			return &entry->value;
		}

		Iterator operator++()
		{
			entry = entry->next;
			return *this;
		}
		
		Iterator operator++(int)
		{
			entry = entry->next;
			return *this;
		}
		
		Entry *entry;
	};

	Iterator Begin()
	{
		return Iterator(head);
	}

	Iterator End()
	{
		return Iterator(NULL);
	}

	void DelAfter(Iterator it)
	{
		Entry *entry = it.entry;

		if (!entry)
			return;

		Entry *deleted = entry->next;

		if (!deleted)
			return;

		entry->next = deleted->next;

		deleted->next = free;
		free = deleted;
	}

	void ForEach(void (*func)(Value&))
	{
		assert(func);

		for (Iterator it = Begin(); it != End(); ++it)
		{
			func(*it);
		}
	}

	template <class T> void ForEach(void (*func)(Value&, T), T data)
	{
		assert(func);

		for (Iterator it = Begin(); it != End(); ++it)
		{
			func(*it, data);
		}
	}

	void DelIf(bool (*shoulddel)(const Value&),
			void (*uninit)(Value&) = NULL)
	{
		assert(shoulddel);

		while (!Empty() && shoulddel(head->value))
		{
			if (uninit)
				uninit(head->value);
			Pop();
		}

		if (Empty())
			return;

		Iterator it0 = Begin();
		Iterator it1 = Begin();
		for (++it1; it1 != End(); ++it1)
		{
			if (shoulddel(*it1))
			{
				if (uninit)
					uninit(*it1);
				DelAfter(it0);
			}
			else
			{
				++it0;
			}

			it1 = it0;
		}
	}

	void PrintFree()
	{
		for (Iterator it = Begin(); it != End(); ++it)
		{
			std::printf("%p ", it.entry);
		}
		std::printf("\n");
	}
};

#endif
