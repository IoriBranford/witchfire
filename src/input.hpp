//	input -- convert user input to commands sent via callbacks

#ifndef __INPUT_H__
#define __INPUT_H__

#include "SDL.h"

typedef void (*DigitalCallback)(int command, bool b);
typedef void (*AnalogCallback)(int command, float v);
typedef void (*PointCallback)(int commandflags, float x, float y);

void Input_Init();
void Input_Uninit();

void Input_SetDigitalCallback(DigitalCallback digitalcallback);
void Input_SetAnalogCallback(AnalogCallback analogcallback);
void Input_SetPointCallback(PointCallback pointcallback);
void Input_ClearCallbacks();

void Input_ClearEventCommands();
void Input_SetEventCommand(int eventtype, int device,
		int eventvalue, int command);
void Input_SetEventCommands(int eventtype, int device,
		int n, int eventvalues[], int commands[]);
// For eventtype SDL_MOUSEMOTION: eventvalue is ignored, command is irrelevant

void Input_OnEvent(const SDL_Event &event);

void Input_ResetDefaultEventCommands();

#endif
