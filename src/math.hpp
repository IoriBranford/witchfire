#ifndef __MATH_H__
#define __MATH_H__

#include "units.hpp"

int Sin(int x)
{
	int sign = (x / HALF_CIRCLE) % 2 == 0 ? 1 : -1;
	if (x < 0)
		sign = -sign;
	x = std::abs(x % HALF_CIRCLE);
	int xx = x*4 * (HALF_CIRCLE - x);
	return sign*xx*4 / ((5 * HALF_CIRCLE * HALF_CIRCLE) - xx);
}

int Cos(int x)
{
	return Sin((HALF_CIRCLE / 2) + x);
}

#endif
