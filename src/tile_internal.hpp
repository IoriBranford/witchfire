#ifndef __TILE_INTERNAL_H__
#define __TILE_INTERNAL_H__

#include "SDL.h"

#include <string>
#include <map>
#include <vector>

struct Tileset
{
	float tcw;
	float tch;
	short tilew;
	short tileh;
	short cols;
	short rows;
	unsigned int texture;

	Tileset(
			float tcw = 1, float tch = 1,
			short tw = 1, short th = 1,
			short c = 1, short r = 1,
			unsigned int t = 0)
		:
			tcw(tcw), tch(tch),
			tilew(tw), tileh(th),
			cols(c), rows(r),
			texture(t)
	{
	}

	float TileTCX(int tile) const
	{
		return tcw * (tile % cols);
	}

	float TileTCY(int tile) const
	{
		return tch * (tile / cols);
	}

	int TileCt() const
	{
		return cols * rows;
	}
};

enum LoopState
{
	LOOP_INACTIVE,
	LOOP_ACTIVE,
	LOOP_DEACTIVATING
};

struct TileLayer
{
	short cols;
	short rows;
	int depth;
	const Tileset *tileset;
	std::vector<short> tiles;

	TileLayer(int c, int r, int d, const Tileset *ts)
		: cols(c), rows(r), depth(d), tileset(ts) 
	{
		tiles.resize(c * r, 0);
	}

	short& operator[](int i)
	{
		return tiles[i];
	}

	int Index(int c, int r) const
	{
		return (c * rows) + r;
	}

	short& Tile(int c, int r)
	{
		return tiles[Index(c, r)];
	}

	int Size()
	{
		return cols * rows;
	}
};

extern std::map<std::string, Tileset> alltilesets;
extern std::vector<TileLayer> alltilelayers;
extern SDL_Rect tilemap_viewrect_mi;
extern SDL_Rect tilemap_looprect_mi;
extern LoopState tilemap_loopstate;

void TileLayer_Draw(TileLayer &tilelayer);

#endif
