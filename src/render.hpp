#ifndef __RENDER_H__
#define __RENDER_H__

#include "SDL.h"

bool Render_Init(SDL_Window *window, Uint32 rendererflags);

void Render_Clear();

void Render_SetDrawingTransform(float x = 0, float y = 0, float z = 0,
		float angle = 0);
void Render_SetDrawingTexture(unsigned int texture);

void Render_BeginDrawing();
void Render_DrawQuad(float x0, float y0, float x1, float y1,
		float tx0 = 0, float ty0 = 0, float tx1 = 1, float ty1 = 1,
		float z = 0);
void Render_EndDrawing();

void Render_Present(SDL_Window *window);

#endif
