#ifndef __DATABASE_H__
#define __DATABASE_H__

#include "data.hpp"

bool Database_LoadInto(const char *filename, const char *dbname,
		int mapdataopenflags);

Data Database_Get(const char *dbname, const char *dataname);
void AllDatabases_Unload();

#endif
