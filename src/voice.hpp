// voice -- noise-making object

#ifndef __VOICE_H__
#define __VOICE_H__

struct Voice;

struct Mix_Chunk;

void Voice_SetPosition(Voice *voice, short angle, short distance);
void Voice_Play(Voice *voice, Mix_Chunk *chunk, int loops = 0);
bool Voice_Playing(Voice *voice);
void Voice_Stop(Voice *voice);

void AllVoices_Init(int n);
void AllVoices_Tick();
bool AllVoices_Full();
Voice* Voice_New();
void Voice_Del(Voice *voice);

void AllVoices_SetVolume(int volume);

struct Body;
void Voice_SetToBody(Voice *voice, Body *body);

#endif
