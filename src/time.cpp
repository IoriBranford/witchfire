#include "time.hpp"

#include "SDL.h"

static unsigned time_ticks;

void Time_ResetTicks()
{
	time_ticks = 0;
}

unsigned Time_GetTicks(unsigned fromnow)
{
	return time_ticks + fromnow;
}

void Time_Tick(unsigned ticks)
{
	time_ticks += ticks;
}
