#ifndef _DISK_H_
#define _DISK_H_

#include <string>

bool Disk_Open(const char *arg0);
//bool Disk_AutoSetup();
void Disk_Close();

bool Disk_FileExists(const char *name);

int Disk_CStrOpen(const char* name, char **textp);
void Disk_CStrClose(char *text);

int Disk_TextOpen(const char *name, std::string & text);
int Disk_TextSave(const char *name, const std::string & text);

void Disk_SetWritePathBase();
void Disk_SetWritePathUser();

const char* Disk_DirSlash();

#endif
