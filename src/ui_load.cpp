#include "ui.hpp"
#include "ui_internal.hpp"
#include "body.hpp"
#include "sprite.hpp"
#include "text.hpp"
#include "mapdata.hpp"
#include "cel.hpp"
#include "font.hpp"
#include "music.hpp"
#include "units.hpp"

static const char *UI_DIR = "ui/";

static void UILoad_ReadObject(Data object, Data tilesets)
{
	const Cel *cel = NULL;
	Sprite *sprite = NULL;
	Body *body = NULL;
	Text *text = NULL;

	Data buttonaction;
	Data textfont;
	Data textstring;

	Data properties = object.Find("properties");
	if (properties)
	{
		buttonaction = properties.Find("buttonaction");
		textfont = properties.Find("textfont");
		textstring = properties.Find("textstring");
	}
	
	const char *type = object.Find("type").CStr();
	const char *name = object.Find("name").CStr();

	SDL_Rect dstrect =
	{
		object.Find("x").Int(), object.Find("y").Int(),
		0, 0
	};

	if (!std::strcmp(type, "imagelayer"))
	{
		cel = Cel_Get(object.Find("cel").CStr());
	}
	else
	{
		Data tileset;
		Data gid = object.Find("gid");
		if (gid)
		{
			tileset = MapData_FindGIDTileset(tilesets, gid.Int());
			cel = Cel_Get(tileset.Find("cel").CStr());
			dstrect.y -= tileset.Find("tileheight").Int();
		}
		
		if (buttonaction || !std::strcmp(name, "cursor"))
		{
			SDL_Rect volrect =
			{
				0, 0,
				PixelToMicron(object.Find("width").Int()),
				PixelToMicron(object.Find("height").Int())
			};
			volrect.x = -volrect.w / 2;
			volrect.y = -volrect.h / 2;

			SDL_Point pos =
			{
				PixelToMicron(dstrect.x),
				PixelToMicron(dstrect.y),
			};

			if (tileset)
			{
				pos.x += PixelToMicron(tileset.Find("tilewidth").Int()) / 2;
				pos.y += PixelToMicron(tileset.Find("tileheight").Int()) / 2;
			}
			else
			{
				pos.x -= volrect.x;
				pos.y -= volrect.y;
			}

			int ingroupflags = 1 << HITGROUP_UIBUTTON;
			int hitsgroupflags = 1 << HITGROUP_UICURSOR;

			body = Body_New(VOLUME_BOX, &pos);

			if (body)
			{
				if (!std::strcmp(name, "cursor"))
				{
					ingroupflags = 1 << HITGROUP_UICURSOR;
					hitsgroupflags = 1 << HITGROUP_UIBUTTON;
				}

				Body_SetVolumeBox(body, &volrect);
				Body_SetInGroupFlags(body, ingroupflags, true);
				Body_SetHitsGroupFlags(body, hitsgroupflags, 1);
			}
		}
	}

	if (cel)
	{
		sprite = Sprite_New(&dstrect, cel);
		Sprite_SetAnimation(sprite, -1);
	}

	if (textstring)
	{
		std::string font ("Unifont16");
		if (textfont)
			if (textfont.CStr() && textfont.CStr()[0])
				font = textfont.CStr();
		text = Text_New(font.c_str(), &dstrect, textstring.CStr());
	}

	UIWidget *newwidget = UIWidget_New(name, body, sprite, text);

	if (buttonaction)
	{
		newwidget->releaseaction = UIAction_Find(buttonaction.CStr());
		Body_SetOwnerAndHitCallback(body, newwidget, NULL);
	}
}

static void UILoad_ReadLayer(Data layer, Data tilesets)
{
	const char *type = layer.Find("type").CStr();

	if (!std::strcmp(type, "imagelayer"))
	{
		UILoad_ReadObject(layer, tilesets);
	}
	else
	if (!std::strcmp(type, "objectgroup"))
	{
		Data os = layer.Find("objects");
		if (os)
			for (Data o = os.Begin(); o != os.End(); ++o)
				UILoad_ReadObject(o, tilesets);
	}
}

static void UILoad_ReadProperty(Data property)
{
	if (property.Named("music"))
	{
		Music_Load(property.CStr());
	}
}

bool UI_Load(const char *name)
{
	std::string path (UI_DIR);
	path += name;
	path += Data::EXT;
	return MapData_FOpen(path.c_str(),
			MAPDATAOPEN_LOADTILESETIMAGES,
			UILoad_ReadLayer, UILoad_ReadProperty);
}
