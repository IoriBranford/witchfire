#ifndef __PLAY_INTERNAL_H__
#define __PLAY_INTERNAL_H__

#include "units.hpp"

enum PlayPhase
{
	PHASE_PLAY,
	PHASE_CLEAR,
	PHASE_GAMEOVER
};

static const int PLAYER_DIGITAL_SPEED = PixelToMicron(4);

void PlayUI_Init();
void PlayUI_UpdateScore();
void PlayUI_UpdateLives();

#endif
