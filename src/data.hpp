#ifndef _DATA_H_
#define _DATA_H_

#include <string>
#include <cstring>
#include "cJSON.h"
#include <yxml.h>

class Data
{
public:
	static const char *EXT;

	Data(cJSON *j = NULL)
	{
		json = j;
	}
	Data(bool b)
	{
		json = cJSON_CreateBool(b);
	}
	Data(const char *s)
	{
		json = cJSON_CreateString(s);
	}
	template<class T> Data(const T & n)
	{
		json = cJSON_CreateNumber((double)n);
	}

	void InitArray()
	{
		json = cJSON_CreateArray();
	}
	void InitObj()
	{
		json = cJSON_CreateObject();
	}

	bool Open(const char *name);
	bool Open(const std::string & name)
	{
		return Open(name.c_str());
	}
	bool FOpen(const char *path);
	bool FOpen(const std::string & path)
	{
		return FOpen(path.c_str());
	}
	bool Parse(const char *text);

	bool Save(const char *name);
	bool Save(const std::string & name)
	{
		return Save(name.c_str());
	}
	bool FSave(const char *path);
	bool FSave(const std::string & path)
	{
		return FSave(path.c_str());
	}
	void Close();

	const char* Name() const
	{
		return json->string;
	}
	bool Named(const char *str)
	{
		return !std::strcmp(json->string, str);
	}
	bool Named(const std::string & str)
	{
		return Named(str.c_str());
	}
	bool NamePrefixed(const char *prefix)
	{
		return std::strstr(json->string, prefix) == json->string;
	}
	bool NamePrefixed(const std::string & str)
	{
		return NamePrefixed(str.c_str());
	}

	int Int() const
	{
		return json->valueint;
	}
	float Float() const
	{
		return (float)json->valuedouble;
	}
	char* CStr() const
	{
		return json ? json->valuestring : NULL;
	}
	bool Bool() const
	{
		return json->type == cJSON_True;
	}

	bool Empty() const
	{
		return !json->child;
	}
	Data Begin() const
	{
		return Data(json->child);
	}
	Data End() const
	{
		return Data();
	}
	Data Find(const char *name) const
	{
		Data child (cJSON_GetObjectItem(json, name));
		//assert(child);
		return child;
	}

	operator bool() const
	{
		return json != NULL;
	}
	bool operator==(Data d) const
	{
		return json == d.json;
	}
	bool operator!=(Data d) const
	{
		return !(*this == d);
	}
	Data& operator++()
	{
		json = json->next;
		return *this;
	}
	Data& operator++(int)
	{
		return ++(*this);
	}
	Data& operator--()
	{
		json = json->prev;
		return *this;
	}
	Data& operator--(int)
	{
		return --(*this);
	}
	Data& operator=(const Data & d)
	{
		json = d.json;
		return *this;
	}

	Data InsFront(const char *dname, Data d)
	{
		return InsAfter(Data(), dname, d);
	}
	Data InsAfter(Data it, const char *dname, Data d);
	Data Replace(Data it, const char *dname, Data d);

	Data Clone()
	{
		return Data(cJSON_Duplicate(json, true));
	}

	void ParseStrIntoNum();

	bool FOpenXML(const char *path);
	bool ParseXML(const char *text);

private:
	void Rename(const char *dname);

	int ParseXMLElement(yxml_t &xml, const char **text);

	cJSON *json;
};

#endif
