#include "stage_internal.hpp"
#include "script.hpp"

static int Vel(lua_State *machine)
{
	ScriptFunc_Return(machine, (double)stage_vel_mi.x);
	ScriptFunc_Return(machine, (double)stage_vel_mi.y);
	return 2;
}

void StageScript_RegisterLib()
{
	const luaL_Reg LIB[] =
	{
		SCRIPTFUNC_REG(Vel),
		{NULL, NULL}
	};

	ScriptFunc_RegisterLib("Stage", LIB);
}
