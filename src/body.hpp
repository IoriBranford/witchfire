#ifndef __BODY_H__
#define __BODY_H__

#include "SDL.h"

#include "alist.hpp"

struct Body;
// AABB physics object

typedef AList<Body> BodyList;

enum VolumeType
{
	VOLUME_NONE,
	VOLUME_BOX,
	VOLUME_BALL,
	VOLUME_RAY
};

//void Body_SetVolume(Body *body, const SDL_Rect *vol_mi);
void Body_SetVolumeBox(Body *body, const SDL_Rect *rect_mi);
void Body_SetVolumeBall(Body *body, int radius_mi,
		const SDL_Point *center_mi = 0);
void Body_SetVolumeRay(Body *body, const SDL_Point *vec_mi,
		const SDL_Point *origin_mi = 0, int radius_mi = 0);
void Body_CopyVolume(Body *dstbody, const Body *srcbody);

void Body_SetPos(Body *body, const SDL_Point *pos_mi);
void Body_SetVel(Body *body, const SDL_Point *vel_mi);

const SDL_Point* Body_Pos_Micron(const Body *body);
const SDL_Point* Body_Vel_Micron(const Body *body);
const SDL_Point* Body_PosInTime_Micron(const Body *body, int t = 0);

// Move by adding forces to determine accel this tick
// 0. Reset accel to initial values, if don't want forces from previous tick
void Body_SetAccel(Body *body, int ax_mi, int ay_mi);
// 1. Add internal forces
void Body_AddAccelTowardVel(Body *body, int vx_mi, int vy_mi);
// 2. Add external forces
void Body_AddAccelToKeepIn(Body *body, const SDL_Rect *bounds_mi,
		float bouncefactor = 0);
void Body_AddAccelToFollow(Body *body, const Body *leadbody,
		const SDL_Point *offset_mi = NULL);

// Simple movement by directly manipulating position
void Body_AddPosAndKeepIn(Body *body, int x_mi, int y_mi,
		const SDL_Rect *bounds_mi);

// Place rect at body pos with alignment matching body volume's
void Body_SetSpriteDstRect(const Body *body, SDL_Rect *dstrect_px);

enum BodyOverlapType
{
	OVERLAP_RECT,
	OVERLAP_POINTS
};

struct RectOverlap
{
	BodyOverlapType type;
	SDL_Rect rect;
};

struct PointsOverlap
{
	BodyOverlapType type;
	SDL_Point p0;
	SDL_Point p1;
};

union BodyOverlap
{
	BodyOverlapType type;
	RectOverlap rect;
	PointsOverlap points;
};

struct Hit
{
	void *hitter;
	int hittergroupflags;
	BodyOverlap overlap_mi;
};

enum HitGroup
{
	HITGROUP_PSHIP	,
	HITGROUP_ESHIP	,
	HITGROUP_PBULLET,
	HITGROUP_EBULLET,
	HITGROUP_PMELEE ,
	HITGROUP_EMELEE ,
	HITGROUP_PSHIELD ,
	HITGROUP_ESHIELD ,
	HITGROUP_UICURSOR,
	HITGROUP_UIBUTTON
};

static const int HITGROUP_SHIP_FLAGS =
		(1 << HITGROUP_ESHIP) |
		(1 << HITGROUP_PSHIP);

static const int HITGROUP_BULLET_FLAGS =
		(1 << HITGROUP_EBULLET) |
		(1 << HITGROUP_PBULLET);

static const int HITGROUP_MELEE_FLAGS =
		(1 << HITGROUP_EMELEE) |
		(1 << HITGROUP_PMELEE);

static const int HITGROUP_SHIELD_FLAGS =
		(1 << HITGROUP_ESHIELD) |
		(1 << HITGROUP_PSHIELD);

typedef void (*BodyOwnerHitCallback)(void *owner, const Hit &hit);

void Body_SetInGroupFlags(Body *body, int ingroupflags, bool on);
void Body_SetHitsGroupFlags(Body *body, int hitsgroupflags, bool on);
void Body_SetOwnerAndHitCallback(Body *body, void *owner,
		BodyOwnerHitCallback ownerhitcallback);

bool Body_IsInRect(const Body *body, const SDL_Rect *rect_mi,
		BodyOverlap *overlap_mi = NULL);
//bool Body_IsHit(const Body *body, int hitflags);
bool Body_IntersectsBody(Body *body0, Body *body1,
		BodyOverlap *overlap_mi = NULL);

void AllBodies_Init(int n);
void AllBodies_Tick();

bool AllBodies_Full();
Body* Body_New(	//const SDL_Rect *vol_mi = 0,
		VolumeType voltype = VOLUME_BOX,
		const SDL_Point *pos_mi = 0,
		int mass = 1,
		int ingroupflags = 0,
		int hitsgroupflags = 0,
		void *owner = NULL,
		BodyOwnerHitCallback hitcallback = NULL
		);
void Body_Del(Body *body);

void AllBodies_Draw();

#endif
