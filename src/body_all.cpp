#include "body.hpp"
#include "body_internal.hpp"

static BodyList allbodies;

#include <climits>
static const int DEL_MARK = INT_MIN;

void AllBodies_Init(int n)
{
	allbodies.Init(n);
}

static bool Body_ShouldDel(const Body &body)
{
	return body.mass == DEL_MARK;
}

void AllBodies_Tick()
{
	allbodies.DelIf(Body_ShouldDel);
	allbodies.ForEach(Body_Tick);

	for (BodyList::Iterator it0 = allbodies.Begin();
			it0 != allbodies.End();
			++it0)
	{
		Body &body0 = *it0;

		if (!(body0.ingroupflags | body0.hitsgroupflags))
			continue;

		BodyList::Iterator it1 = it0;

		for (++it1;
			it1 != allbodies.End();
			++it1)
		{
			Body &body1 = *it1;

			Hit hit;

			if (!Body_IntersectsBody(&body0, &body1,
						&hit.overlap_mi))
				continue;

			int hitflags0 = body0.ingroupflags
				& body1.hitsgroupflags;
			int hitflags1 = body1.ingroupflags
				& body0.hitsgroupflags;

			//body0.hitflags |= hitflags0;
			//body1.hitflags |= hitflags1;

			if (body0.owner != NULL && body0.ownerhitcallback)
			{
				hit.hitter = body1.owner;
				hit.hittergroupflags = hitflags1;
				body0.ownerhitcallback(body0.owner, hit);
			}

			if (body1.owner != NULL && body1.ownerhitcallback)
			{
				hit.hitter = body0.owner;
				hit.hittergroupflags = hitflags0;
				body1.ownerhitcallback(body1.owner, hit);
			}
		}
	}
}

bool AllBodies_Full()
{
	return allbodies.Full();
}

Body* Body_New(VolumeType voltype, const SDL_Point *pos_mi, int mass,
		int ingroupflags, int hitsgroupflags,
		void *owner, BodyOwnerHitCallback ownerhitcallback)
{
	return allbodies.Push(Body(voltype, pos_mi, mass,
				ingroupflags, hitsgroupflags,
				owner, ownerhitcallback));
}

void Body_Del(Body *body)
{
	body->mass = DEL_MARK;
}

void AllBodies_Draw()
{
	allbodies.ForEach(Body_Draw);
}

