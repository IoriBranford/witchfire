#ifndef __APP_HPP__
#define __APP_HPP__

struct SDL_Window;

typedef void (*AppPhaseLoadAssetsFunc) ();
typedef void (*AppPhaseInitFunc) ();
typedef void (*AppPhaseTickFunc) ();
typedef void (*AppPhaseRenderFunc) (int);

void App_RequestQuit();

void App_RequestPhaseChange(AppPhaseLoadAssetsFunc loadassetsfunc = 0,
		AppPhaseInitFunc initfunc = 0,
		AppPhaseTickFunc tickfunc = 0,
		AppPhaseRenderFunc renderfunc = 0);

SDL_Window* App_Window();

#endif
