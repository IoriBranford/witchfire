#include "ship.hpp"
#include "ship_internal.hpp"
#include "script.hpp"
#include "bullet.hpp"
#include "body.hpp"
#include "sprite.hpp"
#include "voice.hpp"
#include "cel.hpp"
#include "sound.hpp"
#include "units.hpp"
#include <cmath>

static Ship *shipscript_myship;

static int MyShip(lua_State *machine)
{
	ScriptFunc_Return(machine, (void*)shipscript_myship);
	return 1;
}

static int PlayerShip(lua_State *machine)
{
	Ship *player = AllShips_Player();
	if (!player || AllShips_PlayerDying())
		ScriptFunc_ReturnNil(machine);
	else
		ScriptFunc_Return(machine, (void*)player);

	return 1;
}

static int FindNearestOtherShip(lua_State *machine)
{
	Ship *ship = (Ship*)ScriptFunc_Arg(machine, "ship", (void*)0);
	SCRIPTFUNC_REQUIREARG(ship);

	Ship *foundship = NULL;
	int nearestdistsq = Ship_FindNearestOtherShip(ship, &foundship);
	
	ScriptFunc_Return(machine, (void*)foundship);
	ScriptFunc_Return(machine, std::sqrt(nearestdistsq));
	return 2;
}

static int VecToOtherShip(lua_State *machine)
{
	Ship *ship = (Ship*)ScriptFunc_Arg(machine, "ship", (void*)0);
	Ship *othership = (Ship*)ScriptFunc_Arg(machine, "othership", (void*)0);
	SCRIPTFUNC_REQUIREARG(ship);
	SCRIPTFUNC_REQUIREARG(othership);

	int intime = ScriptFunc_Arg(machine, "intime", 0.0);

	SDL_Point v = *Ship_VecToOtherShip(ship, othership, intime);
	ScriptFunc_Return(machine, (double)v.x);
	ScriptFunc_Return(machine, (double)v.y);

	return 2;
}

static int ShipAlive(lua_State *machine)
{
	Ship *ship = (Ship*)ScriptFunc_Arg(machine, "ship", (void*)0);
	SCRIPTFUNC_REQUIREARG(ship);

	ScriptFunc_Return(machine, !Ship_IsDying(*ship));
	return 1;
}

static int ShipOnScreen(lua_State *machine)
{
	Ship *ship = (Ship*)ScriptFunc_Arg(machine, "ship", (void*)0);
	SCRIPTFUNC_REQUIREARG(ship);

	ScriptFunc_Return(machine,
			Body_IsInRect(ship->body, &SCREEN_BOUNDS_MI));
	return 1;
}

static int SetTargetVel(lua_State *machine)
{
	Ship *ship = (Ship*)ScriptFunc_Arg(machine, "ship", (void*)0);
	SCRIPTFUNC_REQUIREARG(ship);

	ship->analogvalues[ANALOG_X] = ScriptFunc_Arg(machine, "vel_x", 0.0);
	ship->analogvalues[ANALOG_Y] = ScriptFunc_Arg(machine, "vel_y", 0.0);
	return 0;
}

static int FireBullet(lua_State *machine)
{
	Ship *ship = (Ship*)ScriptFunc_Arg(machine, "ship", (void*)0);
	SCRIPTFUNC_REQUIREARG(ship);

	if (!ship || Ship_IsDying(*ship) || Ship_IsDead(*ship))
		return 0;

	const char *type = ScriptFunc_Arg(machine, "type", "");
	int frame = ScriptFunc_Arg(machine, "frame", 0.0);
	double angle_rad = ScriptFunc_Arg(machine, "angle", 0.0);
	int speed_mi = ScriptFunc_Arg(machine, "speed", 0.0);
	SDL_Point accel_mi =
	{
		(int)ScriptFunc_Arg(machine, "accel_x", 0.0),
		(int)ScriptFunc_Arg(machine, "accel_y", 0.0)
	};
	const char *sound = ScriptFunc_Arg(machine, "sound", (const char*)0);

	SDL_Point pos = *Body_Pos_Micron(ship->body);
	SDL_Point off =
	{
		(int)ScriptFunc_Arg(machine, "offset_x", 0.0),
		(int)ScriptFunc_Arg(machine, "offset_y", 0.0)
	};
	pos.x += off.x;
	pos.y += off.y;
	Bullet_New(type, &pos, angle_rad, speed_mi, &accel_mi, frame);

	if (!sound)
	{

	}

	if (sound)
		Voice_Play(ship->voice, Sound_Get(sound), 0);

/*
	const char *flashcel = ScriptFunc_Arg(machine, "flashcel", 
			(const char*)0);
	int flashtime = ScriptFunc_Arg(machine, "flashtime", (int)1);
	if (flashcel);
		Ship_GunFlash(ship, flashcel, flashtime);
*/
	return 0;
}

static int MeleeAttack(lua_State *machine)
{
	Ship *ship = (Ship*)ScriptFunc_Arg(machine, "ship", (void*)0);
	SCRIPTFUNC_REQUIREARG(ship);

	if (Ship_MeleeAttacking(*ship) || Ship_MeleeStunned(*ship))
		return 0;

	int pushdist = ScriptFunc_Arg(machine, "pushdist",
		       (double)	PixelToMicron(80));
	int time = ScriptFunc_Arg(machine, "time", 9.0);
	int stuntime = ScriptFunc_Arg(machine, "stuntime", 12.0);
	Ship_MeleeAttack(*ship, pushdist, time, stuntime);

	return 0;
}

static int SetShield(lua_State *machine)
{
	Ship *ship = (Ship*)ScriptFunc_Arg(machine, "ship", (void*)0);
	SCRIPTFUNC_REQUIREARG(ship);

	if (Ship_MeleeAttacking(*ship) || Ship_MeleeStunned(*ship))
		return 0;

	int radius = ScriptFunc_Arg(machine, "radius", 0.0);

	if (radius)
		Ship_RaiseShield(*ship, radius);
	else
		Ship_LowerShield(*ship);

	return 0;
}

static int SetCel(lua_State *machine)
{
	Ship *ship = (Ship*)ScriptFunc_Arg(machine, "ship", (void*)0);
	SCRIPTFUNC_REQUIREARG(ship);

	const char *cel = ScriptFunc_Arg(machine, "cel", "");
	Sprite_SetCel(ship->sprite, Cel_Get(cel));

	return 0;
}

static int SetAnimation(lua_State *machine)
{
	Ship *ship = (Ship*)ScriptFunc_Arg(machine, "ship", (void*)0);
	SCRIPTFUNC_REQUIREARG(ship);

	int loopct = ScriptFunc_Arg(machine, "loopct", 0.0);
	int startframe = ScriptFunc_Arg(machine, "startframe", 0.0);
	Sprite_SetAnimation(ship->sprite, loopct, startframe);

	return 0;
}

static int PlaySound(lua_State *machine)
{
	Ship *ship = (Ship*)ScriptFunc_Arg(machine, "ship", (void*)0);
	SCRIPTFUNC_REQUIREARG(ship);

	const char *sound = ScriptFunc_Arg(machine, "sound", (const char*)0);
	int loops = ScriptFunc_Arg(machine, "loops", 0.0);

	if (sound)
		Voice_Play(ship->voice, Sound_Get(sound), loops);

	return 0;
}

static int RemoveMyShip(lua_State *machine)
{
	Ship_Exit(*shipscript_myship);
	return 0;
}

void ShipScript_RegisterLib()
{
	const luaL_Reg LIB[] =
	{
		SCRIPTFUNC_REG(MyShip),
		SCRIPTFUNC_REG(PlayerShip),
		SCRIPTFUNC_REG(FindNearestOtherShip),
		SCRIPTFUNC_REG(VecToOtherShip),
		SCRIPTFUNC_REG(ShipAlive),
		SCRIPTFUNC_REG(ShipOnScreen),
		SCRIPTFUNC_REG(SetTargetVel),
		SCRIPTFUNC_REG(FireBullet),
		SCRIPTFUNC_REG(MeleeAttack),
		SCRIPTFUNC_REG(SetShield),
		SCRIPTFUNC_REG(SetCel),
		SCRIPTFUNC_REG(SetAnimation),
		SCRIPTFUNC_REG(PlaySound),
		SCRIPTFUNC_REG(RemoveMyShip),
		{NULL, NULL}
	};

	ScriptFunc_RegisterLib("Ship", LIB);
}

void ShipScript_Tick(Ship &ship)
{
	if (*ship.nextscript || ship.scriptthread)
	{
		shipscript_myship = &ship;
		ship.scriptthread = Script_Resume(ship.scriptthread,
				ship.nextscript, shipscript_myship);
		std::memset(ship.nextscript, 0, sizeof(ship.nextscript));
	}
}
