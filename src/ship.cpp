#include "ship.hpp"
#include "ship_internal.hpp"

#include "bullet.hpp"

#include "body.hpp"
#include "sprite.hpp"
#include "voice.hpp"
#include "stats.hpp"

#include "cel.hpp"
#include "sound.hpp"
#include "script.hpp"
#include "database.hpp"
#include "music.hpp"

#include "units.hpp"

#include "play.hpp"

#include <cstring>

static const SDL_Rect PLAYER_MOVE_BOUNDS =
{
	PixelToMicron(SCREEN_LEFT_PX + 16),
	PixelToMicron(SCREEN_TOP_PX + 16),
	PixelToMicron(SCREEN_RIGHT_PX-32),
	PixelToMicron(SCREEN_BOTTOM_PX-32)
};

Ship::Ship()
{
	std::memset(type, 0, sizeof(type));
	health = 1;
	lifetime = 0;
	digitalflags = 0;
	analogvalues[ANALOG_X] = analogvalues[ANALOG_Y] = 0;
	movebounds = NULL;
	body = NULL;
	sprite = NULL;
	voice = NULL;
	std::memset(nextscript, 0, sizeof(nextscript));
	scriptthread = NULL;
	meleebody = NULL;
	meleesprite = NULL;
	meleeattackpushdist = 0;
	meleeattacktime = 0;
	meleeattackstuntime = 0;
	meleehitstunnedtime = 0;
	shieldbody = NULL;
	shieldsprite = NULL;
}

bool Ship_IsEnemyType(const char *shiptype)
{
	Data ps = Database_Get("objects", shiptype);
	if (ps)
	{
		Data p;
		if ((p = ps.Find("team")))
		{
			return std::strcmp(p.CStr(), "enemy") == 0;
		}
	}
	return false;
}

static void Ship_Kill(Ship &ship)
{
	Data ps = Database_Get("objects", ship.type);
	if (ps)
	{
		Data p;

		if ((p = ps.Find("sound.dying")))
		{
			if (ship.voice)
			{
				Voice_Play(ship.voice, Sound_Get(p.CStr()));
			}
		}

		if ((p = ps.Find("cel.dying")))
		{
			Sprite_SetCel(ship.sprite, Cel_Get(p.CStr()));
			Sprite_SetAnimation(ship.sprite, 1);
		}

		if ((p = ps.Find("killscore")))
		{
			Play_IncScore(p.Int());
		}

		if ((p = ps.Find("killclearenemybullets")))
		{
			if (!std::strcmp(p.CStr(), "all"))
				AllBullets_KillEnemy();
		}

//		if ((p = ps.Find("team")))
//		{
//			if (std::strcmp(p.CStr(), "player"))
//			{
//			}
//		}
	}

	Body_SetHitsGroupFlags(ship.body, HITGROUP_BULLET_FLAGS, false);
	Body_SetHitsGroupFlags(ship.body, 1 << HITGROUP_ESHIP, true);
}

static void Ship_OnHit(void *owner, const Hit &hit)
{
	Ship *ship = (Ship*)owner;

	int damage = 0;

	if (hit.hittergroupflags & HITGROUP_SHIP_FLAGS)
	{
		Ship *hittership = (Ship*)hit.hitter;
		if (!Ship_IsDying(*ship) && Ship_IsDying(*hittership))
			damage += 1 - hittership->health;
			// TODO get this from ship
	}

	if (hit.hittergroupflags & HITGROUP_BULLET_FLAGS)
	{
		damage += 1; // TODO get this from bullet
	}

	if (damage)
	{
		if (!Ship_IsDying(*ship))
		{
			if (ship->health - damage <= 0)
			{
				Ship_Kill(*ship);
			}
		}

		ship->health -= damage;
	}

	if (hit.hittergroupflags & HITGROUP_MELEE_FLAGS)
	{
		Ship *meleeattacker = (Ship*)(hit.hitter);

		Ship_MeleeAttack_Hit(*ship, *meleeattacker);
	}
}

static SDL_Point ship_vectoothership_result;

const SDL_Point* Ship_VecToOtherShip(Ship *ship0, Ship *ship1, int intime)
{
	ship_vectoothership_result = *Body_PosInTime_Micron(ship1->body,intime);
	ship_vectoothership_result.x -= Body_Pos_Micron(ship0->body)->x;
	ship_vectoothership_result.y -= Body_Pos_Micron(ship0->body)->y;
	return &ship_vectoothership_result;
}

void Ship_Init(Ship &ship, const char *type, const SDL_Point *pos,
		const char *script)
{
	std::strcpy(ship.type, type);
	SDL_Rect volume =
	{
		PixelToMicron(-1),
		PixelToMicron(-1),
		PixelToMicron(2	),
		PixelToMicron(2	)
	};
	SDL_Rect spriterect =
	{
		MicronToPixel(pos->x),
		MicronToPixel(pos->y),
		0,
		0
	};
	int ingroupflags = 0;
	int hitsgroupflags = 0;
	char *cel = NULL;

	Data ps = Database_Get("objects", type);
	if (ps)
	{
		Data p;
		if ((p=ps.Find("volume.x"))) volume.x = PixelToMicron(p.Int());
		if ((p=ps.Find("volume.y"))) volume.y = PixelToMicron(p.Int());
		if ((p=ps.Find("volume.w"))) volume.w = PixelToMicron(p.Int());
		if ((p=ps.Find("volume.h"))) volume.h = PixelToMicron(p.Int());
		if ((p=ps.Find("health"))) ship.health = (p.Int());

		if ((p = ps.Find("team")))
		{
			if (!std::strcmp(p.CStr(), "enemy"))
			{
				ingroupflags = 1 << (HITGROUP_ESHIP);
				hitsgroupflags = 1 << HITGROUP_PBULLET
					| 1 << HITGROUP_PMELEE;
			}
			else
			if (!std::strcmp(p.CStr(), "player"))
			{
				ingroupflags = 1 << (HITGROUP_PSHIP);
				hitsgroupflags = 1 << (HITGROUP_EBULLET);
				ship.movebounds = &PLAYER_MOVE_BOUNDS;
			}
		}

		if ((p=ps.Find("cel"))) cel = p.CStr();
		if ((p=ps.Find("music")))
		{
			Music_Fade(120, p.CStr());
		}
	}

	ship.body = Body_New(VOLUME_BOX, pos, 1,
			ingroupflags, hitsgroupflags,
			&ship, Ship_OnHit);

	Body *body = (ship.body);
	Body_SetVolumeBox(body, &volume);

	//const SDL_Point *bodypos = Body_Pos_Micron(body);
	//spriterect.x += bodypos->x;
	//spriterect.y += bodypos->y;
	ship.sprite = Sprite_New(&spriterect, Cel_Get(cel));
	Sprite_SetToBody(ship.sprite, body);
	ship.voice = Voice_New();

	if (script)
		std::strcpy(ship.nextscript, script);

	ship.meleeattacktime = 0;
	ship.meleehitstunnedtime = 0;
}

void Ship_Uninit(Ship &ship)
{
	Ship_MeleeAttack_End(ship);
	Ship_LowerShield(ship);

	Body_Del(ship.body);
	Sprite_Del(ship.sprite);
	Voice_Del(ship.voice);

	if (ship.scriptthread)
		Script_KillThreadOf(&ship);

	if (Ship_IsEnemyType(ship.type))
		AllShips_DecEnemyCt();
}

void Ship_Tick(Ship &ship)
{
	Body *body = (ship.body);

	if (Ship_MeleeStunned(ship))
	{
		Body_SetAccel(body, 0, 0);
	}
	else
	{
		ShipScript_Tick(ship);

		int vx = ship.analogvalues[ANALOG_X];
		int vy = ship.analogvalues[ANALOG_Y];

		Body_SetAccel(body, 0, 0);
		Body_AddAccelTowardVel(body, vx, vy);
	}

	if (ship.movebounds)
	{
		Body_AddAccelToKeepIn(body, ship.movebounds,
				Ship_MeleeStunned(ship) ? 1 : 0);
	}

	Ship_MeleeAttack_Tick(ship);
	Ship_Shield_Tick(ship);

	++ship.lifetime;
}

bool Ship_IsDying(const Ship &ship)
{
	return ship.health <= 0;
}

bool Ship_IsDead(const Ship &ship)
{
	return Ship_IsDying(ship) &&
		(!ship.sprite || Sprite_AnimStopped(ship.sprite));
	//|| !Body_IsInRect((ship.body), &SCREEN_BOUNDS_MI);
}

void Ship_Exit(Ship &ship)
{
	ship.health = 0;
	Sprite_SetAnimation(ship.sprite, ANIMLOOP_STOP);
}

void Ship_PreRender(Ship &ship, int dt)
{
	Body *body = (ship.body);
	Sprite *sprite = (ship.sprite);

	Sprite_SetToBody(sprite, body);
	if (ship.voice != NULL)
	{
		Voice *voice = (ship.voice);
		Voice_SetToBody(voice, body);
	}

	Sprite_SetAngle(sprite, Ship_MeleeStunned(ship) ?
			30.0 * ship.meleehitstunnedtime : 0);
}

