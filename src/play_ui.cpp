#include "play_internal.hpp"
#include "ui.hpp"
#include "text.hpp"
#include "stats.hpp"
#include <cstdio>
#include <vector>

static UIWidget *playui_portrait;
static UIWidget *playui_score;
static UIWidget *playui_scoretoextend;
static std::vector<UIWidget*> playui_lives;

void PlayUI_Init()
{
	playui_portrait = UIWidget_Get("portrait");
	playui_score = UIWidget_Get("score");
	playui_scoretoextend = UIWidget_Get("extscore");

	playui_lives.clear();
	char lifename[8];
	for (int i = 0; i < STATS_LIVES_MAX; ++i)
	{
		std::sprintf(lifename, "life%d", i);
		playui_lives.push_back(UIWidget_Get(lifename));
	}

	PlayUI_UpdateScore();
	PlayUI_UpdateLives();
}

void PlayUI_UpdateScore()
{
	char scorestr[16];

	if (playui_score)
	{
		Text *text = UIWidget_Text(playui_score);

		if (text)
		{
			std::sprintf(scorestr, "%8d", Stats_Score());
			Text_SetString(text, scorestr);
		}
	}

	if (playui_scoretoextend)
	{
		Text *text = UIWidget_Text(playui_scoretoextend);

		if (text)
		{
			std::sprintf(scorestr, "%8d", Stats_ScoreToExtend());
			Text_SetString(text, scorestr);
		}
	}
}

void PlayUI_UpdateLives()
{
	for (size_t i = 0; i < playui_lives.size(); ++i)
	{
		if (playui_lives[i])
			UIWidget_SetVisible(playui_lives[i], i < Stats_Lives());
	}
}

