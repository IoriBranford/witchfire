#include "ship.hpp"
#include "tile.hpp"
#include "data.hpp"
#include "database.hpp"
#include "units.hpp"

#include <vector>
#include <algorithm>
#include <functional>
#include <cstring>

//enum GroupSpawnCondition
//{
//	PREV_GROUP_CLEAR,
//	STAGE_POS
//};

struct ShipSpawn
{
	unsigned group;
	char type[32];
	SDL_Point pos;
	char script[32];
	unsigned postdelay;

	ShipSpawn(unsigned g = 0,
			const char *t = NULL,
			const SDL_Point *p = NULL,
			const char *scr = NULL,
			unsigned pd = 0)
	{
		group = g;

		if (t)
			std::strcpy(type, t);
		else
			std::memset(type, 0, sizeof(type));

		if (p)
		{
			pos.x = p->x;
			pos.y = p->y;
		}
		else
			pos.x = pos.y = 0;

		if (scr)
			std::strcpy(script, scr);
		else
			std::memset(script, 0, sizeof(script));

		postdelay = pd;
	}

	bool operator<(const ShipSpawn &ss) const
	{
		return pos.x < ss.pos.x;
	}

	bool operator>(const ShipSpawn &ss) const
	{
		return pos.x > ss.pos.x;
	}
};

static std::vector<ShipSpawn> ship_seq;
static unsigned ship_seq_currentgroup;
static unsigned ship_seq_delay;

void ShipSeq_Init(unsigned length)
{
	ship_seq.clear();
	ship_seq.reserve(length);
	ship_seq_currentgroup = 0;
	ship_seq_delay = 0;
}

void ShipSeq_AddSpawn(unsigned group,
		const char *type,
		const SDL_Point *pos,
		const char *script,
		unsigned postdelay)
{
	ship_seq.push_back(ShipSpawn(group, type, pos, script, postdelay));
	std::push_heap(ship_seq.begin(), ship_seq.end(),
			std::greater<ShipSpawn>());
}

void ShipSeq_IncGroup()
{
	++ship_seq_currentgroup;
}

bool ShipSeq_Finished()
{
	return ship_seq.empty();
}

bool ShipSeq_Waiting()
{
	return !ShipSeq_Finished() &&
		ship_seq[0].group <= ship_seq_currentgroup;
}

void ShipSeq_Tick()
{
	if (ship_seq_delay)
		--ship_seq_delay;

	while (!ship_seq_delay && !ShipSeq_Finished())
	{
		ShipSpawn &shipspawn = ship_seq[0];
		if (ship_seq_currentgroup < shipspawn.group)
			break;

		SDL_Point pos =
		{
			(shipspawn.pos.x),
			(shipspawn.pos.y)
		};

		SDL_Rect spawnrect =
		{
			pos.x,
			pos.y,
			1,
			1
		};

		Data ps = Database_Get("objects", shipspawn.type);
		if (ps)
		{
			Data p;
			if ((p = ps.Find("volume.x")))
				spawnrect.x += p.Int();
			if ((p = ps.Find("volume.y")))
				spawnrect.y += p.Int();
			if ((p = ps.Find("volume.w")))
				spawnrect.w = p.Int();
			if ((p = ps.Find("volume.h")))
				spawnrect.h = p.Int();
		}

		if (!TileMap_RectInView(&spawnrect))
			break;

		TileMap_ToScreenPos(&pos);
		Ship_New(shipspawn.type, &pos, shipspawn.script);
		ship_seq_delay = shipspawn.postdelay;
		std::pop_heap(ship_seq.begin(), ship_seq.end(),
			std::greater<ShipSpawn>());
		ship_seq.pop_back();
		//TEST
		//if (ShipSeq_Finished())
		//	TileMap_DeactivateLoop();
	}
}

#include "units.hpp"

void ShipSeqTest_Init()
{
	static const char *ENEMY_NAMES[] =
	{
		"bunny",
		"bunny",
		"tanpopo",
		"bunny",
		"bunny"
	};

	static const char *ENEMY_SCRIPTS[] =
	{
		"enemy",
		"enemy",
		"enemy",
		"enemy",
		"enemy"
	};

	unsigned int n = sizeof(ENEMY_NAMES) / sizeof(ENEMY_NAMES[0]);

	for (unsigned int i = 0; i < 100; ++i)
	{
		SDL_Point shippos =
		{
			PixelToMicron(SCREEN_RIGHT_PX),
			PixelToMicron(SCREEN_BOTTOM_PX) / 6
		};

		for (unsigned int j = 0; j < n; ++j)
		{
			ShipSeq_AddSpawn(i, ENEMY_NAMES[j], &shippos,
					ENEMY_SCRIPTS[j], 6);
			shippos.y += PixelToMicron(SCREEN_BOTTOM_PX) / 6;
		}
	}
}
