#include "ship.hpp"
#include "ship_internal.hpp"
#include "body.hpp"
#include "sprite.hpp"
#include "voice.hpp"
#include "sound.hpp"
#include "database.hpp"
#include "units.hpp"
#include <cmath>

static void Ship_MeleeAttack_OnHit(void *meleeattacker, const Hit &hit)
{
	
}

void Ship_MeleeAttack_End(Ship &ship)
{
	if (ship.meleebody)
		Body_Del(ship.meleebody);
	ship.meleebody = NULL;
	if (ship.meleesprite)
		Sprite_Del(ship.meleesprite);
	ship.meleesprite = NULL;
	ship.meleeattackpushdist = 0;
	ship.meleeattacktime = 0;
	ship.meleeattackstuntime = 0;
}

bool Ship_MeleeAttacking(Ship &ship)
{
	return ship.meleeattacktime > 0;
}

bool Ship_MeleeStunned(Ship &ship)
{
	return ship.meleehitstunnedtime > 0;
}

void Ship_MeleeAttack(Ship &ship, int pushdist, int time, int stuntime)
{
	if (ship.meleebody)
		Ship_MeleeAttack_End(ship);

	ship.meleebody = Body_New(VOLUME_BOX, Body_Pos_Micron(ship.body), 1,
			1<<HITGROUP_PMELEE,
			1<<HITGROUP_ESHIP,// | 1<<HITGROUP_EBULLET,
			&ship, Ship_MeleeAttack_OnHit);

	SDL_Rect volume = { 0, 0, 0, 0 };

	Data ps = Database_Get("objects", ship.type);
	if (ps)
	{
		Data p;
		if((p = ps.Find("meleevolume.x")))
			volume.x = PixelToMicron(p.Int());
		if((p = ps.Find("meleevolume.y")))
			volume.y = PixelToMicron(p.Int());
		if((p = ps.Find("meleevolume.w")))
			volume.w = PixelToMicron(p.Int());
		if((p = ps.Find("meleevolume.h")))
			volume.h = PixelToMicron(p.Int());
	}

	if (!(volume.x || volume.y || volume.w || volume.h))
		Body_CopyVolume(ship.meleebody, ship.body);
	else
		Body_SetVolumeBox(ship.meleebody, &volume);

	ship.meleeattackpushdist = pushdist;
	ship.meleeattacktime = time;
	ship.meleeattackstuntime = stuntime;
}

void Ship_MeleeAttack_Tick(Ship &ship)
{
	if (ship.meleebody)
	{
		Body_SetAccel(ship.meleebody, 0, 0);
		Body_AddAccelToFollow(ship.meleebody, ship.body);
	}

	if (ship.meleeattacktime)
	{
		--ship.meleeattacktime;
		if (!ship.meleeattacktime)
			Ship_MeleeAttack_End(ship);
	}

	if (ship.meleehitstunnedtime)
	{
		--ship.meleehitstunnedtime;
		if (!ship.meleehitstunnedtime)
		{
			ship.movebounds = NULL;
		}
	}
}

void Ship_MeleeAttack_Hit(Ship &target, Ship &attacker)
{
	if (Ship_MeleeStunned(target))
		return;

	double angle_rad = std::atan2(
			Body_Pos_Micron(target.body)->y
			- Body_Pos_Micron(attacker.body)->y,
			Body_Pos_Micron(target.body)->x
			- Body_Pos_Micron(attacker.body)->x);

	SDL_Point vel = 
	{
		(int)(std::cos(angle_rad) *
			attacker.meleeattackpushdist /
			attacker.meleeattackstuntime),
		(int)(std::sin(angle_rad) *
			attacker.meleeattackpushdist /
			attacker.meleeattackstuntime)
	};
	Body_SetVel(target.body, &vel);
	Body_SetAccel(target.body, 0, 0);

	Ship_MeleeAttack_End(target);
	target.meleehitstunnedtime = attacker.meleeattackstuntime;
	Voice_Play(target.voice, Sound_Get("meleehit"), 0);

	target.movebounds = &SCREEN_BOUNDS_MI;
}

