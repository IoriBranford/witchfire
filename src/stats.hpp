#ifndef __STATS_H__
#define __STATS_H__

void Stats_Init();
void Stats_Reset();

unsigned int Stats_Score();
unsigned int Stats_ScoreToExtend();
unsigned int Stats_Lives();
unsigned int Stats_Stage();

static const unsigned int STATS_LIVES_MAX = 9;

unsigned int Stats_IncScore(unsigned int points);
bool Stats_WillExtend(unsigned int points);
void Stats_Extend();
unsigned int Stats_DecLives();
unsigned int Stats_IncStage();

#endif
