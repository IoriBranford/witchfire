#include "SDL.h"
#include "SDL_opengl.h"

#include "cel.hpp"

struct Vertex
{
	float xyz[3];
	float uv[2];
};

void Render3DTest_LoadAssets()
{
	Cel_Load("stone");
	//SDL_SetTextureBlendMode(Cel_Get("stone"), SDL_BLENDMODE_BLEND);
}

void Render3DTest_Draw()
{
	float texw = 1, texh = 1;
	SDL_GL_BindTexture(Cel_Texture(Cel_Get("stone")), &texw, &texh);

	float floorsize = 4;
	glEnable(GL_FOG);
	//glFogi(GL_FOG_COORD_SRC, GL_FRAGMENT_DEPTH);
	//glFogi(GL_FOG_MODE, GL_LINEAR);
	glFogi(GL_FOG_START, 1);
	glFogi(GL_FOG_END, 2*floorsize);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glFrustum(-1, 1, .75, -.75, 1, 1 + (floorsize*2));
	//SDL_Log("GL error %x", glGetError());

	static const Vertex verts[] =
	{
		{{-floorsize, .75f, -1.f-(floorsize*2)},{0, 0      }},
		{{ floorsize, .75f, -1.f-(floorsize*2)},{texw, 0   }},
		{{-floorsize, .75f, -1.f             } ,{0, texh   }},
		{{ floorsize, .75f, -1.f             } ,{texw, texh}}
	};
//	const GLfloat uv[] =
//	{
//		0, 0,
//		texw, 0,
//		0, texh,
//		texw, texh,
//	};
//	const GLubyte rgba[] =
//	{
//		0x00, 0x00, 0x00, 0xFF,
//		0x00, 0x00, 0x00, 0xFF,
//		0x80, 0x80, 0x80, 0xFF,
//		0x80, 0x80, 0x80, 0xFF,
//	};
	const GLushort idx[] =
	{
		0,1,2,1,2,3
	};

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	//glEnableClientState(GL_COLOR_ARRAY);

	glVertexPointer(3, GL_FLOAT, sizeof(Vertex), &verts[0].xyz);
	glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), &verts[0].uv);
	//glColorPointer(4, GL_UNSIGNED_BYTE, 0, rgba);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, idx);
	//xyz[1] = -xyz[1];
	//xyz[4] = -xyz[4];
	//xyz[7] = -xyz[7];
	//xyz[10] = -xyz[10];
	//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, idx);

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	//glDisableClientState(GL_COLOR_ARRAY);

	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glDisable(GL_FOG);
}
