#ifndef __TIME_H__
#define __TIME_H__

// Time (in ticks) spent so far in current app state

void Time_ResetTicks();
unsigned Time_GetTicks(unsigned fromnow = 0);
void Time_Tick(unsigned ticks);

#endif
