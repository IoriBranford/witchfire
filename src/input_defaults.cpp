#include "input.hpp"

#include "SDL.h"
#include "ship.hpp"

void Input_ResetDefaultEventCommands()
{
	SDL_Keycode keycodes[] =
	{
		SDLK_LEFT,
		SDLK_RIGHT,
		SDLK_UP,
		SDLK_DOWN,
		SDLK_z,
		SDLK_x
	};

	int jaxes[] =
	{
		0,
		1
	};

	int caxes[] =
	{
		SDL_CONTROLLER_AXIS_LEFTX,
		SDL_CONTROLLER_AXIS_LEFTY
	};

	int jbuttons[] =
	{
		0,
		1
	};

	int cbuttons[] =
	{
		SDL_CONTROLLER_BUTTON_DPAD_LEFT,
		SDL_CONTROLLER_BUTTON_DPAD_RIGHT,
		SDL_CONTROLLER_BUTTON_DPAD_UP,
		SDL_CONTROLLER_BUTTON_DPAD_DOWN,
		SDL_CONTROLLER_BUTTON_A,
		SDL_CONTROLLER_BUTTON_B
	};

	int mbuttons[] =
	{
		SDL_BUTTON_LEFT,
		SDL_BUTTON_RIGHT
	};

	int commands[] =
	{
		DIGITAL_LEFT,
		DIGITAL_RIGHT,
		DIGITAL_UP,
		DIGITAL_DOWN,
		DIGITAL_FIRE_LEFT,
		DIGITAL_FIRE_RIGHT
	};

	int analogcommands[] =
	{
		ANALOG_X,
		ANALOG_Y
	};

	Input_ClearEventCommands();

	Input_SetEventCommands(SDL_KEYDOWN, 0,
			6, keycodes, commands);

	Input_SetEventCommands(SDL_JOYAXISMOTION, 0,
			2, jaxes, analogcommands);
#ifndef _WIN32 //TODO something about ghost hat events in Win
	int hatvalues[] =
	{
		SDL_HAT_LEFT,
		SDL_HAT_RIGHT,
		SDL_HAT_UP,
		SDL_HAT_DOWN
	};

	Input_SetEventCommands(SDL_JOYHATMOTION, 0,
			4, hatvalues, commands);
#endif
	Input_SetEventCommands(SDL_JOYBUTTONDOWN, 0,
			2, jbuttons, &commands[DIGITAL_FIRE_LEFT]);

	Input_SetEventCommands(SDL_CONTROLLERAXISMOTION, 0,
			2, caxes, analogcommands);
	Input_SetEventCommands(SDL_CONTROLLERBUTTONDOWN, 0,
			6, cbuttons, commands);

	Input_SetEventCommand(SDL_MOUSEMOTION, 0, 0, 0);
	Input_SetEventCommands(SDL_MOUSEBUTTONDOWN, 0,
			2, mbuttons, &commands[DIGITAL_FIRE_LEFT]);
}
