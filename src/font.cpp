#include "data.hpp"
#include "disk.hpp"
#include "app.hpp"
#include "render.hpp"
#include "texture.hpp"

#include "utf8.h"
#include "SDL.h"
#include "SDL_image.h"

#include <map>
#include <sstream>
#include <cstdio>

static const char *FONT_DIR = "fnt/";
static const char *FONT_EXT = ".fnt";

struct FontChar
{
	float texrectx, texrecty;
	float texrectw, texrecth;
	char offsetx, offsety;
	char advancex;
	char page;

	FontChar(float tx = 0, float ty = 0, float tw = 0, float th = 0,
			char ox = 0, char oy = 0, char ax = 0, char p = 0)
	{
		texrectx = tx; texrecty = ty; texrectw = tw; texrecth = th;
		offsetx = (ox); offsety = (oy); advancex = (ax); page = (p);
	}
};

struct FontPage
{
	unsigned int texture;
	short width, height;

	FontPage(unsigned int t = 0, short w = 0, short h = 0)
		: texture(t), width(w), height(h)
	{
	};
};

struct Font
{
	int baseline;
	int lineheight;
	std::map<unsigned, FontChar> chars;	// codepoint to fontchar
	std::map<int, FontPage> pages;	// page# to Texture

	Font()
		: baseline(0), lineheight(0), chars(), pages()
	{
	}
};

static std::map<std::string, Font> allfonts;

static void Font_ReadChars(Font &font, Data charsdata)
{
	for (Data chardata = charsdata.Begin();
			chardata != charsdata.End();
			++chardata)
	{
		if (!chardata.Named("char"))
			continue;

		int pagen = chardata.Find("page").Int();
		const FontPage &page = font.pages[pagen];
		float pagew = page.width;
		float pageh = page.height;
		unsigned utf8int = UCharToUTF8int(chardata.Find("id").Int());

		float x = chardata.Find("x").Float()/pagew;
		float y = chardata.Find("y").Float()/pageh;

		font.chars[utf8int] = FontChar(
				x,
				y,
				(chardata.Find("width").Float()/pagew),
				(chardata.Find("height").Float()/pageh),
				(char)chardata.Find("xoffset").Int(),
				(char)chardata.Find("yoffset").Int(),
				(char)chardata.Find("xadvance").Int(),
				(char)pagen
				);
	}
}

static void Font_LoadPages(const char *fontname, Font & font, Data pagesdata)
{
	for (Data pagedata = pagesdata.Begin();
			pagedata != pagesdata.End();
			++pagedata)
	{
		std::string path (FONT_DIR);
		path += (fontname);
		size_t n = path.rfind('/');
		if (n != std::string::npos)
		{
			path.erase(n);
			path += '/';
		}
		else
		{
			path.clear();
		}
		path += pagedata.Find("file").CStr();

		int width, height;
		unsigned int texture = Texture_FOpen(path.c_str(),
				&width, &height);

		if (texture)
		{
			font.pages[pagedata.Find("id").Int()] = 
				FontPage(texture, width, height);
		}
		else
		{
			SDL_LogError(SDL_LOG_CATEGORY_RENDER, SDL_GetError());
		}
	}
}

bool Font_Load(const char *fontname)
{
	std::map<std::string, Font>::iterator it = allfonts.find(fontname);
	if (it != allfonts.end())
		return (!it->second.pages.empty());

	std::string path (FONT_DIR);
	path += fontname;
	path += FONT_EXT;

	Data data;
	bool success = data.FOpenXML(path.c_str());

	if (success)
	{
		Data fontdata = data.Find("font");
		//fontdata.Save(fontname);

		allfonts[fontname] = Font();
		Font &font = allfonts[fontname];

		Data commondata = fontdata.Find("common");
		font.baseline = commondata.Find("base").Int();
		font.lineheight = commondata.Find("lineHeight").Int();

		Font_LoadPages(fontname, font, fontdata.Find("pages"));
		Font_ReadChars(font, fontdata.Find("chars"));
	}

	data.Close();
	return success;
}

Font* Font_Get(const char *name)
{
	std::map<std::string, Font>::iterator it;
	std::string key (name);
	it = allfonts.find(key);
	if (it != allfonts.end())
		return &it->second;
	return NULL;
}

void Font_DrawString(Font *font, SDL_Rect *textrect, const char *u8text)
{
	struct { float x, y; } pos = {textrect->x, textrect->y};

	int pagei = -1;
	FontPage *page = NULL;

	while (*u8text)
	{
		unsigned utf8int = 0;
		u8text += UTF8seqToUTF8int((const unsigned char*)u8text,
				&utf8int);

		std::map<unsigned, FontChar>::iterator utf8intfontchar
			= font->chars.find(utf8int);
		if (utf8intfontchar == font->chars.end())
		{
			continue;
		}

		const FontChar &fontchar = utf8intfontchar->second;
		if (pagei != fontchar.page)
		{
			std::map<int, FontPage>::iterator pageit
				= font->pages.find(fontchar.page);
			if (pageit == font->pages.end())
			{
				continue;
			}

			if (page)
				Render_EndDrawing();

			page = &pageit->second;
			pagei = fontchar.page;

			Render_SetDrawingTransform(0, 0, 0, 0);
			Render_SetDrawingTexture(page->texture);
			Render_BeginDrawing();
		}

		if (page && fontchar.texrectw && fontchar.texrecth)
		{
			float left = pos.x + fontchar.offsetx;
			float top = pos.y + fontchar.offsety;

			Render_DrawQuad(left,
					top,
					left + (fontchar.texrectw*page->width),
					top + (fontchar.texrecth*page->height),
					fontchar.texrectx,
					fontchar.texrecty,
					fontchar.texrectx + fontchar.texrectw,
					fontchar.texrecty + fontchar.texrecth);

			//dstrect.x = pos.x;
			//dstrect.y = pos.y;
			//dstrect.w = fontchar.advancex;
			//dstrect.h = font->lineheight;
			//SDL_RenderDrawRect(renderer, &dstrect);
		}

		pos.x += fontchar.advancex;
	}

	if (page)
		Render_EndDrawing();
}

void AllFonts_Unload()
{
	allfonts.clear();
}
