#include "app.hpp"
#include "app_internal.hpp"

static AppPhase app_phase;

bool AppPhase_Valid()
{
	return app_phase.tickfunc || app_phase.renderfunc;
}

void AppPhase_Change(const AppPhase &nextphase)
{
	app_phase = nextphase;
}
void AppPhase_LoadAssets()
{
	if (app_phase.loadassetsfunc)
		app_phase.loadassetsfunc();
}

void AppPhase_Init()
{
	if (app_phase.initfunc)
		app_phase.initfunc();
}

void AppPhase_Tick()
{
	if (app_phase.tickfunc)
		app_phase.tickfunc();
}

void AppPhase_PreRender(int dt)
{
	if (app_phase.renderfunc)
		app_phase.renderfunc(dt);
}

