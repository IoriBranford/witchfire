#ifndef __SOUND_H__
#define __SOUND_H__

struct Mix_Chunk;

bool Sound_Load(const char *name);
int Sound_LoadBatch(int ct, const char **names, const char *prefix);
Mix_Chunk* Sound_Get(const char *name);
void Sound_Play(Mix_Chunk *sound);
void AllSounds_Unload();

#endif
