#include "body_internal.hpp"

#include <cstring>

Volume::Volume(VolumeType t)
{
	std::memset(this, 0, sizeof(Volume));
	type = t;
}

void Body_SetVolumeBox(Body *body, const SDL_Rect *rect_mi)
{
	body->vol_mi = Volume(VOLUME_BOX);
	body->vol_mi.box.rect = *rect_mi;
}

void Body_SetVolumeBall(Body *body, int radius_mi, const SDL_Point *center_mi)
{
	body->vol_mi = Volume(VOLUME_BALL);
	body->vol_mi.ball.radius = radius_mi;
	if (center_mi)
		body->vol_mi.ball.center = *center_mi;
	else
		body->vol_mi.ball.center.x = body->vol_mi.ball.center.y = 0;
}

void Body_SetVolumeRay(Body *body, const SDL_Point *vec_mi,
		const SDL_Point *origin_mi, int radius_mi)
{
	body->vol_mi = Volume(VOLUME_RAY);
	body->vol_mi.ray.vec = *vec_mi;
	body->vol_mi.ray.radius = radius_mi;
	if (origin_mi)
		body->vol_mi.ray.origin = *origin_mi;
}

void Body_CopyVolume(Body *dstbody, const Body *srcbody)
{
	dstbody->vol_mi = srcbody->vol_mi;
}

