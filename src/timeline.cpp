#include "timeline.hpp"
#include "time.hpp"

#include "alist.hpp"

struct TimelineEvent
{
	int id;
	TimelineFunc func;
	void *data;
	unsigned tick;

	TimelineEvent(TimelineFunc f = NULL, void *d = NULL, unsigned s = 0)
	{
		id = NULL;
		func = f;
		data = d;
		tick = s;
	}
};

typedef AList<TimelineEvent> TimelineEventList;
static TimelineEventList timeline_events;

void Timeline_Init(int maxevents)
{
	timeline_events.Init(maxevents);
}

int Timeline_AddEvent(TimelineFunc func,
		void *data,
		unsigned tickslater)
{
	int id = timeline_events.New();
	if (id != NULL)
	{
		timeline_events[id] = TimelineEvent(func, data,
				Time_GetTicks(tickslater));
		timeline_events[id].id = id;
	}
	return id;
}

void Timeline_CancelEvent(int id)
{
	timeline_events.Del(id);
}

void Timeline_Tick()
{
	for (TimelineEventList::Iterator it = timeline_events.Begin();
			it != timeline_events.End();
			)
	{
		TimelineEvent &event = *it;
		if (event.func && Time_GetTicks() >= event.tick)
		{
			event.func(event.data);
			timeline_events.Del(event.id);
		}
		else
		{
			++it;
		}
	}
}

void Timeline_Clear()
{
	timeline_events.Clear();
}
