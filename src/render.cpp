#include "render.hpp"

#include "units.hpp"

#include "SDL.h"
#include "SDL_opengl.h"

#include <cassert>

struct Vertex
{
	float x, y, z;
	float tx, ty;

	Vertex(float x = 0, float y = 0, float z = 0,
			float tx = 0, float ty = 0)
		: x(x), y(y), z(z), tx(tx), ty(ty)
	{
	}
};

struct Triangle
{
	Vertex verts[3];
};

static const size_t MAX_TRIANGLES = 4096;

static size_t render_trianglect;
static Triangle render_triangles[MAX_TRIANGLES];

bool Render_Init(SDL_Window *window, Uint32 rendererflags)
{
	if (!SDL_GL_CreateContext(window))
		return false;

	// left-hand system rotated 180deg around X axis

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(SCREEN_LEFT_PX, SCREEN_RIGHT_PX,
			SCREEN_BOTTOM_PX, SCREEN_TOP_PX,
			512, -512);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glDisable(GL_LIGHTING);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_DEPTH_TEST);

	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GEQUAL, 0.5);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY); 
	const GLsizei STRIDE = sizeof(Vertex);
	glVertexPointer(3, GL_FLOAT, STRIDE, &render_triangles->verts->x);
	glTexCoordPointer(2, GL_FLOAT, STRIDE, &render_triangles->verts->tx);

	if (rendererflags & SDL_RENDERER_PRESENTVSYNC)
	{
		if (SDL_GL_SetSwapInterval(-1) == -1)
			SDL_GL_SetSwapInterval(1);
	}
	else
	{
		SDL_GL_SetSwapInterval(0);
	}

	return true;
}

void Render_Clear()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Render_SetDrawingTransform(float x, float y, float z, float angle)
{
	glLoadIdentity();
	glTranslatef(x, y, z);
	glRotatef(angle, 0, 0, 1);
}

void Render_SetDrawingTexture(unsigned int texture)
{
	if (!texture)
	{
		glDisable(GL_TEXTURE_2D);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else
	{
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texture);
	}
}

void Render_BeginDrawing()
{
	render_trianglect = 0;
}

static void Render_DrawTriangle(const Triangle &triangle)
{
	if (render_trianglect >= MAX_TRIANGLES)
	{
		return;
	}

	render_triangles[render_trianglect++] = triangle;
}

void Render_DrawQuad(float x0, float y0, float x1, float y1,
		float tx0, float ty0, float tx1, float ty1,
		float z)
		//double angle_deg, const SDL_Point *center,
		//SDL_RendererFlip flip)
{
	if (render_trianglect + 1 >= MAX_TRIANGLES)
	{
		return;
	}

	Triangle &tl = render_triangles[render_trianglect];
	Triangle &br = render_triangles[render_trianglect + 1];
	render_trianglect += 2;

	tl.verts[0].x = tl.verts[1].x = br.verts[2].x = x0;
	tl.verts[0].y = tl.verts[2].y = br.verts[1].y = y0;
	br.verts[0].x = br.verts[1].x = tl.verts[2].x = x1;
	br.verts[0].y = br.verts[2].y = tl.verts[1].y = y1;

	tl.verts[0].tx = tl.verts[1].tx = br.verts[2].tx = tx0;
	tl.verts[0].ty = tl.verts[2].ty = br.verts[1].ty = ty0;
	br.verts[0].tx = br.verts[1].tx = tl.verts[2].tx = tx1;
	br.verts[0].ty = br.verts[2].ty = tl.verts[1].ty = ty1;

	tl.verts[0].z = tl.verts[1].z = tl.verts[2].z = z;
	br.verts[0].z = br.verts[2].z = br.verts[1].z = z;

	//Render_DrawTriangle(tl);
	//Render_DrawTriangle(br);
}

void Render_EndDrawing()
{
	glDrawArrays(GL_TRIANGLES, 0, render_trianglect * 3);
}

void Render_Present(SDL_Window *window)
{
	SDL_GL_SwapWindow(window);
}

