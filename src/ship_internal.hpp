#ifndef __SHIP_STRUCT_H__
#define __SHIP_STRUCT_H__

struct Body;
struct Sprite;
struct Voice;

struct SDL_Rect;
struct lua_State;

struct Ship
{
	char type[64];

	int health;
	int lifetime;

	int digitalflags;
	float analogvalues[ANALOG_COUNT];

	const SDL_Rect *movebounds;

	Body *body;
	Sprite *sprite;
	Voice *voice;

	char nextscript[64];
	lua_State *scriptthread;

	Body *meleebody;
	Sprite *meleesprite;
	int meleeattackpushdist;
	int meleeattacktime;
	int meleeattackstuntime;
	int meleehitstunnedtime;

	Body *shieldbody;
	Sprite *shieldsprite;

	Ship();
};

bool Ship_IsEnemyType(const char *shiptype);

void Ship_Init(Ship &ship, const char *type, const SDL_Point *pos,
		const char *script);
void Ship_Uninit(Ship &ship);
void Ship_Tick(Ship &ship);
bool Ship_IsDying(const Ship &ship);
bool Ship_IsDead(const Ship &ship);
void Ship_Exit(Ship &ship);
void Ship_PreRender(Ship &ship, int dt);

void ShipScript_RegisterLib();
void ShipScript_Tick(Ship &ship);

bool Ship_MeleeAttacking(Ship &ship);
bool Ship_MeleeStunned(Ship &ship);
void Ship_MeleeAttack_End(Ship &ship);
void Ship_MeleeAttack(Ship &ship, int pushback, int time, int stuntime);
void Ship_MeleeAttack_Tick(Ship &ship);
void Ship_MeleeAttack_Hit(Ship &target, Ship &attacker);

void Ship_RaiseShield(Ship &ship, int radius);
void Ship_LowerShield(Ship &ship);
void Ship_Shield_Tick(Ship &ship);

#endif
