#include "sound.hpp"

#include "disk.hpp"

#include <map>
#include <string>

#include "SDL.h"
#include "SDL_mixer.h"

static std::map<std::string, Mix_Chunk*> allsounds;

static const char *SOUND_DIR = "snd/";
static const char *SOUND_EXT = ".wav";

bool Sound_Load(const char *name)
{
	std::string key (name);
	std::map<std::string, Mix_Chunk*>::iterator it;

	it = allsounds.find(key);
	if (it != allsounds.end())
		return (it->second == NULL);

	std::string path (SOUND_DIR);
	path += name;
	path += SOUND_EXT;

	char *text;
	int length = Disk_CStrOpen(path.c_str(), &text);
	if (length <= 0)
		return false;

	SDL_RWops *rwops = SDL_RWFromMem(text, length);
	Mix_Chunk *sound = Mix_LoadWAV_RW(rwops, true);//path.c_str());

	Disk_CStrClose(text);

	if (!sound)
	{
		SDL_LogError(SDL_LOG_CATEGORY_ERROR, Mix_GetError());
	}

	allsounds[key] = sound;
	return true;
}

int Sound_LoadBatch(int ct, const char **names, const char *prefix)
{
	int got = 0;
	std::string name;
	int prefixlen = 0;
	if (prefix)
	{
		name += prefix;
		prefixlen = name.length();
	}

	for (int i = 0; i < ct; ++i)
	{
		if (names[i])
		{
			name += names[i];

			if (Sound_Load(name.c_str()))
				++got;

			if (prefixlen)
				name.erase(prefixlen, std::string::npos);
		}
	}

	return got;
}

Mix_Chunk* Sound_Get(const char *name)
{
	std::map<std::string, Mix_Chunk*>::iterator it;
	std::string key (name);
	it = allsounds.find(key);
	if (it != allsounds.end())
		return it->second;
	return NULL;
}

void Sound_Play(Mix_Chunk *sound)
{
	if (sound)
		Mix_PlayChannel(-1, sound, 0);
}

void AllSounds_Unload()
{
	std::map<std::string, Mix_Chunk*>::iterator it;
	for (it = allsounds.begin(); it != allsounds.end(); ++it)
	{
		Mix_FreeChunk(it->second);
	}
	allsounds.clear();
}
