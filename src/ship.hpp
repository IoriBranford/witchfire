#ifndef __SHIP_H__
#define __SHIP_H__

#include "SDL.h"

struct Ship;
// active combatant on the field

enum Ship_DigitalCommands
{
	DIGITAL_LEFT,
	DIGITAL_RIGHT,
	DIGITAL_UP,
	DIGITAL_DOWN,
	DIGITAL_FIRE_LEFT,
	DIGITAL_FIRE_RIGHT,
	DIGITAL_COUNT
};

enum Ship_AnalogCommands
{
	ANALOG_X,
	ANALOG_Y,
	ANALOG_COUNT
};

const SDL_Point* Ship_VecToOtherShip(Ship *ship0, Ship *ship1, int intime = 0);

void AllShips_Init(int n);
void AllShips_Tick();
void AllShips_PreRender(int dt);
Ship* Ship_New(const char *type, const SDL_Point *pos, const char *script);

bool AllShips_NoPlayer();
bool AllShips_PlayerDying();
bool AllShips_NoEnemy();
void AllShips_SpawnPlayer();
Ship* AllShips_Player();
void AllShips_DecEnemyCt();
int Ship_FindNearestOtherShip(Ship *ship, Ship **foundship = 0);

void ShipSeq_Init(unsigned length);
void ShipSeq_AddSpawn(unsigned group,
		const char *type,
		const SDL_Point *pos,
		const char *script,
		unsigned postdelay = 0);
void ShipSeq_IncGroup();
bool ShipSeq_Finished();
bool ShipSeq_Waiting();
void ShipSeq_Tick();

void ShipSeqTest_Init();

#endif
