#include "SDL.h"
#include "SDL_image.h"
#include "SDL_opengl.h"

#include "app.hpp"
#include "disk.hpp"

#include <cassert>

unsigned int Texture_CreateFromSurface(SDL_Surface *rawsurface)
{
	assert(rawsurface);

	SDL_Surface *surface = SDL_ConvertSurfaceFormat(rawsurface,
			SDL_PIXELFORMAT_ARGB8888, 0);

	unsigned int texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	unsigned int texintfmt, texfmt;
#ifdef GLES
	texintfmt = texfmt = GL_BGRA_EXT;
#else
	texintfmt = GL_RGBA8;//surface->format->BytesPerPixel;
	texfmt = GL_BGRA;
#endif

	glTexImage2D(GL_TEXTURE_2D, 0, texintfmt, surface->w, surface->h, 0,
			texfmt, GL_UNSIGNED_INT_8_8_8_8_REV, surface->pixels);

	SDL_FreeSurface(surface);
	return texture;
}

unsigned int Texture_FOpen(const char *path, int *outwidth, int *outheight)
{
	SDL_Surface *rawsurface = NULL;

	char *text = NULL;
	int length = Disk_CStrOpen(path, &text);
	if (length > 0)
	{
		SDL_RWops *rwops = SDL_RWFromMem(text, length);
		rawsurface = IMG_Load_RW(rwops, true);//path.c_str());
		Disk_CStrClose(text);
	}

	if (!rawsurface)
	{
		SDL_LogError(SDL_LOG_CATEGORY_ERROR, IMG_GetError());
		return 0;
	}

	if (outwidth)
		*outwidth = rawsurface->w;
	if (outheight)
		*outheight = rawsurface->h;

	unsigned int texture = Texture_CreateFromSurface(rawsurface);
	SDL_FreeSurface(rawsurface);

	return texture;
}

void Texture_Delete(unsigned int texture)
{
	glDeleteTextures(1, &texture);
}

