local Ship = Ship

local myship = Ship.MyShip()
local t = 0
local fireinterval = 30
local offsetrange = 0x1000

while true do
	--local cauldron = Ship.FindShip {name = "cauldron"}
	--if cauldron then
	--	local vel_x, vel_y = Ship.ShipVel {ship = cauldron}
	--	Ship.SetTargetVel {ship = myship, vel_x = vel_x, vel_y = vel_y}
	--end
	
	local vel_x, vel_y = Stage.Vel()
	Ship.SetTargetVel {ship = myship, vel_x = -vel_x, vel_y = -vel_y}
	
	if t % fireinterval == 0 then
		local angle = math.pi * 1.5 --math.atan2(firey, firex)
		local bullet = {
			ship = myship,
			type = "enemy_bullet",
			speed = 0x100,
			angle = angle,
			accel_y = 0x20,
			offset_x = math.random(-offsetrange, offsetrange)
		}
		Ship.FireBullet(bullet)
	end

	t = t + 1
	if not Ship.ShipOnScreen {ship = myship} then
		Ship.RemoveMyShip()
	end
	coroutine.yield()
end
