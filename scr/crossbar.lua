local Ship = Ship

local myship = Ship.MyShip()

while true do
	local vel_x, vel_y = Stage.Vel()
	Ship.SetTargetVel {ship = myship, vel_x = -vel_x, vel_y = -vel_y}
	
	if not Ship.ShipOnScreen {ship = myship} then
		Ship.RemoveMyShip()
	end
	coroutine.yield()
end
