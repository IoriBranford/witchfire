local Ship = Ship

local myship = Ship.MyShip()
local t = 0
local fireinterval = 60

while true do
	local firex, firey = -0x400, 0
	local dist = 1
	local playership = Ship.PlayerShip()

	if playership then
		firex, firey = Ship.VecToOtherShip {
			ship = myship,
			othership = playership
		}
		dist = math.sqrt(firex*firex + firey*firey)
		dist = dist * 32 / (dist - 0x4000)
	end

	Ship.SetTargetVel {
		ship = myship,
		vel_x = firex/dist,
		vel_y = firey/dist
	}

	if playership and t % fireinterval == 0 then
		local angle = math.atan2(firey, firex)
		Ship.FireBullet {
			ship = myship,
			type = "enemy_bullet",
			speed = -0x600,
			angle = angle,
			accel_x = 0x80 * math.cos(angle),
			accel_y = 0x80 * math.sin(angle)
		}
	end

	t = t + 1
	coroutine.yield()
end
