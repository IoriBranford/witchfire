local Ship = Ship
local Stage = Stage

local myship = Ship.MyShip()
local t = 0
local fireinterval = 30

while true do
	local vel_x, vel_y = Stage.Vel()
	Ship.SetTargetVel {ship = myship, vel_x = -vel_x, vel_y = -vel_y}

	local playership = Ship.PlayerShip()
	if playership then
		firex, firey = Ship.VecToOtherShip {
			ship = myship,
			othership = playership
		}
		if t % fireinterval == 0 then
			local angle = math.atan2(firey, firex)
			local bullet = {
				ship = myship,
				type = "enemy_bullet",
				speed = 0x400,
				angle = angle,
				offset_x = 0x1000,
				offset_y = -0x800
			}
			Ship.FireBullet(bullet)

			bullet.offset_x = -bullet.offset_x
			Ship.FireBullet(bullet)
		end
	end

	t = t + 1
	if not Ship.ShipOnScreen {ship = myship} then
		Ship.RemoveMyShip()
	end
	coroutine.yield()
end
