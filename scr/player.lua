local Ship = Ship

local myship = Ship.PlayerShip()
local firearc = math.pi / 16
local firetimer = 0
local fireinterval = 4

local bullet = {
	type = "kagari_bullet",
	speed = 0x2000,
	sound = "player_shot"
}

local function Fire(angle)
	if firetimer % fireinterval ~= 0 then
		firetimer = firetimer + 1
		return
	end

	local nearestenemy, dist = Ship.FindNearestOtherShip { ship = myship }
	if dist < 0x2800 then
		Ship.MeleeAttack {
			ship = myship,
			pushdist = 0x6000,
			time = 9,
			stuntime = 12
		}
		firetimer = 0;
		return
	end

	arc = firearc * math.sin(firetimer * math.pi / 30)

	bullet.ship = myship
	bullet.frame = firetimer / fireinterval	-- give sprays more dynamic look
	
	bullet.angle = angle - arc
	Ship.FireBullet(bullet)
	bullet.angle = angle
	Ship.FireBullet(bullet)
	bullet.angle = angle + arc
	Ship.FireBullet(bullet)

	firetimer = firetimer + 1
end

while true do
	vel_x, vel_y, fireleft, fireright = Game.PlayerInput()

	if fireleft and fireright then
		vel_x = vel_x / 2
		vel_y = vel_y / 2
		firetimer = 0
		Ship.SetShield {ship = myship, radius = 0x2000}
	elseif fireleft then
		Fire(math.pi)
		Ship.SetShield {ship = myship, radius = 0}
	elseif fireright then
		Fire(0)
		Ship.SetShield {ship = myship, radius = 0}
	else
		firetimer = 0
		Ship.SetShield {ship = myship, radius = 0}
	end

	Ship.SetTargetVel {
		ship = myship,
		vel_x = vel_x,
		vel_y = vel_y
	}

	coroutine.yield()
end
