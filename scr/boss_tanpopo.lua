local Ship = Ship

local ENTER_SPEED = -0x100
local EXIT_SPEED = 0x400
local FIRE_INTERVAL = 15
local FIRE_SPREAD = math.pi / 6

local myship = Ship.MyShip()

Ship.SetTargetVel {
	ship = myship,
	vel_x = ENTER_SPEED,
	vel_y = 0
}

local t = 0
repeat
	t = t + 1
	coroutine.yield()
until t >= 120

t = 0
repeat
	local firex, firey = EXIT_SPEED, 0
	local dist = 1
	local playership = Ship.PlayerShip()

	if playership then
		firex, firey = Ship.VecToOtherShip {
			ship = myship,
			othership = playership
		}
		dist = math.sqrt(firex*firex + firey*firey)
		dist = dist * 32 / (dist - 0x8000)
	end

	Ship.SetTargetVel {
		ship = myship,
		vel_x = firex/dist,
		vel_y = firey/dist
	}

	if playership and t % FIRE_INTERVAL == 0 then
		local angle = math.atan2(firey, firex)
		for i = -1, 1, 1 do
			Ship.FireBullet {
				ship = myship,
				type = "enemy_bullet",
				speed = 0x400,
				angle = angle + (i * FIRE_SPREAD)
			}
		end
	end

	t = t + 1
	coroutine.yield()
until not Ship.ShipAlive {ship = myship}

if not Ship.ShipAlive {ship = myship} then
	for t = 1, 180, 1 do
		coroutine.yield()
	end

	Ship.PlaySound {ship = myship, sound = "explode_xl_end"}
end
