-- Get data from engine with accessor funcs
local self = Self()

-- Do sequence of actions via action funcs
-- Move(self,x,y), Attack(self,angle), etc

-- Suspend script until next tick with coroutine.yield()
-- Use it to wait for a condition e.g.:
--	while not condition do
--		coroutine.yield()
--	end
