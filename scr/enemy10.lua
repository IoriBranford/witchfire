local Ship = Ship

local ENTER_SPEED = -0x200
local EXIT_ACCEL = 0x80
local EXIT_SPEED = 0x400
local FIRE_INTERVAL = 15

local myship = Ship.MyShip()
local playership = nil

local myvel_x = ENTER_SPEED

Ship.SetTargetVel {
	ship = myship,
	vel_x = myvel_x,
	vel_y = 0
}

local t = 0
repeat
	t = t + 1
	coroutine.yield()
until t >= 60

t = 0
repeat
	Ship.SetTargetVel{
		ship = myship,
		vel_x = myvel_x,
		vel_y = 0
	}

	myvel_x = math.min(myvel_x + EXIT_ACCEL, EXIT_SPEED)

	playership = Ship.PlayerShip()

	if t % FIRE_INTERVAL == 0 and playership ~= nil then
		local firex, firey = Ship.VecToOtherShip {
			ship = myship,
			othership = playership
		}

		Ship.FireBullet {
			ship = myship,
			type = "enemy_bullet",
			speed = 0x600,
			angle = math.atan2(firey, firex)
		}
	end

	t = t + 1
	coroutine.yield()
until not Ship.ShipOnScreen {ship = myship}

Ship.RemoveMyShip()
