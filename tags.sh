#!/bin/sh
ctags -R src
ctags -Ra --c-kinds=+p include
ctags -Ra --c-kinds=+p ext
ctags -Ra --c-kinds=+p /usr/include/SDL2 /usr/include/gme /usr/include/physfs.h /usr/include/luajit-2.0
