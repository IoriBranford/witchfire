cmake_minimum_required(VERSION 2.8)

project(witchfire)

set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)

# PLATFORM

execute_process(COMMAND gcc -dumpmachine
	OUTPUT_VARIABLE COMPILER_TARGET_MACHINE
	OUTPUT_STRIP_TRAILING_WHITESPACE)

if(COMPILER_TARGET_MACHINE MATCHES "x86_64")
	set(COMPILER_TARGET_BITS 64)
else()
	set(COMPILER_TARGET_BITS 32)
endif()

# DEPENDS

include(FindPkgConfig)
if (WIN32)
	set(ENV{PKG_CONFIG_PATH} ${CMAKE_SOURCE_DIR}/lib/pkgconfig;$ENV{PKG_CONFIG_PATH})

	pkg_search_module(GME ${PKG_REQUIRED} libgme)
	set(CMAKE_INCLUDE_PATH ${CMAKE_SOURCE_DIR}/include)
	find_path(LUA_INCLUDE_DIRS
		NAMES luajit.h
		PATH_SUFFIXES luajit-2.0)
	find_library(LUA_LIBRARIES lua51)
	if(LUA_INCLUDE_DIRS AND LUA_LIBRARIES)
		set(LUA_FOUND 1)
	endif(LUA_INCLUDE_DIRS AND LUA_LIBRARIES)
	#if(NOT LUA_FOUND)
	#	pkg_search_module(LUA ${PKG_REQUIRED} lua) #hope it's 5.1
	#endif(NOT LUA_FOUND)
	#if(NOT LUA_FOUND)
	#	find_package(Lua51 ${PKG_REQUIRED})
	#	if(LUA51_FOUND)
	#		set(LUA_FOUND 1)
	#	endif(LUA51_FOUND)
	#endif(NOT LUA_FOUND)

	# In Win-Builds, multiple problems with the supplied package. Uncomment when fixed or to use a different local installation
	#pkg_search_module(SDL2 ${PKG_REQUIRED} sdl2)
else(WIN32)
	set(PKG_REQUIRED REQUIRED)
	set(GME_LIBRARIES gme) # linux: libgme.pc not standard, but libgme.so tends to be in /usr/lib
	#pkg_search_module(LUA ${PKG_REQUIRED} lua5.2)
	pkg_search_module(LUA ${PKG_REQUIRED} luajit)
	pkg_search_module(SDL2 ${PKG_REQUIRED} sdl2)
endif(WIN32)
pkg_search_module(SDL2_IMAGE ${PKG_REQUIRED} SDL2_image)
pkg_search_module(SDL2_MIXER ${PKG_REQUIRED} SDL2_mixer)
find_package(OpenGL ${PKG_REQUIRED})#set(OPENGL_LIBRARY EGL GLESv2)
find_package(PhysFS ${PKG_REQUIRED})
if(WIN32)
	include(win_ext)
endif(WIN32)
 
# BUILD

include(build)

# DEPLOY

if(WIN32)
	include(win_dll)
endif(WIN32)

set(DATATYPES gfx snd mus scr stg fnt ui db)
find_package(Git)
if(GIT_FOUND)
	foreach(DATATYPE ${DATATYPES})
		execute_process(
			COMMAND git ls-tree -r --name-only HEAD ${DATATYPE}
			WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
			OUTPUT_VARIABLE DATAFILES)
		separate_arguments(DATAFILES UNIX_COMMAND ${DATAFILES})
		install(FILES ${DATAFILES}
			DESTINATION ${DATATYPE}
			COMPONENT Data)
	endforeach(DATATYPE)
else(GIT_FOUND)
	install(DIRECTORY ${DATATYPES}
		DESTINATION .
		COMPONENT Data)
endif(GIT_FOUND)

set(CPACK_PACKAGE_FILE_NAME ${CMAKE_PROJECT_NAME}-${CMAKE_SYSTEM_NAME}${COMPILER_TARGET_BITS})
set(CPACK_PACKAGE_INSTALL_DIRECTORY ${CMAKE_PROJECT_NAME})

set(CPACK_COMPONENTS_ALL Application LibRuntime Data LibHeader Library)
set(CPACK_COMPONENT_APPLICATION_GROUP	"Runtime")
set(CPACK_COMPONENT_LIBRUNTIME_GROUP	"Runtime")
set(CPACK_COMPONENT_DATA_GROUP		"Data")
set(CPACK_COMPONENT_LIBHEADER_GROUP	"Development")
set(CPACK_COMPONENT_LIBRARY_GROUP	"Development")

set(CPACK_ALL_INSTALL_TYPES Player Programmer)
set(CPACK_COMPONENT_APPLICATION_INSTALL_TYPES	Player)
set(CPACK_COMPONENT_LIBRUNTIME_INSTALL_TYPES	Player Programmer)
set(CPACK_COMPONENT_DATA_INSTALL_TYPES		Player Programmer)
set(CPACK_COMPONENT_LIBHEADER_INSTALL_TYPES	Programmer)
set(CPACK_COMPONENT_LIBRARY_INSTALL_TYPES	Programmer)

include(CPack)
