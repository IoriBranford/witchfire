Welcome to project WitchFireWorks, a 2D shooting game in development.

[Downloads][downloads] will feature public test builds. (Other files are currently for developer use only.) Test builds are named `witchfire-<revision>-<platform>.zip`

[Wiki][wiki] will contain all project documentation.

[downloads]:https://bitbucket.org/IoriBranford/witchfire/downloads
[wiki]:https://bitbucket.org/IoriBranford/witchfire/wiki