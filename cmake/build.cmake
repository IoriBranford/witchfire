set(GAME_EXE ${CMAKE_PROJECT_NAME})

set(MY_FLAGS_WARNING
	"-Wall"
	)

set(CMAKE_C_FLAGS
	"${MY_FLAGS_WARNING} ${SDL2_CFLAGS_OTHER} ${SDL2_IMAGE_CFLAGS_OTHER} ${SDL2_MIXER_CFLAGS_OTHER} ${GME_CFLAGS_OTHER} ${LUA_CFLAGS_OTHER}"
	)

set(CMAKE_CXX_FLAGS
	"${MY_FLAGS_WARNING} ${SDL2_CFLAGS_OTHER} ${SDL2_IMAGE_CFLAGS_OTHER} ${SDL2_MIXER_CFLAGS_OTHER} ${GME_CFLAGS_OTHER} ${LUA_CFLAGS_OTHER}"
	)

set(MY_FLAGS_DEBUG
	""
	)

set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} ${MY_FLAGS_DEBUG}")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} ${MY_FLAGS_DEBUG}")

set(CMAKE_EXE_LINKER_FLAGS
	"${SDL2_LDFLAGS_OTHER} ${SDL2_IMAGE_LDFLAGS_OTHER} ${SDL2_MIXER_LDFLAGS_OTHER} ${GME_LDFLAGS_OTHER} ${LUA_LDFLAGS_OTHER}"
	)

if(WIN32)
	set(CMAKE_EXE_LINKER_FLAGS
		"${CMAKE_EXE_LINKER_FLAGS} -static-libgcc -static-libstdc++"
		)
	if(COMPILER_BITS LESS 64)
		set(CMAKE_EXE_LINKER_FLAGS
			"${CMAKE_EXE_LINKER_FLAGS} -Wl,--large-address-aware"
			)
	endif(COMPILER_BITS LESS 64)
endif(WIN32)

include_directories(
	src
	${SDL2_INCLUDE_DIRS}
	${SDL2_IMAGE_INCLUDE_DIRS}
	${SDL2_MIXER_INCLUDE_DIRS}
	${GME_INCLUDE_DIRS}
	${PHYSFS_INCLUDE_DIR}
	${LUA_INCLUDE_DIRS}
	)

link_directories(
	${SDL2_LIBRARY_DIRS}
	${SDL2_IMAGE_LIBRARY_DIRS}
	${SDL2_MIXER_LIBRARY_DIRS}
	${GME_LIBRARY_DIRS}
	${OPENGL_LIBRARY_DIR}
	${PHYSFS_LIBRARY_DIR}
	${LUA_LIBRARY_DIRS}
	)

add_executable(${GAME_EXE}
	src/app.cpp
	src/app_phase.cpp
	src/prefs.cpp
	src/time.cpp
	src/disk.cpp
	src/data.cpp
	src/data_xml.cpp
	src/mapdata.cpp
	src/cJSON.c
	src/yxml.c
	src/utf8.c
	src/input.cpp
	src/input_defaults.cpp
	#src/timeline.cpp
	src/body.cpp
	src/body_all.cpp
	src/body_volume.cpp
	src/body_hit.cpp
	src/sprite.cpp
	src/tile.cpp
	src/tile_map.cpp
	src/text.cpp
	src/render.cpp
	#src/render3d.cpp
	src/voice.cpp
	src/texture.cpp
	src/cel.cpp
	src/font.cpp
	src/sound.cpp
	src/music.cpp
	src/script.cpp
	src/script_func.cpp
	src/database.cpp
	src/title.cpp
	src/play.cpp
	src/play_ui.cpp
	src/stats.cpp
	src/ship.cpp
	#src/ship_control.cpp
	src/ship_melee.cpp
	src/ship_shield.cpp
	src/ship_all.cpp
	src/ship_script.cpp
	src/ship_seq.cpp
	src/bullet.cpp
	src/stage.cpp
	src/stage_load.cpp
	src/stage_script.cpp
	src/ui.cpp
	src/ui_load.cpp
	src/ui_action.cpp
	src/ui_cursor.cpp
	)

target_link_libraries(${GAME_EXE}
	${SDL2_LIBRARIES}
	${SDL2_IMAGE_LIBRARIES}
	${SDL2_MIXER_LIBRARIES}
	${GME_LIBRARIES}
	${OPENGL_LIBRARY}
	${PHYSFS_LIBRARY}
	${LUA_LIBRARIES}
	)

install(TARGETS ${GAME_EXE}
	RUNTIME DESTINATION .
	COMPONENT Application)
