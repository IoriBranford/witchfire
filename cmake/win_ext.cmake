include(ExternalProject)

# Pre-Built

set(EP_PREFIX ${CMAKE_BINARY_DIR}/ext)
set(DOWNLOAD_DIR ${EP_PREFIX}/dl)
set_directory_properties(PROPERTIES EP_PREFIX ${EP_PREFIX})

if(NOT SDL2_FOUND)
	ExternalProject_Add(SDL2
		URL http://libsdl.org/release/SDL2-devel-2.0.3-mingw.tar.gz
		DOWNLOAD_DIR ${DOWNLOAD_DIR}
		CONFIGURE_COMMAND ""
		BUILD_COMMAND ""
		INSTALL_COMMAND ""
		)
	ExternalProject_Get_Property(SDL2 SOURCE_DIR)

	set(SDL2_LIBRARIES mingw32 SDL2main SDL2.dll)

	set(SDL2_INCLUDE_DIRS	${SOURCE_DIR}/include)
	set(SDL2_LIBRARY_DIRS	${SOURCE_DIR}/${COMPILER_TARGET_MACHINE}/lib)

	set(SDL2_DLL		${SOURCE_DIR}/${COMPILER_TARGET_MACHINE}/bin/SDL2.dll)
endif(NOT SDL2_FOUND)

if(NOT SDL2_IMAGE_FOUND)
	ExternalProject_Add(SDL2_image
		URL https://www.libsdl.org/projects/SDL_image/release/SDL2_image-devel-2.0.0-mingw.tar.gz
		DOWNLOAD_DIR ${DOWNLOAD_DIR}
		CONFIGURE_COMMAND ""
		BUILD_COMMAND ""
		INSTALL_COMMAND ""
		)
	ExternalProject_Get_Property(SDL2_image SOURCE_DIR)

	set(SDL2_IMAGE_LIBRARIES SDL2_image.dll)

	set(SDL2_IMAGE_INCLUDE_DIRS ${SOURCE_DIR}/${COMPILER_TARGET_MACHINE}/include/SDL2)
	set(SDL2_IMAGE_LIBRARY_DIRS ${SOURCE_DIR}/${COMPILER_TARGET_MACHINE}/lib)

	set(SDL2_image_DLL	${SOURCE_DIR}/${COMPILER_TARGET_MACHINE}/bin/SDL2_image.dll)
	set(png16-16_DLL	${SOURCE_DIR}/${COMPILER_TARGET_MACHINE}/bin/libpng16-16.dll)
	set(zlib1_DLL		${SOURCE_DIR}/${COMPILER_TARGET_MACHINE}/bin/zlib1.dll)
endif(NOT SDL2_IMAGE_FOUND)

if(NOT SDL2_MIXER_FOUND)
	ExternalProject_Add(SDL2_mixer
		URL https://www.libsdl.org/projects/SDL_mixer/release/SDL2_mixer-devel-2.0.0-mingw.tar.gz
		DOWNLOAD_DIR ${DOWNLOAD_DIR}
		CONFIGURE_COMMAND ""
		BUILD_COMMAND ""
		INSTALL_COMMAND ""
		)
	ExternalProject_Get_Property(SDL2_mixer SOURCE_DIR)

	set(SDL2_MIXER_LIBRARIES SDL2_mixer.dll)

	set(SDL2_MIXER_INCLUDE_DIRS	${SOURCE_DIR}/${COMPILER_TARGET_MACHINE}/include/SDL2)
	set(SDL2_MIXER_LIBRARY_DIRS	${SOURCE_DIR}/${COMPILER_TARGET_MACHINE}/lib)

	set(SDL2_mixer_DLL	${SOURCE_DIR}/${COMPILER_TARGET_MACHINE}/bin/SDL2_mixer.dll)
	set(ogg-0_DLL		${SOURCE_DIR}/${COMPILER_TARGET_MACHINE}/bin/libogg-0.dll)
	set(vorbis-0_DLL	${SOURCE_DIR}/${COMPILER_TARGET_MACHINE}/bin/libvorbis-0.dll)
	set(vorbisfile-3_DLL	${SOURCE_DIR}/${COMPILER_TARGET_MACHINE}/bin/libvorbisfile-3.dll)
endif(NOT SDL2_MIXER_FOUND)

# To be built

set(EP_PREFIX ${CMAKE_BINARY_DIR}/ext/${COMPILER_TARGET_MACHINE})
set_directory_properties(PROPERTIES EP_PREFIX ${EP_PREFIX})

if(NOT LUA_FOUND)
	ExternalProject_Add(Lua
		URL http://luajit.org/download/LuaJIT-2.0.4.tar.gz
		URL_MD5 dd9c38307f2223a504cbfb96e477eca0
		DOWNLOAD_DIR ${DOWNLOAD_DIR}
		CONFIGURE_COMMAND ""
		BUILD_COMMAND make -C src LDFLAGS=-static-libgcc
		BUILD_IN_SOURCE 1
		INSTALL_COMMAND ""
		)
	ExternalProject_Get_Property(Lua SOURCE_DIR)
	ExternalProject_Get_Property(Lua BINARY_DIR)

	set(LUA_LIBRARIES lua51)

	set(LUA_INCLUDE_DIRS	${SOURCE_DIR}/src)
	set(LUA_LIBRARY_DIRS	${SOURCE_DIR}/src)

	set(lua51_DLL		${SOURCE_DIR}/src/lua51.dll)
endif(NOT LUA_FOUND)

if(NOT GME_FOUND)
	ExternalProject_Add(gme
		URL https://game-music-emu.googlecode.com/files/game-music-emu-0.6.0.tar.bz2
		URL_MD5 b98fafb737bc889dc65e7a8b94bd1bf5
		DOWNLOAD_DIR ${DOWNLOAD_DIR}
		CMAKE_GENERATOR ${CMAKE_GENERATOR}
		CMAKE_ARGS
			-DCMAKE_SHARED_LINKER_FLAGS=-static-libgcc\ -static-libstdc++
		INSTALL_COMMAND ""
		)
	ExternalProject_Get_Property(gme SOURCE_DIR)
	ExternalProject_Get_Property(gme BINARY_DIR)

	set(GME_LIBRARIES gme.dll)

	set(GME_INCLUDE_DIRS ${SOURCE_DIR})
	set(GME_LIBRARY_DIRS ${BINARY_DIR}/gme)

	set(gme_DLL ${BINARY_DIR}/gme/libgme.dll)
endif(NOT GME_FOUND)

if(NOT PHYSFS_FOUND)
	ExternalProject_Add(physfs
		URL http://icculus.org/physfs/downloads/physfs-2.0.3.tar.bz2
		DOWNLOAD_DIR ${DOWNLOAD_DIR}
		CMAKE_GENERATOR ${CMAKE_GENERATOR}
		CMAKE_ARGS
			-DPHYSFS_BUILD_STATIC=FALSE
			-DPHYSFS_BUILD_WX_TEST=FALSE
			-DPHYSFS_BUILD_TEST=FALSE
			-DCMAKE_C_FLAGS=-Wno-pointer-to-int-cast
			-DCMAKE_SHARED_LINKER_FLAGS=-static-libgcc\ -static-libstdc++
		INSTALL_COMMAND ""
		)
	ExternalProject_Get_Property(physfs SOURCE_DIR)
	ExternalProject_Get_Property(physfs BINARY_DIR)

	set(PHYSFS_LIBRARY physfs.dll)

	set(PHYSFS_INCLUDE_DIR ${SOURCE_DIR})
	set(PHYSFS_LIBRARY_DIR ${BINARY_DIR})

	set(physfs_DLL ${BINARY_DIR}/libphysfs.dll)
endif(NOT PHYSFS_FOUND)
