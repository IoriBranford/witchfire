set(CORE_DLLS
	winpthread-1	# Win-Builds
	)

foreach(DLL ${CORE_DLLS})
	find_file(${DLL}_DLL
		NAMES
			lib${DLL}.dll
		PATHS
			/opt/windows_${COMPILER_TARGET_BITS}	# Win-Builds
			#/mingw					# MinGW
			#"$ENV{PATH}"
		PATH_SUFFIXES
			bin
		)
	install(FILES ${${DLL}_DLL}
		DESTINATION .
		COMPONENT LibRuntime)
endforeach(DLL)

set(LIB_DLLS
	SDL2
	SDL2_image
	png16-16
	zlib1
	SDL2_mixer
	ogg-0
	vorbis-0
	vorbisfile-3
	freetype-6
	physfs
	gme
	lua51
	)

foreach(DLL ${LIB_DLLS})
	if(DEFINED ${DLL}_DLL)
	# we already know where dll is because we grabbed/built it ourselves
	# (because library isn't installed in the devenv)
		if("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_BINARY_DIR}")
		# in-source build for rapid testing
			add_custom_command(TARGET ${GAME_EXE} POST_BUILD
				COMMAND ${CMAKE_COMMAND} -E copy_if_different
				${${DLL}_DLL} ${CMAKE_SOURCE_DIR}
				)
		endif("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_BINARY_DIR}")
	else(DEFINED ${DLL}_DLL)
		find_file(${DLL}_DLL
			NAMES
				${DLL}.dll
				lib${DLL}.dll
			PATHS
				/
				/usr
				/usr/local
				/usr/local/cross-tools/${COMPILER_TARGET_MACHINE}
				#"$ENV{PATH}"
			PATH_SUFFIXES
				bin
				lib
				#lib/x86
			)
	endif(DEFINED ${DLL}_DLL)
	install(FILES ${${DLL}_DLL}
		DESTINATION .
		COMPONENT LibRuntime)
endforeach(DLL)
